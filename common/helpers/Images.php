<?php
namespace common\helpers;


class Images {

    const TYPE_PAGES = '5';

    const TYPE_LESSONS = '10';

    const TYPE_COURSES = '20';

    const TYPE_TASKS = '30';

    const TYPE_USERS = '40';

    public static $params = array(

        self::TYPE_PAGES => array(
            'extensions' => array('jpg','jpeg','png'),
            'max' => 10,
            'min_width' => '120',
            'min_height' => '120',
        ),

        self::TYPE_LESSONS => array(
            'extensions' => array('jpg','jpeg','png'),
            'max' => 1,
            'min_width' => '120',
            'min_height' => '120',
        ),

        self::TYPE_COURSES => array(
            'extensions' => array('jpg','jpeg','png'),
            'max' => 1,
            'min_width' => '120',
            'min_height' => '120',
        ),

        self::TYPE_TASKS => array(
            'extensions' => array('jpg','jpeg','png'),
            'max' => 1,
            'min_width' => '120',
            'min_height' => '120',
        ),

        self::TYPE_USERS => array(
            'extensions' => array('jpg','jpeg','png'),
            'max' => 1,
            'min_width' => '120',
            'min_height' => '120',
        ),
    );


}
<?php

namespace common\models;

use common\components\traits\Validation;
use Yii;

/**
 * This is the model class for table "users_profile".
 *
 * @property integer $id
 * @property integer $users_id
 * @property string $surname
 * @property string $patronymic
 * @property string $post
 *
 * @property Users $users
 */
class UsersProfile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users_profile';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['users_id', 'cities_id', 'sex', 'phone'], 'integer'],
            [['surname', 'vk', 'country'], 'string', 'max' => 255],
            [['birthday'], 'safe'],
            [['users_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['users_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'users_id' => 'Пользователь',
            'surname' => 'Фамилия',
            'cities_id' => 'Город',
            'phone' => 'Телефон',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasOne(Users::className(), ['id' => 'users_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(Cities::className(), ['id' => 'cities_id']);
    }

}

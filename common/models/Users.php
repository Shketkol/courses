<?php
namespace common\models;

use common\components\traits\Validation;
use common\helpers\Images;
use shketkol\images\src\behaviors\ImageBehave;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\Url;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class Users extends ActiveRecord implements IdentityInterface
{
    use Validation;

    const STATUS_DELETED = 0;
    const STATUS_INVITED = 1;
    const STATUS_REQUESTED = 2;
    const STATUS_DEACTIVATED = 3;
    const STATUS_ACTIVE = 10;

    public static $Statuses = [
        self::STATUS_INVITED => "Приглашен",
        self::STATUS_REQUESTED => "Запрос",
        self::STATUS_ACTIVE => "Активирован",
        self::STATUS_DEACTIVATED => "Деактивирован",
    ];

    const TYPE_ADMIN = 1;
    const TYPE_TEACHER = 2;
    const TYPE_SCHOLAR = 3;
    const TYPE_FRANCH = 4;

    public $confirm_pass;
    public $surname;
    public $phone;
    public $cities_id;
    public $content;
    public $sex;
    public $birthday;
    public $country;

    public $img_file;
    public $newPassword;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%users}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => new Expression('NOW()'),
            ],
            'img_file' => [
                'class' => ImageBehave::className(),
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email','password_hash', 'cities_id', 'name', 'surname'], 'required'],
            [['status', 'email_confirmed', 'users_roles_id', 'cities_id', 'sex', 'phone'], 'integer'],
            [['birthday'], 'date', 'format' => 'php:d.m.Y'],
            [['email'], 'unique'],
            [['email'], 'email'],
            [['password_hash', 'email'], 'filter', 'filter' => 'trim', 'skipOnArray' => true],
            [['name', 'password_hash', 'password_reset_token', 'email', 'surname', 'phone', 'confirm_pass'], 'string', 'max' => 255],
            [['password_hash'], 'string', 'min' => 6],
            [['auth_key'], 'string', 'max' => 32],
            [['password_reset_token'], 'unique'],
            [['content', 'country', 'images'], 'safe'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['users_roles_id', 'default', 'value' => self::TYPE_SCHOLAR],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED, self::STATUS_DEACTIVATED, self::STATUS_REQUESTED]],

            [['newPassword'], 'validateBreak'],
            [['password_hash'], 'validateBreak2'],
            [['name', 'country', 'surname'], 'filter', 'filter' => 'trim', 'skipOnArray' => true],

            [['img_file'], 'file', 'extensions' => 'jpg, gif, png'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'email' => 'E-mail',
            'created_at' => 'Дата регистрации',
            'updated_at' => 'Дата изменения',
            'password_hash' => 'Пароль',
            'confirm_pass' => 'Подтверждение пароля',
            'status' => 'Статус',
            'surname' => 'Фамилия',
            'cities_id' => 'Город',
            'users_roles_id' => 'Роль',
            'phone' => 'Телефон',
            'birthday' => 'День рождения',
            'sex' => 'Пол',
            'country' => 'Страна',
        ];
    }

    public function validateBreak($attribute, $params=null)
    {
        if (strstr($attribute, ' ') !== false){
            $this->addError('newPassword', 'Проверьте корректность введенных данных');
        }
    }
    public function validateBreak2($attribute, $params=null)
    {
        if (strstr($this->$attribute, ' ') !== false){
            $this->addError($attribute, 'Проверьте корректность введенных данных');
        }
    }

    public function validatePass($attribute, $params=null)
    {
        if (!\Yii::$app->security->validatePassword($attribute, $this->password_hash)) {
            $this->addError($attribute, 'Неправильный пароль');
            return false;
        }else {
            $this->password_hash = $attribute;
            if (!$this->validate(['password_hash'])){
                return false;
            } else {
                return true;
            }
        }
    }

    public function validatePassLenth($attribute)
    {
        $this->password_hash = $attribute;
        $this->password_hash = trim($this->password_hash);
        if (iconv_strlen($this->password_hash) < 6){
            $this->addError('password_hash', 'Пароль не может быть меньше 6 символов');
            return false;
        } else {
            return true;
        }
    }

    public static function find()
    {
        return parent::find()->onCondition(static::tableName() . '.status <> :status OR ' . static::tableName() . '.status IS NULL',
            [
                ':status' => self::STATUS_DELETED
            ]);
    }


    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
//        if (!static::isPasswordResetTokenValid($token)) {
//            return null;
//        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public function getUsersProfile()
    {
        return $this->hasOne(UsersProfile::className(), ['users_id' => 'id']);
    }

    public function getUsersRoles()
    {
        return $this->hasOne(UsersRoles::className(), ['id' => 'users_roles_id']);
    }

    public static function deleteUser($id){
        $model = Users::findOne($id);
        if(!empty($model)){
            $model->email = $model->email.'.'.time();
            $model->status = Users::STATUS_DELETED;
            if($model->save()){
                return true;
            } else {
                return false;
            }
        }else {
            return false;
        }

    }

    public static function getCountFinish($user)
    {
        $id_arr = [];
        $models = UsersAnswers::find()
            ->select('id, tasks_id')
            ->where(['users_id' => $user])
            ->asArray()
            ->all();

        $tasks = [];
        foreach ($models as $model){
            if (array_search($model['tasks_id'], $tasks) === false){
                $tasks[] = $model['tasks_id'];
                $id_arr[] = $model['id'];
            }
        }

        $models = UsersAnswers::find()
            ->where(['IN', 'id', $id_arr])
            ->count();

        return $models;
    }

    public static function getCountFinishSuccess($user)
    {
        $id_arr = [];
        $models = UsersAnswers::find()
            ->select('id, tasks_id')
            ->where(['users_id' => $user])
            ->andWhere(['correct' => 1])
            ->asArray()
            ->all();

        $tasks = [];
        foreach ($models as $model){
            if (array_search($model['tasks_id'], $tasks) === false){
                $tasks[] = $model['tasks_id'];
                $id_arr[] = $model['id'];
            }
        }

        $models = UsersAnswers::find()
            ->where(['IN', 'id', $id_arr])
            ->count();

        return $models;
    }


    public function getArrayField(){
        return array('content');
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return bool whether the email was send
     */
    public function sendEmail()
    {
        /* @var $user User */
        $user = Users::findOne([
            'status' => Users::STATUS_ACTIVE,
            'email' => $this->email,
        ]);

        $admins  = Users::findAll([
            'users_roles_id' => Users::TYPE_ADMIN,
        ]);

        if (!$user) {
            return false;
        }

        foreach ($admins as $admin){
            Yii::$app
                ->mailer
                ->compose(
                    ['html' => 'signup-html', 'text' => 'signup-text'],
                    ['user' => $user],
                    ['admin' => $admin]
                )
                ->setFrom([$admin->email => 'Проект курсов'])
                ->setTo($this->email)
                ->setSubject('Регистрация нового ученика' . $user->name)
                ->send();
        }

        return true;
    }
}


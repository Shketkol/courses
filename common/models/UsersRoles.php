<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "users_roles".
 *
 * @property integer $id
 * @property integer $type
 * @property string $name
 *
 * @property Users[] $users
 */
class UsersRoles extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users_roles';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'role_type'], 'integer'],
            [['name'], 'required'],
            [['name'], 'string', 'max' => 300],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Тип',
            'name' => 'Роль',
            'role_type' => 'Тип роли',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(Users::className(), ['users_roles_id' => 'id']);
    }

}

<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "questions_task".
 *
 * @property integer $id
 * @property integer $tasks_id
 * @property string $desc
 * @property integer $format
 * @property string $options
 * @property integer $rating
 * @property integer $test
 * @property string $correct_answer
 *
 * @property Tasks $tasks
 */
class QuestionsTask extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'questions_task';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tasks_id'], 'required'],
            [['tasks_id', 'format', 'rating', 'test'], 'integer'],
            [['desc'], 'string'],
            [['tasks_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tasks::className(), 'targetAttribute' => ['tasks_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tasks_id' => 'Задание',
            'desc' => 'Описание',
            'format' => 'Формат',
            'options' => 'Варианты для выпадающего списка',
            'rating' => 'Рейтинг',
            'test' => 'Вариант проверки',
            'correct_answer' => 'Правильные ответы',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks()
    {
        return $this->hasOne(Tasks::className(), ['id' => 'tasks_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVariantsAnswer()
    {
        return $this->hasMany(VariantsAnswer::className(), ['questions_task_id' => 'id']);
    }

    public static function getAnswer($user, $question, $variant=null)
    {
        if($variant != null){
            $model = UsersAnswers::find()
                ->where(['users_id' => $user])
                ->andWhere(['questions_task_id' => $question])
                ->andWhere(['answer' => $variant])
                ->one();
        } else {
            $model = UsersAnswers::find()
                ->where(['users_id' => $user])
                ->andWhere(['questions_task_id' => $question])
                ->one();
        }

        return (!empty($model)) ? $model : null;
    }
}

<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "cities".
 *
 * @property integer $id
 * @property string $name
 *
 * @property UsersProfile[] $usersProfiles
 */
class Statistic extends Model
{
    public $year;
    public $month;

    public static $monthArr = [
        1 => 'Январь',
        2 => 'Февраль',
        3 => 'Март',
        4 => 'Апрель',
        5 => 'Май',
        6 => 'Июнь',
        7 => 'Июль',
        8 => 'Август',
        9 => 'Сентябрь',
        10 => 'Октябрь',
        11 => 'Ноябрь',
        12 => 'Декабрь'
    ];
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['year', 'month'], 'required'],
            [['year', 'month'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'year' => 'Год',
        ];
    }
}

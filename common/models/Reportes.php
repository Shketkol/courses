<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "reportes".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $intent_id
 * @property string $content
 * @property integer $balls
 * @property integer $test
 *
 * @property Users $user
 * @property Intent $intent
 */
class Reportes extends \yii\db\ActiveRecord
{
    const REPORT_NEW = 0;
    const REPORT_SUCCESS = 1;
    const REPORT_WRONG = 2;

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['date'],
                ],
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reportes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'intent_id', 'content'], 'required'],
            [['user_id', 'intent_id', 'balls', 'test'], 'integer'],
            [['content'], 'string'],
            [['date'], 'safe'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['intent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Intent::className(), 'targetAttribute' => ['intent_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'intent_id' => 'Цель',
            'content' => 'Описание',
            'balls' => 'Балы',
            'test' => 'Проверен',
            'date' => 'Дата',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIntent()
    {
        return $this->hasOne(Intent::className(), ['id' => 'intent_id']);
    }
}

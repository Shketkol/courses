<?php

namespace common\models;

use common\components\traits\Validation;
use common\helpers\Images;
use shketkol\images\src\behaviors\ImageBehave;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "lessons".
 *
 * @property integer $id
 * @property integer $courses_id
 * @property string $title
 * @property string $desc
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Courses $courses
 * @property Tasks[] $tasks
 */
class Lessons extends \yii\db\ActiveRecord
{
    use Validation;

    public $title_model = 'Lessons';

    public $image;
    public $active;

    public static $types =array(
        1 => 'green',
        2 => 'blue',
        3 => 'orange',
    );

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => new Expression('NOW()'),

            ],
            'image' => [
                'class' => ImageBehave::className(),
            ]

        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lessons';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['courses_id', 'title'], 'required'],
            [['courses_id', 'position', 'active'], 'integer'],
            [['created_at', 'updated_at', 'desc'], 'safe'],
            [['duration'], 'number'],
            [['title', 'video'], 'string', 'max' => 300],
            [['courses_id'], 'exist', 'skipOnError' => true, 'targetClass' => Courses::className(), 'targetAttribute' => ['courses_id' => 'id']],

            [['image'], 'file', 'extensions' => 'jpg, gif, png, jpeg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'courses_id' => 'Тренинг',
            'title' => 'Название',
            'desc' => 'Описание',
            'video' => 'Видео',
            'image' => 'Изображение',
            'duration' => 'Длительность, часы',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourses()
    {
        return $this->hasOne(Courses::className(), ['id' => 'courses_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks()
    {
        return $this->hasMany(Tasks::className(), ['lessons_id' => 'id']);
    }

    public function getPositionField(){
        return 'position';
    }

    public function getPositionFieldType(){
        return 'courses_id';
    }

    public static function getCountFinish($user, $courses)
    {
        $model = UsersLessons::find()
            ->where(['courses_id' => $courses, 'users_id' => $user, 'status' => 2])
            ->count();
        return $model;
    }
}

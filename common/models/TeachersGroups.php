<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "teachers_groups".
 *
 * @property int $id
 * @property int $teachers_id
 * @property int $teachers_courses_id
 * @property int $groups_id
 *
 * @property Teachers $teachers
 * @property TeachersCourses $teachersCourses
 * @property Groups $groups
 */
class TeachersGroups extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'teachers_groups';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['teachers_id', 'teachers_courses_id', 'groups_id'], 'required'],
            [['teachers_id', 'teachers_courses_id', 'groups_id'], 'integer'],
            [['teachers_id'], 'exist', 'skipOnError' => true, 'targetClass' => Teachers::className(), 'targetAttribute' => ['teachers_id' => 'id']],
            [['teachers_courses_id'], 'exist', 'skipOnError' => true, 'targetClass' => TeachersCourses::className(), 'targetAttribute' => ['teachers_courses_id' => 'id']],
            [['groups_id'], 'exist', 'skipOnError' => true, 'targetClass' => Groups::className(), 'targetAttribute' => ['groups_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'teachers_id' => 'Teachers ID',
            'teachers_courses_id' => 'Teachers Courses ID',
            'groups_id' => 'Группа',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeachers()
    {
        return $this->hasOne(Teachers::className(), ['id' => 'teachers_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeachersCourses()
    {
        return $this->hasOne(TeachersCourses::className(), ['id' => 'teachers_courses_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroups()
    {
        return $this->hasOne(Groups::className(), ['id' => 'groups_id']);
    }
}

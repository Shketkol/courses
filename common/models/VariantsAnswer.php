<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "variants_answer".
 *
 * @property integer $id
 * @property integer $questions_task_id
 * @property string $title
 *
 * @property QuestionsTask $questionsTask
 */
class VariantsAnswer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'variants_answer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['questions_task_id', 'title'], 'required'],
            [['questions_task_id', 'correct'], 'integer'],
            [['title'], 'string', 'max' => 300],
            [['questions_task_id'], 'exist', 'skipOnError' => true, 'targetClass' => QuestionsTask::className(), 'targetAttribute' => ['questions_task_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'questions_task_id' => 'Questions Task ID',
            'title' => 'Название',
            'correct' => 'Правильный ответ',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestionsTask()
    {
        return $this->hasOne(QuestionsTask::className(), ['id' => 'questions_task_id']);
    }
}

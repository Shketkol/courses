<?php

namespace common\models;

use common\components\traits\Validation;
use common\helpers\Images;
use shketkol\images\src\behaviors\ImageBehave;
use Yii;

/**
 * This is the model class for table "tasks".
 *
 * @property integer $id
 * @property integer $lessons_id
 * @property string $title
 * @property string $desc
 *
 * @property QuestionsTask[] $questionsTasks
 * @property Lessons $lessons
 */
class Tasks extends \yii\db\ActiveRecord
{
    use Validation;

    public $file;

    public function behaviors()
    {
        return [
            'file' => [
                'class' => ImageBehave::className(),
            ]

        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tasks';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lessons_id', 'title'], 'required'],
            [['lessons_id', 'position'], 'integer'],
            [['desc'], 'safe'],
            [['title'], 'string', 'max' => 300],
            [['lessons_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lessons::className(), 'targetAttribute' => ['lessons_id' => 'id']],

            [['file'], 'file'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lessons_id' => 'Урок',
            'title' => 'Название',
            'desc' => 'Описание',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestionsTasks()
    {
        return $this->hasMany(QuestionsTask::className(), ['tasks_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLessons()
    {
        return $this->hasOne(Lessons::className(), ['id' => 'lessons_id']);
    }

    public function getPositionField(){
        return 'position';
    }

    public function getPositionFieldType(){
        return 'lessons_id';
    }
}

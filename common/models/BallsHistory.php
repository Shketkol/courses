<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "balls_history".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $courses_id
 * @property integer $ball
 * @property string $created_at
 *
 * @property Users $user
 * @property Courses $courses
 */
class BallsHistory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'balls_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'courses_id', 'ball'], 'required'],
            [['user_id', 'courses_id', 'ball'], 'integer'],
            [['created_at'], 'safe'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['courses_id'], 'exist', 'skipOnError' => true, 'targetClass' => Courses::className(), 'targetAttribute' => ['courses_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'courses_id' => 'Courses ID',
            'ball' => 'Ball',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourses()
    {
        return $this->hasOne(Courses::className(), ['id' => 'courses_id']);
    }

    public static function getCountBalls($user, $course)
    {
        $model = BallsHistory::find()
            ->select('count(ball) as count')
            ->where(['user_id' => $user, 'courses_id' => $course])
            ->asArray()
            ->one();

        return (!empty($model['count'])) ? $model['count'] : 0;
    }
}

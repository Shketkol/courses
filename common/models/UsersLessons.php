<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "users_lessons".
 *
 * @property integer $id
 * @property integer $users_id
 * @property integer $courses_id
 * @property integer $lessons_id
 * @property string $created_at
 *
 * @property Users $users
 * @property Courses $courses
 * @property Lessons $lessons
 */
class UsersLessons extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                'value' => new Expression('NOW()'),
            ]
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users_lessons';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['users_id', 'courses_id', 'lessons_id'], 'required'],
            [['users_id', 'courses_id', 'lessons_id', 'status', 'raad'], 'integer'],
            [['created_at'], 'safe'],
            [['users_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['users_id' => 'id']],
            [['courses_id'], 'exist', 'skipOnError' => true, 'targetClass' => Courses::className(), 'targetAttribute' => ['courses_id' => 'id']],
            [['lessons_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lessons::className(), 'targetAttribute' => ['lessons_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'users_id' => 'Users ID',
            'courses_id' => 'Courses ID',
            'lessons_id' => 'Lessons ID',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasOne(Users::className(), ['id' => 'users_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourses()
    {
        return $this->hasOne(Courses::className(), ['id' => 'courses_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLessons()
    {
        return $this->hasOne(Lessons::className(), ['id' => 'lessons_id']);
    }

    public static function getUserLessons($user, $lessons)
    {
        $model = UsersLessons::findOne(['users_id' => $user, 'lessons_id' => $lessons]);
        return (!empty($model)) ? true : false;
    }

    public static function getUserLessonsIsset($user, $lessons)
    {
        $model = UsersLessons::findOne(['users_id' => $user, 'lessons_id' => $lessons]);
        return (!empty($model)) ? $model : null;
    }
}

<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "invoices".
 *
 * @property integer $id
 * @property integer $users_id
 * @property integer $products_id
 * @property string $price
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Users $users
 * @property Products $products
 * @property Payments[] $payments
 */
class Invoices extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => new Expression('NOW()'),
            ]
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'invoices';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['users_id', 'courses_id'], 'required'],
            [['users_id', 'courses_id', 'status', 'currencies_id'], 'integer'],
            [['price'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['users_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['users_id' => 'id']],
            [['courses_id'], 'validateCourse']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'users_id' => 'Пользователь',
            'products_id' => 'Products ID',
            'courses_id' => 'Курс',
            'price' => 'Price',
            'status' => 'Статус',
            'created_at' => 'Дата создания',
            'updated_at' => 'Updated At',
        ];
    }

    public function validateCourse()
    {
        $model = Invoices::findOne(['users_id' => $this->users_id, 'courses_id' => $this->courses_id, 'status' => 3]);
        if (!empty($model)){
            $this->addError('users_id', 'Эта оплата уже проведена');
            return false;
        } else {
            return true;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasOne(Users::className(), ['id' => 'users_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourses()
    {
        return $this->hasOne(Courses::className(), ['id' => 'courses_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayments()
    {
        return $this->hasMany(Payments::className(), ['invoices_id' => 'id']);
    }

    public function getCurrency()
    {
        return $this->hasOne(Currencies::className(), ['id' => 'currencies_id']);
    }
}

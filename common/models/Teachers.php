<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "teachers".
 *
 * @property integer $id
 * @property integer $users_id
 * @property integer $cities_id
 * @property integer $courses_id
 *
 * @property Users $users
 * @property Cities $cities
 * @property Courses $courses
 */
class Teachers extends \yii\db\ActiveRecord
{
    public $email;
    public $phone;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'teachers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['users_id', 'cities_id'], 'required'],
            [['users_id', 'cities_id'], 'integer'],
            [['users_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['users_id' => 'id']],
            [['cities_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cities::className(), 'targetAttribute' => ['cities_id' => 'id']],
            [['users_id'], 'validateUser']
        ];
    }

    public function validateUser()
    {
        $model = Teachers::findOne(['users_id' => $this->users_id]);
        if (!empty($model)){
            $this->addError('users_id', 'Данный преподаватель уже добавлен в систему');
            return false;
        } else {
            return true;
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'users_id' => 'Пользователь',
            'cities_id' => 'Город',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasOne(Users::className(), ['id' => 'users_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasOne(Cities::className(), ['id' => 'cities_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourses()
    {
        return $this->hasMany(TeachersCourses::className(), ['teacher_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroups()
    {
        return $this->hasMany(TeachersGroups::className(), ['id' => 'teachers_groups_id']);
    }

    public static function getCountCourses($users)
    {
        return TeachersCourses::find()->where(['teacher_id' => $users])->count();
    }

    public static function getCountPupils($user)
    {
        $result = 0;
        $cities = Teachers::find()->where(['users_id' => $user])->one();
        $cities_arr[] = $cities->cities_id;
        $courses_id = [];
        $courses = TeachersCourses::findAll(['teacher_id' => $cities->id]);
        foreach ($courses as $city){
            $courses_id[] = $city->courses_id;
        }

        $users = Users::find()
            ->join('LEFT JOIN', 'users_profile', 'users.id = users_profile.users_id')
            ->where(['IN', 'users_profile.cities_id', $cities_arr])
            ->all();
        foreach ($users as $user){
            $user_course = Invoices::find()
                ->select('users_id')
                ->distinct()
                ->where(['users_id' => $user->id])
                ->andWhere(['IN', 'courses_id', $courses_id])
                ->andWhere(['status' => 3])
                ->asArray()
                ->one();

            if(!empty($user_course)){
                $result = $result + 1;
            }
        }

        return $result;



    }
}

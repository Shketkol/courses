<?php

namespace common\models;

use common\helpers\Images;
use shketkol\images\src\behaviors\ImageBehave;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "courses".
 *
 * @property integer $id
 * @property string $title
 * @property string $desc
 * @property string $price
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Lessons[] $lessons
 */
class Courses extends \yii\db\ActiveRecord
{
    const TYPE_SINGLE = 1;
    const TYPE_GROUP = 2;
    const TYPE_TEACHER = 3;

    public static $types = [
        self::TYPE_SINGLE => 'Одиночный ученик',
        self::TYPE_GROUP => 'Груповой курс',
        self::TYPE_TEACHER => 'Для преподавателя'
    ];

    public $image;

    public $title_model = 'Courses';

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => new Expression('NOW()'),
            ],
            'image' => [
                'class' => ImageBehave::className(),
            ]
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'courses';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'price'], 'required'],
            [['active', 'status'], 'integer'],
            [['price', 'currencies_id', 'type'], 'number'],
            [['created_at', 'updated_at', 'desc', 'start', 'finish'], 'safe'],
            [['title'], 'string', 'max' => 300],
            [['lead'], 'string', 'max' => 75],
            [['active'], 'default', 'value' => 0],
            [['start', 'finish'], 'date', 'format' => 'php:d.m.Y'],
            [['start', 'finish'], 'validateDate'],

            [['image'], 'file', 'extensions' => 'jpg, gif, png, jpeg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Тип курса',
            'title' => 'Название',
            'desc' => 'Описание',
            'price' => 'Цена',
            'image' => 'Изображение',
            'lead' => 'Кратко',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'currencies_id' => 'Валюта',
            'start' => 'Дата начала курса',
            'finish' => 'Дата окончания курса',
            'active' => 'Отображать на сайте',
        ];
    }

    public function validateDate()
    {
        if (!empty($this->start)){
            if (!empty($this->finish)){
                if (strtotime($this->start) >= strtotime($this->finish)){
                    $this->addError('finish', 'Дата начала больше');
                    return false;
                }
            } else {
                $this->addError('finish', 'Дата окончания не может быть пустой');
                return false;
            }
        }
        if (!empty($this->finish)){
            if (!empty($this->start)){
                if (strtotime($this->start) >= strtotime($this->finish)){
                    $this->addError('finish', 'Дата начала больше');
                    return false;
                }
            } else {
                $this->addError('finish', 'Дата начала не может быть пустой');
                return false;
            }
        }
        return true;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLessons()
    {
        return $this->hasMany(Lessons::className(), ['courses_id' => 'id']);
    }

    public function getCurrency()
    {
        return $this->hasOne(Currencies::className(), ['id' => 'currencies_id']);
    }

    public static function getUserPay($user, $courses)
    {
        $model = Invoices::find()
            ->where(['users_id' => $user])
            ->andWhere(['courses_id' => $courses, 'status' => 3])
            ->one();
        return (!empty($model)) ? 'Оплачен' : 'Неоплачен';
    }
}

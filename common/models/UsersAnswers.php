<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "users_answers".
 *
 * @property integer $id
 * @property integer $users_id
 * @property integer $tasks_id
 * @property integer $questions_task_id
 * @property string $answer
 * @property string $created_at
 *
 * @property Users $users
 * @property Tasks $tasks
 * @property QuestionsTask $questionsTask
 */
class UsersAnswers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                'value' => new Expression('NOW()'),
            ]
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users_answers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['users_id', 'tasks_id', 'questions_task_id', 'answer'], 'required'],
            [['users_id', 'tasks_id', 'questions_task_id', 'correct', 'result'], 'integer'],
            [['created_at'], 'safe'],
            [['answer'], 'string', 'max' => 300],
            [['users_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['users_id' => 'id']],
            [['tasks_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tasks::className(), 'targetAttribute' => ['tasks_id' => 'id']],
            [['questions_task_id'], 'exist', 'skipOnError' => true, 'targetClass' => QuestionsTask::className(), 'targetAttribute' => ['questions_task_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'users_id' => 'Пользователь',
            'tasks_id' => 'Задания',
            'questions_task_id' => 'Questions Task ID',
            'answer' => 'Answer',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasOne(Users::className(), ['id' => 'users_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks()
    {
        return $this->hasOne(Tasks::className(), ['id' => 'tasks_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestionsTask()
    {
        return $this->hasOne(QuestionsTask::className(), ['id' => 'questions_task_id']);
    }

    public static function getKeep($user, $tasks_id)
    {
//        var_dump($user, $tasks_id);die;

        $task = Tasks::findOne(['id' => $tasks_id]);
        $tasksArr = [];
        foreach (Tasks::findAll(['lessons_id' => $task->lessons_id]) as $item){
            $tasksArr[] = $item->id;
        }

        $model = self::find()
            ->where([
                'users_id' => $user,
                'correct' => null,
                ])
            ->andWhere(['in', 'tasks_id', $tasksArr])
            ->all();

        if (empty($model)){
            $model = self::find()
                ->where([
                    'users_id' => $user,
                    'correct' => 0,
                ])
                ->andWhere(['in', 'tasks_id', $tasksArr])
                ->all();
        }

        return (!empty($model)) ? false : true;
    }
}

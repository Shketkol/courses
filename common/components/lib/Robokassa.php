<?php
namespace common\components\lib;
/* ===================================
 * Author: Nazarkin Roman
 * -----------------------------------
 * Contacts:
 * email - roman@nazarkin.su
 * icq - 642971062
 * skype - roman444ik
 * -----------------------------------
 * GitHub:
 * https://github.com/NazarkinRoman
 * ===================================
*/
class Robokassa{
    private $mrh_login = 'generationbz',
        $mrh_pass1 = 'VhUJG11cu75AFJ5LAnfX',
        $mrh_pass2 = 'dsdK83MhWucedYD2lV63',
        $isTtest = '&IsTest=0',
        $endpoint = 'https://auth.robokassa.ru/Merchant/Index.aspx?';


    public $out_summ, $inv_id = 0, $inv_desc, $shp_item = 1, $in_curr = '', $culture = "ru",$encoding = "utf-8", $OutSumCurrency='RUR'; /* request parameters */
    /**
     * Описание параметров
     *
     * @param string $mrh_login логин мерчанта
     * @param string $mrh_pass1 пароль №1
     * @param string $mrh_pass2 пароль №2
     * @param boolean $test работа с тестовым сервером
     *
     * @param string $inv_desc описание заказа
     * @param string $in_curr способ оплаты
     * @param string $culture язык сайта
     * @param string $encoding кодировка сайта
     * @param string $OutSumCurrency в какой валюте выставлена сумма
     *
     * @param integer $inv_id № заказа в магазине
     * @param float $out_summ Сумма к оплате
     *
     *
     * @return none
     */
    public function __construct($test = false)
    {

        if($test) $this->isTtest = '&IsTest=1';
    }
    /**
     * Получение URL для запроса
     *
     * @return string $url
     */
    public function getRedirectURL()
    {
        $crc  = md5("$this->mrh_login:$this->out_summ:$this->inv_id:$this->OutSumCurrency:$this->mrh_pass1:Shp_item=$this->shp_item");
        return $this->endpoint . 'MrchLogin=' .$this->mrh_login
        ."&OutSum=$this->out_summ&OutSumCurrency=$this->OutSumCurrency&InvId=$this->inv_id&IncCurrLabel=$this->in_curr".
        "&Desc=$this->inv_desc&SignatureValue=$crc&Shp_item=$this->shp_item".
        "&Culture=$this->culture&Encoding=$this->encoding".$this->isTtest;
    }

    public function getResultURL()
    {
        $hash = md5("$this->mrh_login:$this->inv_id:$this->mrh_pass2");
        $invId = '&InvoiceID=' . $this->inv_id;
        return 'https://auth.robokassa.ru/Merchant/WebService/Service.asmx/OpState?MerchantLogin=' . $this->mrh_login
        . $invId
        . $this->isTtest
        . '&Signature=' . $hash;

    }

    /**
     * Описание параметров
     *
     * @param integer $inv_id № заказа в магазине
     *
     *  возвращает XML строку
     */

    public function getPaymentStatusXML($inv_id)
    {
        $hash = md5("$this->mrh_login:$inv_id:$this->mrh_pass2");
        $invId = '&InvoiceID=' . $inv_id;
        $url = 'https://auth.robokassa.ru/Merchant/WebService/Service.asmx/OpState?MerchantLogin=' . $this->mrh_login
        . $invId
        . $this->isTtest
        . '&Signature=' . $hash;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_FAILONERROR,1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $out = curl_exec($ch) or die();
        curl_close($ch);

        return $out;

    }


    /**
     * Описание параметров
     *
     * @param string $IncCurrLabel способ оплаты
     * @param float $summ Сумма к оплате
     *
     *
     * @return $out массив ключ(способ оплаты) => значение(Сумма с наценкой метода)
     */

    public function getIncSums($sum, $IncCurrLabel='', $lang='ru'){
        $url ="https://auth.robokassa.ru/Merchant/WebService/Service.asmx/GetRates?MerchantLogin=$this->mrh_login&IncCurrLabel=$IncCurrLabel&OutSum=$sum&Language=$lang";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_FAILONERROR,1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch) or die();
        curl_close($ch);
        $sums = simplexml_load_string($result);
        $out = array();
        foreach($sums->Groups->Group as $sum){
            $key = (string)$sum->Items->Currency['Label'];
            $out[$key] = (float)$sum->Items->Currency->Rate['IncSum'];
        }
        return $out;
    }

    /**
     * Описание параметров
     *
     * @param string $IncCurrLabel способ оплаты
     * @param float $summ Сумма к оплате
     *
     *
     * @return $OutSum Сумма к зачислению на счет магазина с вычетом процента при оплате
     */
    public function calcOutSum($sum, $IncCurrLabel){
        $url = "https://auth.robokassa.ru/Merchant/WebService/Service.asmx/CalcOutSumm?MerchantLogin=$this->mrh_login&IncCurrLabel=$IncCurrLabel&IncSum=$sum";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_FAILONERROR,1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch) or die();
        curl_close($ch);
        $result = simplexml_load_string($result);
        $OutSum = (float)$result->OutSum;

        return $OutSum;
    }
    /**
     * Проверка исполнения операции. Сравнение хеша
     *
     * @param string $hash значение SignatureValue, переданное кассой на Result URL
     * @param boolean $checkSuccess проверка параметров в скрипте завершения операции (SuccessURL)
     * @return boolean $hashValid
     */
    public function checkHash($hash, $checkSuccess = false)
    {
        $password = $checkSuccess ? $this->mrh_pass2 :$this->mrh_pass1;
        $hashGenerated = strtoupper(md5("$this->out_summ:$this->inv_id:$password:Shp_item=$this->shp_item"));
        return (strtoupper($hash) == $hashGenerated);
    }

    public function getHash()
    {
        $hashGenerated = strtoupper(md5("$this->out_summ:$this->inv_id:$this->mrh_pass2:Shp_item=$this->shp_item"));
        return $hashGenerated;
    }
    /**
     * Проверка завершения операции (проверка оплаты). Сравнение хеша
     *
     * @param string $hash значение SignatureValue, переданное кассой на Result URL
     * @return boolean $hashValid
     */
    public function checkSuccess($hash) {
        return $this->checkHash($hash, true);
    }
    /**
     * Получение строки с пользовательскими данными для шифрования
     *
     * @param boolean $url генерация строки для использования в URL true/false
     * @return string
     */
}
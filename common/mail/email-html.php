<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = $_SERVER['SERVER_NAME'].'/invite?token='.$user->password_reset_token;
?>
<div class="password-reset">
    <p>Для подтверждения, пройдите по ссылке</p>

    <?=$user->comment?>


    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
</div>

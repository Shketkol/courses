<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['/admin/users/users/update', 'id' => $user->id]);
?>
<div class="password-reset">
    <p>Добрый день <?= Html::encode($admin->name) ?>,</p>

    <p>Зарегистрировался новый ученик <?=$user->name?> <?=$user->usersProfile->surname?></p>

    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
</div>

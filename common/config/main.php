<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'db' => require(dirname(__DIR__) . "/config/db.php"),

        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
//            'transport' => [
//                'class' => 'Swift_SmtpTransport',
//                'host' => 'smtp.gmail.com',
//                'username' => 'webra.dev@gmail.com',
//                'password' => 'webra.devwebra.dev',
//                'port' => '587',
//                'encryption' => 'tls',
//            ],
            'useFileTransport' => false,
        ],
    ],

    'modules' => [
        'yii2images' => [
            'class' => 'shketkol\images\src\Module',
            'imagesStorePath' => $_SERVER['DOCUMENT_ROOT'].'/frontend/web/uploads/store', //path to origin images
            'imagesCachePath' => $_SERVER['DOCUMENT_ROOT'].'/frontend/web/uploads/cache', //path to resized copies
            'imagesTempPath' => $_SERVER['DOCUMENT_ROOT'].'/frontend/web/uploads/file', //path to resized copies
            'pathImageOrigin' => '/uploads/store',
            'graphicsLibrary' => 'GD', //but really its better to use 'Imagick'
            'userId' => 1 // userId save previews false user login id
        ],
    ],
];

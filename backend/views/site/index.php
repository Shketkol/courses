<?php

use common\models\Statistic;use yii\bootstrap\ActiveForm;
use yii\grid\GridView;
use \yii\helpers\Html;
use \yii\helpers\Url;
use \common\models\Users;

/* @var $this yii\web\View */

$this->title = 'Курсы';

$this->registerJsFile('@web/public/vendor/raphael.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/public/vendor/morris.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/public/vendor/Chart.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

?>
<section class="content" style="margin: 46px 15px 0 0px;">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="body" style="display: flow-root;">
                        <?$form = ActiveForm::begin([
                            'action' =>   Url::to(['/site/index']),
                            'method' => 'get',
                            'options' => [
                                'class' => ''
                            ]
                        ]); ?>
                        <?=$form->errorSummary($modelForm)?>
                        <div class="col-md-4">
                            <fieldset class="">
                                <div class="form-line">
                                    <label for="exampleInputEmail1">Год</label>
                                    <!--                                <input type="text" class="form-control" name="year" required value="--><?//=$year?><!--">-->
                                    <?=$form->field($modelForm, 'year')->textInput(['required' => true,  'value' => $year])->label(false)->error(false)?>
                                </div>
                            </fieldset>
                        </div>

                        <div class="col-md-4">
                            <label for="">Месяц</label>
                            <?=$form->field($modelForm, 'month')->dropDownList(Statistic::$monthArr, ['value' => $month])->label(false)->error(false)?>
                        </div>

                        <div class="col-md-4" style="margin-top: 26px;">
                            <button type="submit" class="btn btn-primary">Применить</button>
                        </div>
                    <? ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h2>Статистика</h2>

            <div class="card">
                <div class="body" style="overflow: auto">
                    <div class="chart" id="line"
                         style="width: 100%; height: auto; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



<?
$script = <<< JS
  (function ($) {

   var models = $models;   
      
   var data = [];
   for (var i in models){
       var obj = {
       y: models[i]['y'],
       a: models[i]['a'],
       b: models[i]['b'],
       c: models[i]['c'],
       d: models[i]['d'],
       e: models[i]['e'],
       }
       data.push(obj);
   }
      
  if ($('#line').length) {
    new Morris.Line({
      element: 'line',
      data: data,
      xkey: 'y',
      xLabels: "week",
      // xLabelFormat: function (x) { return x.toString(); },
      ykeys: ['a','b','c','d','e'],
      labels: [
          'Количество учеников', 
          'Количество оплат',
          'Количество регистраций',
          'Сумма оплат',
          'Количество заказов',
          ],
      resize: true
    });
  }
}(jQuery));

JS;
$this->registerJs($script, yii\web\View::POS_END);
?>




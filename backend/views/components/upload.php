<?php use shketkol\images\widget\FileInput;?>

<?=$form->field($model, $field.'[]')->widget(FileInput::classname(),
    [
        'options' => ['accept' => 'image/*',
            'multiple' => true,
        ],
        'pluginOptions' => [
            'allowedExtensions' => ['jpg', 'gif', 'png'],
            'initialPreview' => $model->getPreviews($name),
            'initialPreviewAsData' => true,
            'initialPreviewConfig' => $model->getPreviewsConfig($name),
            'overwriteInitial' => false,
            'previewFileType' => 'image',
            'showCaption' => false,
            'showCancel' => false,
            'showUpload' => false,
            'browseClass' => 'btn btn-default btn-sm',
            'browseLabel' => 'Добавить картинку',
            'browseIcon' => '<i class="glyphicon glyphicon-picture"></i>',
            'removeClass' => 'btn btn-danger btn-sm',
            'removeLabel' => ' Удалить',
            'removeIcon' => '<i class="fa fa-trash"></i>',
            'append' => true,
            'maxFileCount' =>\common\helpers\Images::$params[$name]['max'],
            'validateInitialCount' => true,
            'minImageHeight' => 120,
            'minImageWidth' => 120,

            'uploadUrl' => '/yii2images/upload/upload',
            'uploadAsync' => true,
            'uploadExtraData' => [
                'model' => $model::className(),
                'id' => ($model->isNewRecord) ? '' : $model->id,
                'name' => $name,
                'field' => $field,
                'max' => 50
            ],

        ],
        'pluginEvents' => [
            'filebatchselected' => 'function(event, data) {
                    jQuery("#catalogitems-image").fileinput("upload");
                }',
            'filesorted' => 'function(event, params) {
                    
                   for(var i in params.stack){
                        var index = parseInt(i) + parseInt(1);
                        $.ajax({
                            type: "GET",
                            url: "/yii2images/upload/change-position",
                            data: "id="+params.stack[i].key+"&position="+index,
                            success: function(data) {
                                                    
                            }
                    });
                   }
                    
                    
                }',

        ]
    ]) ?>
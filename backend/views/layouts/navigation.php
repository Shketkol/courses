<?php
use common\models\Users;
?>
<!-- #Top Bar -->
<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <!-- User Info -->
        <div class="user-info">
            <div class="image">
<!--                <img src="../../images/user.png" width="48" height="48" alt="User" />-->
            </div>
            <div class="info-container">
                <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?=Yii::$app->user->identity->name?></div>
                <div class="email"><?=Yii::$app->user->identity->email?></div>
                <div class="btn-group user-helper-dropdown">
                    <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                    <ul class="dropdown-menu pull-right">
                        <li><a href="<?=\yii\helpers\Url::to(['/users/profile/profile'])?>"><i class="material-icons">person</i>Профиль</a></li>
                        <li role="seperator" class="divider"></li>
                        <li role="seperator" class="divider"></li>
                        <li><a href="/admin/logout"><i class="material-icons">input</i>Выход</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- #User Info -->
        <!-- Menu -->
        <div class="menu">
            <ul class="list">
                <li class="header">НАВИГАЦИЯ</li>
                <? if (Yii::$app->user->identity->users_roles_id != Users::TYPE_TEACHER) : ?>
                    <li>
                        <a href="<?=\yii\helpers\Url::to(['/site/index'])?>">
                            <i class="material-icons">trending_up</i>
                            <span>Статистика</span>
                        </a>
                    </li>
                <?endif;?>
                <li>
                    <a href="<?=\yii\helpers\Url::to(['/courses/courses/index'])?>">
                        <i class="material-icons">import_contacts</i>
                        <span>Курсы</span>
                    </a>
                </li>
                <? if (Yii::$app->user->identity->users_roles_id != Users::TYPE_TEACHER) : ?>
                    <li>
                        <a href="<?=\yii\helpers\Url::to(['/groups/groups/index'])?>">
                            <i class="material-icons">person_pin</i>
                            <span>Группы</span>
                        </a>
                    </li>
                <?endif;?>
                <? if (Yii::$app->user->identity->users_roles_id != Users::TYPE_TEACHER) : ?>
                    <li>
                        <a href="<?=\yii\helpers\Url::to(['/teachers/teachers/index'])?>">
                            <i class="material-icons">account_box</i>
                            <span>Преподаватели</span>
                        </a>
                    </li>
                <?endif;?>
                <li>
                    <a href="javascript:void(0);" class="menu-toggle waves-effect waves-block">
                        <i class="material-icons">comment</i>
                        <span>Ответы</span>
                    </a>
                    <ul class="ml-menu" style="display: none;">
                        <li>
                            <a href="<?=\yii\helpers\Url::to(['/answers/answers/index', 'type' => \common\models\Courses::TYPE_SINGLE])?>" class="toggled waves-effect waves-block">
                                Одиночные курсы</a>
                        </li>
                        <li>
                            <a href="<?=\yii\helpers\Url::to(['/answers/answers/index', 'type' => \common\models\Courses::TYPE_GROUP])?>" class="toggled waves-effect waves-block">
                                Групповые курсы</a>
                        </li>
                        <?if (Yii::$app->user->identity->users_roles_id != \common\models\Users::TYPE_TEACHER):?>
                            <li>
                                <a href="<?=\yii\helpers\Url::to(['/answers/answers/index', 'type' => \common\models\Courses::TYPE_TEACHER])?>" class="toggled waves-effect waves-block">
                                    Курсы для преподавателей</a>
                            </li>
                        <?endif;?>
                    </ul>
                </li>
                <li>
                    <a href="<?=\yii\helpers\Url::to(['/pupils/pupils/index'])?>">
                        <i class="material-icons">person</i>
                        <span>Ученики</span>
                    </a>
                </li>
                <? if (Yii::$app->user->identity->users_roles_id != Users::TYPE_TEACHER) : ?>
                    <li>
                        <a href="<?=\yii\helpers\Url::to(['/users/users/index'])?>">
                            <i class="material-icons">face</i>
                            <span>Пользователи</span>
                        </a>
                    </li>
                <?endif;?>
                <? if (Yii::$app->user->identity->users_roles_id != Users::TYPE_TEACHER) : ?>
                    <li>
                        <a href="<?=\yii\helpers\Url::to(['/city/city/index'])?>">
                            <i class="material-icons">location_city</i>
                            <span>Города</span>
                        </a>
                    </li>
                <?endif;?>
                <li>
                    <a href="<?=\yii\helpers\Url::to(['/intent/reportes/index'])?>">
                        <i class="material-icons">assignment</i>
                        <span>Отчеты</span>
                    </a>
                </li>
                <li>
                    <a href="<?=\yii\helpers\Url::to(['/invoices/invoices/index'])?>">
                        <i class="material-icons">payment</i>
                        <span>Оплаты</span>
                    </a>
                </li>
                <?if(Yii::$app->user->identity->users_roles_id == 2):?>
                    <li>
                        <a href="<?=\yii\helpers\Url::to(['/mycourses/courses/index'])?>">
                            <i class="material-icons">book</i>
                            <span>Мои курсы</span>
                        </a>
                    </li>
                <?endif;?>
                <li>
                    <a href="/admin/logout">
                        <i class="material-icons">lock_open</i>
                        <span>Выход</span>
                    </a>
                </li>
            </ul>
        </div>
        <!-- #Menu -->
        <!-- Footer -->
        <div class="legal">
            <div class="copyright">
                &copy; 2016 - <?=date('Y', time())?> <a href="javascript:void(0);">Поколение Лидеров</a>.
            </div>
        </div>
        <!-- #Footer -->
    </aside>
    <!-- #END# Left Sidebar -->
    <!-- Right Sidebar -->
</section>


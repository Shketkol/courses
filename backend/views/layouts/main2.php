<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use backend\assets\MainAsset;
use backend\assets\HeadAsset;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

HeadAsset::register($this);
MainAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="top-navbar">
<?php $this->beginBody() ?>


<?= $this->render('header') ?>

<div class="mdk-drawer-layout mdk-js-drawer-layout" push has-scrolling-region>
    <div class="mdk-drawer-layout__content">
        <div class="container-fluid">

            <?
            echo Breadcrumbs::widget([
                'options' => ['class' => 'breadcrumb'],
                'homeLink' => ['label' => 'Главная панель', 'url' => ['/']],
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]);
            ?>

            <?= $content ?>
        </div>
    </div>

    <?= $this->render('navigation') ?>

</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

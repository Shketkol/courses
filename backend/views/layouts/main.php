<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use backend\assets\MainAsset;
use backend\assets\HeadAsset;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

HeadAsset::register($this);
MainAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<style>
    .navbar-brand {
        padding: 0px 0px; !important;
    }
</style>
<body class="theme-red">
<?php $this->beginBody() ?>

<!-- Page Loader -->
<!--<div class="page-loader-wrapper">-->
<!--    <div class="loader">-->
<!--        <div class="preloader">-->
<!--            <div class="spinner-layer pl-red">-->
<!--                <div class="circle-clipper left">-->
<!--                    <div class="circle"></div>-->
<!--                </div>-->
<!--                <div class="circle-clipper right">-->
<!--                    <div class="circle"></div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--        <p>Please wait...</p>-->
<!--    </div>-->
<!--</div>-->
<!-- #END# Page Loader -->


<?= $this->render('header') ?>

<?= $this->render('navigation') ?>

<!--<div class="mdk-drawer-layout mdk-js-drawer-layout" push has-scrolling-region>-->
<!--    <div class="mdk-drawer-layout__content">-->
<!--        <div class="container-fluid">-->
<!---->
<!--            --><?//
//            echo Breadcrumbs::widget([
//                'options' => ['class' => 'breadcrumb'],
//                'homeLink' => ['label' => 'Главная панель', 'url' => ['/']],
//                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
//            ]);
//            ?>
<!---->
<!--            --><?//= $content ?>
<!--        </div>-->
<!--    </div>-->

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <?=(isset($this->blocks['navBlock']))?$this->blocks['navBlock']:'' ?>
                <?
                echo Breadcrumbs::widget([
                    'options' => ['class' => 'breadcrumb'],
                    'homeLink' => ['label' => 'Главная панель', 'url' => ['/']],
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]);
                ?>
            </div>
        </div>

        <?= $content ?>
    </section>


<!--</div>-->
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

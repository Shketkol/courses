<!-- Sidebar -->
<div class="mdk-drawer mdk-js-drawer" id="default-drawer">
    <div class="mdk-drawer__content ">
        <div class="sidebar sidebar-left sidebar-light sidebar-transparent-sm-up sidebar-p-y">
            <ul class="sidebar-menu">
                <li class="sidebar-menu-item">
                    <a class="sidebar-menu-button" href="<?=\yii\helpers\Url::to(['/site/index'])?>">
                        <i class="sidebar-menu-icon material-icons">trending_up</i> Статискика
                    </a>
                </li>
                <li class="sidebar-menu-item">
                    <a class="sidebar-menu-button" href="<?=\yii\helpers\Url::to(['/courses/courses/index'])?>">
                        <i class="sidebar-menu-icon material-icons">import_contacts</i> Курсы
                    </a>
                </li>
                <li class="sidebar-menu-item">
                    <a class="sidebar-menu-button" href="<?=\yii\helpers\Url::to(['/teachers/teachers/index'])?>">
                        <i class="sidebar-menu-icon material-icons">account_box</i> Преподаватели
                    </a>
                </li>
                <li class="sidebar-menu-item">
                    <a class="sidebar-menu-button" href="<?=\yii\helpers\Url::to(['/answers/answers/index'])?>">
                        <i class="sidebar-menu-icon material-icons">comment</i> Ответы
                    </a>
                </li>
                <li class="sidebar-menu-item">
                    <a class="sidebar-menu-button" href="<?=\yii\helpers\Url::to(['/pupils/pupils/index'])?>">
                        <i class="sidebar-menu-icon material-icons">person</i> Ученики
                    </a>
                </li>

                <li class="sidebar-menu-item">
                    <a class="sidebar-menu-button" href="<?=\yii\helpers\Url::to(['/users/users/index'])?>">
                        <i class="sidebar-menu-icon material-icons">face</i> Пользователи
                    </a>
                </li>
                <li class="sidebar-menu-item">
                    <a class="sidebar-menu-button" href="<?=\yii\helpers\Url::to(['/city/city/index'])?>">
                        <i class="sidebar-menu-icon material-icons">location_city</i> Города
                    </a>
                </li>
                <li class="sidebar-menu-item">
                    <a class="sidebar-menu-button" href="/admin/logout">
                        <i class="sidebar-menu-icon material-icons">lock_open</i> Выход
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>

<!-- // END Sidebar -->
<!-- Navbar -->
<nav class="navbar navbar-dark bg-primary navbar-full navbar-fixed-top">

    <!-- Toggle sidebar -->
    <button class="navbar-toggler" type="button" data-toggle="sidebar"></button>

    <!-- Brand -->
    <a href="<?=\yii\helpers\Url::to(['/site/index'])?>" class="navbar-brand"><i class="material-icons">school</i> LearnPlus</a>

    <div class="navbar-spacer"></div>

    <!-- Menu -->
    <ul class="nav navbar-nav">
        <!-- User dropdown -->
        <li class="nav-item dropdown">
            <a class="nav-link active dropdown-toggle" data-toggle="dropdown" href="instructor-courses.html#" role="button" aria-haspopup="false">
                <?=Yii::$app->user->identity->name?>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="Preview">
<!--                <a class="dropdown-item" href="instructor-account-edit.html">-->
<!--                    <i class="material-icons">edit</i> Редактировать профиль-->
<!--                </a>-->
                <a class="dropdown-item" href="/admin/logout">
                    <i class="material-icons">lock</i> Выход
                </a>
            </div>
        </li>
        <!-- // END User dropdown -->
    </ul>
</nav>
<!-- // END Navbar -->
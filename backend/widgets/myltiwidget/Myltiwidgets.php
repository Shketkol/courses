<?php

namespace backend\widgets\myltiwidget;

use backend\widgets\myltiwidget\assets\MyltiAsset;
use yii\bootstrap\Widget;
use Yii;
use yii\web\View;

class Myltiwidgets extends Widget{

    public $model;
    public $attribute;
    public $fields;
    public $form;

    public $name;


    public function init()
    {
        Yii::$app->view->on(View::EVENT_BEFORE_RENDER, function ($event) {
            MyltiAsset::register($event->sender);
        });
        parent::init();


    }

    public function run(){

        $model = $this->model;
        $model_temp = explode('\\', $model::className());
        $model_class = $model_temp[count($model_temp)-1];

        return $this->render("myltiwidgets/index", array(
            'form' => $this->form,
            'name' => $this->name,
            'attribute' => $this->attribute,
            'model' => $this->model,
            'fields' => $this->fields,
            'model_class' => $model_class
        ));
    }



}
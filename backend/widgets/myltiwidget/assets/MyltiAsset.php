<?php

namespace backend\widgets\myltiwidget\assets;


use yii\web\AssetBundle;

class MyltiAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $js = [
        'admin/source/js/mylti.js',
    ];
    public $depends = [
        'yii\web\YiiAsset'
    ];
}

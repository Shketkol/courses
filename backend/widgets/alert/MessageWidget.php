<?php
namespace backend\widgets\alert;

use backend\widgets\alert\assets\AlertAsset;
use Yii;
use yii\base\Widget;
use yii\web\View;

class MessageWidget extends Widget
{
    public $alertTypes = [
        'a_basic_message' => 'sa-basic',
        'title_with_a_text_under' => 'sa-basic',
        'success_message' => 'sa-success',
        'warning_mesage' => 'sa-warning',
    ];

    public $body = array();

    public function init()
    {
        Yii::$app->view->on(View::EVENT_BEFORE_RENDER, function ($event) {
            AlertAsset::register($event->sender);
        });


        parent::init();

        $session = Yii::$app->session;
        $flashes = $session->getAllFlashes();

        $body = array();
        foreach ($flashes as $type => $data) {
            if (isset($this->alertTypes[$type])) {
                $data = (array) $data;
                $this->body[] = array(
                    'body' =>(isset($data['body'])) ? $data['body'] : null,
                    'type' => $this->alertTypes[$type],
                    'title' => $data['title'],
                    'desc' => (isset($data['desc'])) ? $data['desc'] : null
                );
                $session->removeFlash($type);
            }
        }
    }

    public function run()
    {
        return $this->render("message/popup", array(
            'model' => $this->body
        ));
    }
}

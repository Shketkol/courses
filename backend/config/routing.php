<?php
return [
    //users
    '/users' => 'users/users/index',
    '/users/create' => 'users/users/create',
    '/users/update' => 'users/users/update',
    '/users/delete' => 'users/users/delete',
    '/users/invite' => 'users/users/invite',

    'courses' => 'courses/courses/index',

    'teachers' => 'teachers/teachers/index',

    'answers' => 'teachers/answers/index',

    'lessons/<id:[\d]+>' => 'lessons/lessons/index',

    'tasks/<id:[\d]+>' => 'tasks/tasks/index',

    '/logout' => 'site/logout',

    '/' => '/courses/courses/index'
];

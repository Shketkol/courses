<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'language' => 'ru',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-backend',
            'baseUrl' => '/admin',
            'enableCsrfValidation' => false,
        ],
        'user' => [
            'identityClass' => 'common\models\Users',
            'enableAutoLogin' => true,
            'loginUrl' => '/login',
            'identityCookie' => ['name' => '_identity-site', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => require(__DIR__ . '/routing.php'),
        ],

        'assetManager' => [
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'js' => ['/static/plugins/jquery/jquery.min.js']
                ],
            ],
        ],

    ],
    'modules' => [
        'users' => [
            'class' => 'backend\modules\users\Module',
        ],
        'config' => [
            'class' => 'backend\modules\config\Module',
        ],
        'courses' => [
            'class' => 'backend\modules\courses\Module',
        ],
        'lessons' => [
            'class' => 'backend\modules\lessons\Module',
        ],
        'tasks' => [
            'class' => 'backend\modules\tasks\Module',
        ],
        'teachers' => [
            'class' => 'backend\modules\teachers\Module',
        ],
        'answers' => [
            'class' => 'backend\modules\answers\Module',
        ],
        'pupils' => [
            'class' => 'backend\modules\pupils\Module',
        ],
        'city' => [
            'class' => 'backend\modules\city\Module',
        ],
        'intent' => [
            'class' => 'backend\modules\intent\Module',
        ],
        'invoices' => [
            'class' => 'backend\modules\invoices\Module',
        ],
        'mycourses' => [
            'class' => 'backend\modules\mycourses\Module',
        ],
        'groups' => [
            'class' => 'backend\modules\groups\Module',
        ],
    ],
    'params' => $params,
];

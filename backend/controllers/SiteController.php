<?php
namespace backend\controllers;

use backend\components\controllers\AdminController;
use backend\models\UsersRaitingSearch;
use common\models\Invoices;
use common\models\Leads;
use common\models\Statistic;
use common\models\Users;
use Yii;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;

/**
 * Site controller
 */
class SiteController extends AdminController
{
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent:: beforeAction($action);
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->redirect('/login');
    }
    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
//        if (empty($_GET['month'])){
//            $month = date('m', time());
//            $year = date('Y', time());
//        } else {
//            $month = $_GET['month'];
//            $year = $_GET['year'];
//        }


        $modelForm = new Statistic();
        if ($modelForm->load(Yii::$app->request->get())){
            if (!$modelForm->validate()){
                $models = [];
                $month = date('m', time());
                $year = date('Y', time());
                for($i = 1; $i <= date('t', strtotime($year.'-'.$month.'-1 00:00:00')); $i++){
                    $arr = [
                        'y' => $year.'-'.$month.'-'.$i,
                        'a' => $this->getCountPupils($year.'-'.$month.'-'.$i.' 00:00:01', $year.'-'.$month.'-'.$i.' 23:59:59'),
                        'b' => $this->getCountPay($year.'-'.$month.'-'.$i.' 00:00:01', $year.'-'.$month.'-'.$i.' 23:59:59'),
                        'c' => $this->getCountRegister($year.'-'.$month.'-'.$i.' 00:00:01', $year.'-'.$month.'-'.$i.' 23:59:59'),
                        'd' => $this->getSumPay($year.'-'.$month.'-'.$i.' 00:00:01', $year.'-'.$month.'-'.$i.' 23:59:59'),
                        'e' => $this->getCountInvoices($year.'-'.$month.'-'.$i.' 00:00:01', $year.'-'.$month.'-'.$i.' 23:59:59'),
                    ];
                    $models[] = $arr;
                }
                return $this->render('index', [
                    'models' => json_encode($models),
                    'year' => $year,
                    'month' => $month,
                    'modelForm' => $modelForm
                ]);
            } else {
                $month = $modelForm->month;
                $year = $modelForm->year;
            }
        } else {
            $month = date('m', time());
            $year = date('Y', time());
        }

        $models = [];
        for($i = 1; $i <= date('t', strtotime($year.'-'.$month.'-1 00:00:00')); $i++){
            $arr = [
                'y' => $year.'-'.$month.'-'.$i,
                'a' => $this->getCountPupils($year.'-'.$month.'-'.$i.' 00:00:01', $year.'-'.$month.'-'.$i.' 23:59:59'),
                'b' => $this->getCountPay($year.'-'.$month.'-'.$i.' 00:00:01', $year.'-'.$month.'-'.$i.' 23:59:59'),
                'c' => $this->getCountRegister($year.'-'.$month.'-'.$i.' 00:00:01', $year.'-'.$month.'-'.$i.' 23:59:59'),
                'd' => $this->getSumPay($year.'-'.$month.'-'.$i.' 00:00:01', $year.'-'.$month.'-'.$i.' 23:59:59'),
                'e' => $this->getCountInvoices($year.'-'.$month.'-'.$i.' 00:00:01', $year.'-'.$month.'-'.$i.' 23:59:59'),
            ];
            $models[] = $arr;
        }



//        var_dump($models);die;
        return $this->render('index', [
            'models' => json_encode($models),
            'year' => $year,
            'month' => $month,
            'modelForm' => $modelForm
        ]);
    }


    protected function getCountPupils($start, $end)
    {
        $count = 0;
        $users = Users::find()
            ->where(['status' => Users::STATUS_ACTIVE])
            ->andWhere(['users_roles_id' => Users::TYPE_SCHOLAR])
            ->andWhere(['>=', 'created_at', $start])
            ->andWhere(['<=', 'created_at', $end])
            ->all();
        foreach ($users as $user){
            $pay = Invoices::find()
                ->where(['users_id' => $user->id])
                ->andWhere(['status' => 3])
                ->one();
            if (!empty($pay)){
                $count++;
            }
        }

        return $count;
    }

    protected function getCountPay($start, $end)
    {
        $pay = Invoices::find()
            ->where(['status' => 3])
            ->andWhere(['>=', 'updated_at', $start])
            ->andWhere(['<=', 'updated_at', $end])
            ->count();
        return $pay;
    }

    protected function getCountRegister($start, $end)
    {
        $users = Users::find()
            ->where(['>=', 'created_at', $start])
            ->andWhere(['<=', 'created_at', $end])
            ->count();
        return $users;
    }

    protected function getSumPay($start, $end)
    {
        $pay = Invoices::find()
            ->select('SUM(price) as price')
            ->where(['status' => 3])
            ->andWhere(['>=', 'updated_at', $start])
            ->andWhere(['<=', 'updated_at', $end])
            ->asArray()
            ->one();
        return (!empty($pay['price'])) ? $pay['price'] : 0;
    }

    protected function getCountInvoices($start, $end)
    {
        $pay = Invoices::find()
            ->andWhere(['>=', 'updated_at', $start])
            ->andWhere(['<=', 'updated_at', $end])
            ->count();
        return $pay;
    }
}

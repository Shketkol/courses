<?php
namespace backend\components\grid;

use yii\grid\ActionColumn;
use yii\helpers\Html;

class CustomActionColumn extends ActionColumn
{
    public $filter;

    protected function renderFilterCellContent()
    {
        return Html::a('<i class="ti-filter"></i> сброс', $this->filter, ['class' => 'reset_filter btn btn-sm btn-icon btn-info btn-outline m-t-5', 'data-toggle' => 'tooltip', 'data-original-title' => 'Сбросить фильтр']);
    }
}
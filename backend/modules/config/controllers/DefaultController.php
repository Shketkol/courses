<?php

namespace backend\modules\config\controllers;

use backend\components\controllers\AdminController;
use common\models\CityPeople;
use common\models\Config;
use common\models\ProductCitiesPercent;
use Yii;

/**
 * Default controller for the `config` module
 */
class DefaultController extends AdminController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $city = ProductCitiesPercent::find()->orderBy('city_people_id ASC, products_id ASC')->all();
        $model = Config::findOne(['id' => 1]);
        if($model->load(Yii::$app->request->post()) && $model->save()){
            return $this->redirect(['index']);
        }
        return $this->render('index', [
            'cities' => $city,
            'model' => $model
        ]);
    }

    public function actionConfigUpdate($id){
        $model = Config::findOne(['id' => $id]);
        if($model->load(Yii::$app->request->post()) && $model->save()){
            return $this->redirect(['index']);
        }
        return $this->render('create_config', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id){
        $model = ProductCitiesPercent::findOne(['id' => $id]);
        if($model->load(Yii::$app->request->post()) && $model->save()){
            return $this->redirect(['index']);
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionCreate(){
        $model = new ProductCitiesPercent();
        if($model->load(Yii::$app->request->post()) && $model->save()){
            return $this->redirect(['index']);
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }
}

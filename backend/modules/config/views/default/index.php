<?
use yii\grid\GridView;
use \yii\helpers\Html;
use \yii\helpers\Url;
use \common\models\Users;
use \yii\bootstrap\ActiveForm;


$this->title = 'Настройки';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginBlock('navBlock'); ?>
<? echo Html::a('<i class="fa fa-edit m-r-5"></i> Добавить коэффициент ', Url::to(['create']), [
    'class' => 'btn btn-success pull-right m-l-10 waves-effect waves-light'
]) ?>
<?php $this->endBlock(); ?>

<div class="row">
    <div class="col-md-12">
        <div class="panel white-box">
            <h1 class="box-title">Настройка коэффициентов для городов</h1>
            <table class="table table-hover manage-u-table">
                <thead>
                <tr>
                    <th>Население городов</th>
                    <th>Продукт</th>
                    <th>Значение, %</th>
                </tr>
                </thead>
                <tbody>
                <?foreach ($cities as $value){?>
                    <tr>
                        <td><?=$value->cityPeople->name?></td>
                        <td><?=$value->products->name?></td>
                        <td><a href="<?=Url::to(['/config/default/update', 'id' => $value->id])?>"><?=$value->value?></a></td>
                    </tr>
                <? } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel white-box">
            <h1 class="box-title">Настройка доступа к AmoCRM</h1>
            <table class="table table-hover manage-u-table">
                <thead>
                <tr>
                    <th>Название</th>
                    <th>Значение</th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Amo Domain</td>
                        <? $value = \common\models\Config::findOne(['key' => 'amo_domain'])?>
                        <td><a href="<?=Url::to(['/config/default/config-update', 'id' => $value->id])?>"><?=$value->value?></a></td>
                    </tr>
                    <tr>
                        <td>Amo Login</td>
                        <? $value = \common\models\Config::findOne(['key' => 'amo_login'])?>
                        <td><a href="<?=Url::to(['/config/default/config-update', 'id' => $value->id])?>"><?=$value->value?></a></td>
                    </tr>
                    <tr>
                        <td>Amo Hash</td>
                        <? $value = \common\models\Config::findOne(['key' => 'amo_hash'])?>
                        <td><a href="<?=Url::to(['/config/default/config-update', 'id' => $value->id])?>"><?=$value->value?></a></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>


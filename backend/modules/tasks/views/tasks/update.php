<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Leagues */

$this->title = 'Редактировать';
$this->params['breadcrumbs'][] = ['label' => 'Курсы', 'url' => ['/courses/courses/index']];
$this->params['breadcrumbs'][] = ['label' => $model->lessons->courses->title, 'url' => ['/courses/courses/view', 'id' => $model->lessons->courses->id]];
$this->params['breadcrumbs'][] = ['label' => $model->lessons->title, 'url' => ['/lessons/lessons/view', 'id' => $model->lessons->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">
        <?= $this->render('_form', [
            'model' => $model,
            'question' => $question,
            'options' => $options,
        ]) ?>
    </div>

</div> 
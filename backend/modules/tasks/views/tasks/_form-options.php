<?php

use yii\helpers\Html;
use wbraganca\dynamicform\DynamicFormWidget;

$js = ' jQuery("#dynamic-form .add-room").on("click", function(e, item) {
            
            jQuery("#dynamic-form .js_radio").each(function(i) {
                var for_label = $(this).attr("for");
                $(this).attr("for", for_label+i)
            });
            
            jQuery("#dynamic-form .js_format").each(function(i) {
                var for_label = $(this).val();
                $(this).attr("id", for_label)
                $(this).next().attr("for", for_label)
            });
            
            jQuery("#dynamic-form .js_radio_inp").each(function(i) {
            var id = $(this).attr("id");
                $(this).attr("id", id+i)
            });
            
            jQuery("#dynamic-form #questionstask-0-test input").each(function(i) {
                $(this).attr("id", "rad"+i)
                $(this).next("label").attr("for", "rad"+i)

            });
            
        });
               
        jQuery(".dynamicform_inner").on("afterInsert", function(e, item) {
            jQuery(".dynamicform_inner .room-item .js_radio_inp:last").prop("checked", true);
        });

        jQuery("#dynamic-form").on("afterDelete", function(e) {
            jQuery("#dynamic-form .js_radio").each(function(i) {
                var for_label = $(this).attr("for");
                $(this).attr("for", for_label+i)
            });
            jQuery("#dynamic-form .js_radio_inp").each(function(i) {
            var id = $(this).attr("id");
                $(this).attr("id", id+i)
            });
        });
        ';

$this->registerJs($js);

?>

<?// var_dump($modelsOptions);die;?>
<?php DynamicFormWidget::begin([
    'widgetContainer' => 'dynamicform_inner',
    'widgetBody' => '.container-rooms',
    'widgetItem' => '.room-item',
    'limit' => 20,
    'min' => 0,
    'insertButton' => '.add-room',
    'deleteButton' => '.remove-room',
    'model' => $modelsOptions[0],
    'formId' => 'dynamic-form',
    'formFields' => [
        'title',
        'correct'
    ],
]); ?>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Варианты ответов</th>
            <th class="text-center">
                <button type="button" class="add-room btn btn-success btn-xs"><span class="glyphicon glyphicon-plus"></span></button>
            </th>
        </tr>
        </thead>
        <tbody class="container-rooms">
        <?php foreach ($modelsOptions as $indexQuestion => $modelOptions): ?>
            <tr class="room-item">
                <td class="vcenter">
                    <?php
                    // necessary for update action.
                    if (! $modelOptions->isNewRecord) {
                        echo Html::activeHiddenInput($modelOptions, "[{$indexQuestion}][{$indexQuestion}]id");
                    }

                    ?>
                    <?= $form->field($modelOptions, "[{$indexQuestion}][{$indexQuestion}]correct")->radioList(array(
                        0 => 'неправильный ответ',
                        1 => 'правильный ответ'
                    ), ['required' => true,
                        'item' => function ($index, $label, $name, $checked, $value) use ($indexQuestion) {
                            return
                                Html::radio($name, $checked, ['class' => 'js_radio_inp', 'value' => $value, 'id' =>'radio_var_'.$name.$indexQuestion.'_'.$index]).'<label class="js_radio" for="radio_var_'.$name.$indexQuestion.'_'.$index.'">'.$label.'</label>';
                        },
                    ]) ?>
                    <?= $form->field($modelOptions, "[{$indexQuestion}][{$indexQuestion}]title")->label(false)->textInput(['maxlength' => true, 'required' => true]) ?>
                </td>
                <td class="text-center vcenter" style="width: 90px;">
                    <button type="button" class="remove-room btn btn-danger btn-xs"><span class="glyphicon glyphicon-minus"></span></button>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
<?php DynamicFormWidget::end(); ?>
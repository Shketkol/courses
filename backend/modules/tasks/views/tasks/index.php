<?
use yii\grid\GridView;
use \yii\helpers\Html;
use \yii\helpers\Url;

$this->title = 'Курсы';
$this->params['breadcrumbs'][] = $this->title;
?>
    <p>
        <?= Html::a('Добавить', ['create', 'id' => $id], ['class' => 'btn btn-success']) ?>
    </p>
<?php \yii\widgets\Pjax::begin(); ?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'summary' => false,
    'layout' => "{summary}\n{items}\n<div align='right'>{pager}</div>",
    'tableOptions' => [
        'class' => 'table table-hover manage-u-table'
    ],
    'columns' => [
        [
            'attribute'=>'id',
            'contentOptions' =>['class' => 'text-center'],
            'headerOptions' => ['class' => 'text-center','style'=>'width: 80px;']
        ],

        'title',
        [
            'header' => 'Управление',
            'class' => 'backend\components\grid\CustomActionColumn',
            'contentOptions' =>['class' => 'text-center'],
            'headerOptions' => ['class' => 'text-center', 'width' => '80'],
            'filterOptions' => ['class' => 'text-center'],
            'filter' => Url::to(['index']),
            'template' => '{update}{delete}',
            'buttons' => [
                'update' => function ($url, $model) {
                    return Html::a('<i class="ti-pencil" aria-hidden="true"></i>', Url::to(['update', 'id' => $model->id]), [
                        'data-original-title' => 'Редактировать',
                        'data-toggle' => 'tooltip',
                        'class' => 'btn btn-sm btn-icon btn-info btn-outline no-pjax'
                    ]);
                },
            ],
        ],
    ],
]); ?>
<?php \yii\widgets\Pjax::end(); ?>
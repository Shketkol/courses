<?php
$this->registerJsFile('@web/source/lib/ckeditorbigger/ckeditor.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

use yii\helpers\Html;
use yii\widgets\ActiveForm;
$js = '
        jQuery("#questionstask-0-test").ready(function(e, item) {
            jQuery("#radio_ans_1").prop("checked", true)
        });
        
        ';

$this->registerJs($js);
?>


<?php $form = ActiveForm::begin([
    'id' => 'dynamic-form',
    'options' => ['enctype' => 'multipart/form-data'],
    'fieldConfig' => [
        'template' => "{label}<div class=\"form-line\">{input}<span class=\"help-block hidden\"></span></div>",
    ],
]); ?>
<div class="card">
    <div class="header">
        <h4 class="card-title">Общая информация</h4>
    </div>
    <div class="body">
        <?=$form->errorSummary($model)?>
        <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'desc')->textarea(['rows' => 6, 'class' => 'useCKEditor']) ?>
    </div>
</div>
<div class="card">
    <div class="card">
        <h4 class="header">Вопрос</h4>
    </div>
    <div class="body">
        <div class="panel-body">
            <?php \wbraganca\dynamicform\DynamicFormWidget::begin([
                'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                'widgetBody' => '.container-items', // required: css class selector
                'widgetItem' => '.item', // required: css class
                'limit' => 1, // the maximum times, an element can be cloned (default 999)
                'min' => 1, // 0 or 1 (default 1)
                'insertButton' => '.add-item hidden', // css class
                'deleteButton' => '.remove-item hidden', // css class
                'model' => $question[0],
                'formId' => 'dynamic-form',
                'formFields' => [
                    'desc',
                    'format',
                    'ratitig',
                    'test',
                ],
            ]); ?>

            <div class="container-items"><!-- widgetContainer -->
                <?php foreach ($question as $i => $question_item): ?>
                    <div class="item panel panel-default card-block"><!-- widgetBody -->
                        <div class="panel-heading">
<!--                            <h3 class="panel-title pull-left">Вопрос</h3>-->
                            <div class="pull-right">
<!--                                <button type="button" class="add-item btn btn-success btn-xs"><i-->
<!--                                            class="glyphicon glyphicon-plus"></i></button>-->
<!--                                <button type="button" class="remove-item btn btn-danger btn-xs"><i-->
<!--                                            class="glyphicon glyphicon-minus"></i></button>-->
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="body">
                            <?php
                            // necessary for update action.
                            if (!$question_item->isNewRecord) {
                                echo Html::activeHiddenInput($question_item, "[{$i}]id");
                            }
                            ?>
                            <div class="row">
                                <?= $form->field($question_item, "[{$i}]desc")->textarea(['maxlength' => true]) ?>
                                <? if ($model->isNewRecord){
                                    $question_item->format = 1;
                                    $question_item->test = 2;
                                }?>
                                <?= $form->field($question_item, "[{$i}]format")->radioList(array(
                                    1 => 'текст',
                                    2 => 'несколько ответов',
                                    3 => 'файл',
                                    4 => 'один ответ',
                                    5 => 'выпадающий список',
                                ),[
                                        'required' => true,
                                    'item' => function ($index, $label, $name, $checked, $value) {
                                        return
                                            Html::radio($name, $checked, ['value' => $value, 'id' =>'radio_'.$value, 'class' => 'js_format']).'<label for="radio_'.$value.'">'.$label.'</label>';
                                    },

                                ]) ?>

                                <?= $this->render('_form-options', [
                                    'form' => $form,
                                    'indexQuestion' => $i,
                                    'modelsOptions' => $options[$i],
                                ]) ?>

                                <?= $form->field($question_item, "[{$i}]rating")->textInput(['maxlength' => true, 'required' => true, 'type' => 'number', 'min' => 0]) ?>

                                <?= $form->field($question_item, "[{$i}]test")->radioList(array(
                                    1 => 'Автоматически',
                                    2 => 'Вручную'
                                ),['required' => true,
                                    'item' => function ($index, $label, $name, $checked, $value) {
                                        return
                                        Html::radio($name, $checked, ['value' => $value, 'id' =>'rad'.$index]).'<label for="rad'.$index.'">'.$label.'</label>';
                                    },
                                ]) ?>
                            </div><!-- .row -->
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <?php \wbraganca\dynamicform\DynamicFormWidget::end(); ?>
        </div>
    </div>
</div>

<?if (Yii::$app->user->identity->users_roles_id != \common\models\Users::TYPE_TEACHER):?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
<?endif;?>
<?php ActiveForm::end(); ?>



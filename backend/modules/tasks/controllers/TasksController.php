<?php

namespace backend\modules\tasks\controllers;

use backend\components\controllers\AdminController;
use backend\models\TasksSearch;
use common\models\CorrectAnswer;
use common\models\Model;
use common\models\QuestionsTask;
use common\models\Tasks;
use common\models\VariantsAnswer;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

/**
 * Default controller for the `courses` module
 */
class TasksController extends AdminController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex($id)
    {
        $searchModel = new TasksSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $id);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'id' => $id
        ]);
    }

    public function actionCreate($id)
    {
        $model = new Tasks();
        $model->lessons_id = $id;
        $question = [new QuestionsTask];
        $options = [[new VariantsAnswer]];

        if (Yii::$app->request->post()) {
            $model->load(Yii::$app->request->post());
            $model->lessons_id = $id;

            $question = Model::createMultiple(QuestionsTask::classname());
            Model::loadMultiple($question, Yii::$app->request->post());

            if (empty($_POST['VariantsAnswer']) && ($question[0]->format != 1 && $question[0]->format != 3)){
                $model->addError('title', 'Нужно заполнить варианты ответов');
                return $this->render('update', [
                    'model' => $model,
                    'question' => $question,
                    'options' => (!empty($options)) ? $options : [[new VariantsAnswer()]]
                ]);
            }

            if ($model->save()){
                foreach ($question as $key => $item) {
                    $item->tasks_id = $model->id;
                    $item->save(false);

                    if (isset($_POST['VariantsAnswer'][$key]) && is_array($_POST['VariantsAnswer'][$key])) {
                        foreach ($_POST['VariantsAnswer'][$key] as $k => $value) {
                            $options = new VariantsAnswer();
                            $options->questions_task_id = $item->id;
                            $options->title = $value['title'];
                            $options->correct = $value['correct'];
                            $options->save(false);
                        }
                    }
                }
                return $this->redirect(Url::to(['/lessons/lessons/view', 'id' => $model->lessons_id]));
            }



        }

        return $this->render('create', [
            'model' => $model,
            'id' => $id,
            'question' => $question,
            'options' => $options,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
        $question = $model->questionsTasks;
        $options = [];

        if (!empty($question)) {
            foreach ($question as $key => $value) {
                $option = $value->variantsAnswer;
                if (!empty($option)) {
                    $options[$key] = $option;
                }
            }
        }

        if (Yii::$app->request->post()) {
//            var_dump($_POST['VariantsAnswer']);die;
            $model->load(Yii::$app->request->post());

            $question = Model::createMultiple(QuestionsTask::classname());
            Model::loadMultiple($question, Yii::$app->request->post());

            if (empty($_POST['VariantsAnswer'])){
                $model->addError('title', 'Нужно заполнить врианты ответов');
                return $this->render('update', [
                    'model' => $model,
                    'question' => $question,
                    'options' => (!empty($options)) ? $options : [[new VariantsAnswer()]]
                ]);
            }

            $model->save();

            QuestionsTask::deleteAll(['tasks_id' => $model->id]);

            foreach ($question as $key => $item) {
//                echo "<pre>";
//                var_dump($item);die;
//                echo "</pre>";
//                die;
                $item->tasks_id = $model->id;
                $item->save(false);

//                var_dump($_POST['VariantsAnswer']);die;
                if (isset($_POST['VariantsAnswer']) && is_array($_POST['VariantsAnswer'])) {
                    foreach ($_POST['VariantsAnswer'] as $k => $value) {
                        foreach ($value as $v){
                            $options = new VariantsAnswer();
                            $options->questions_task_id = $item->id;
                            $options->title = $v['title'];
                            $options->correct = $v['correct'];
                            $options->save(false);
                        }
                    }
                }
            }

            return $this->redirect(Url::to(['/lessons/lessons/view', 'id' => $model->lessons_id]));
        }
        return $this->render('update', [
            'model' => $model,
            'question' => $question,
            'options' => (!empty($options)) ? $options : [[new VariantsAnswer()]]
        ]);
    }

    public function actionDelete($id)
    {
        $model = $this->loadModel($id);
        $model->delete();
        return $this->redirect(Url::to(['/lessons/lessons/view', 'id' => $model->lessons_id]));
    }

    public function loadModel($id)
    {
        $model = Tasks::findOne(['id' => $id]);
        if ($model === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        return $model;
    }

    public function actionChangePosition()
    {
        if (Yii::$app->request->post()) {
            $id = Yii::$app->request->post('id');
            $position = Yii::$app->request->post('position');

            $type = Tasks::find()->where(['id' => $id])->one();
            $type->position = $position;
            $type->save();

            $models = Tasks::find()
                ->where('id <> :id', ['id' => $id])
                ->andWhere(['lessons_id' => $type->lessons_id])
                ->orderBy('position')
                ->all();
            foreach ($models as $model) {
                if ($model->position == $position) {
                    $model->position = (int)$model->position - 1;
                    $model->save();
                }
            }
            return $this->asJson(1);
        }
    }
}

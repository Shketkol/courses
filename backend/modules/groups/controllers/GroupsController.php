<?php

namespace backend\modules\groups\controllers;

use backend\components\controllers\AdminController;
use backend\models\GroupSearch;
use backend\models\InvoicesSearch;
use backend\models\ReportesSearch;
use backend\models\UserGroupSearch;
use Codeception\Platform\Group;
use common\models\Groups;
use common\models\Invoices;
use common\models\Reportes;
use common\models\UserGroups;
use yii\helpers\Url;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * Default controller for the `users` module
 */
class GroupsController extends AdminController
{
    public function actionIndex()
    {
        $searchModel = new GroupSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = new Groups();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->save()) {
                return $this->redirect('index');
            }
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->save()) {
            return $this->redirect('index');
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        $model = $this->loadModel($id);

        $model->delete();

        return $this->redirect('index');
    }

    public function actionView($id)
    {
        if (Yii::$app->request->post('id')){
            $ids = $_POST['id'];
            $group = $_POST['group'];
            if (!empty($ids)){
                foreach ($ids as $item){
                    $user = UserGroups::findOne(['id' => $item]);
                    $user->groups_id = $group;
                    $user->save();
                }
            }
        }
        $group = Groups::findOne(['id' => $id]);
        $searchModel = new UserGroupSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $id);

        return $this->render('view', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'group' => $group
        ]);
    }

    public function loadModel($id)
    {
        $model = Groups::findOne(['id' => $id]);
        if ($model === null) {
            $answer = json_decode(file_get_contents('php://input'), true);
            if (!empty($answer)) {
                return $this->asJson(array('status' => 'success', 'data' => array(
                    'type' => 'popup',
                    'name' => 'sa-basic',
                    'title' => 'Запись не найдена'
                )));
                exit;
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
        return $model;
    }

}




<?
use yii\grid\GridView;
use \yii\helpers\Html;
use \yii\helpers\Url;
use \common\models\Users;

$this->title = $group->name;
$this->params['breadcrumbs'][] = ['label' => 'Группы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?= Html::beginForm(['/groups/groups/view?id='.$group->id], 'POST', []) ?>
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Переместить пользователей в другую группу
                </h2>
            </div>
            <div class="body">

                    <div class="row clearfix">
<!--                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">-->
<!--                            <div class="form-group">-->
<!--                                <div class="form-line">-->
<!--                                    <input class="form-control" name="start" placeholder="С какого ID пользователя" type="text">-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">-->
<!--                            <div class="form-group">-->
<!--                                <div class="form-line">-->
<!--                                    <input class="form-control" name="end" placeholder="По какой ID пользователя" type="password">-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                            <div class="form-group">
                                <div class="form-line">
                                    <?=Html::dropDownList('group', 'null', \yii\helpers\ArrayHelper::map(\common\models\Groups::find()->all(), 'id', 'name'), ['class' => 'form-control'])?>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                            <button type="submit" class="btn btn-primary btn-lg m-l-15 waves-effect">Применить</button>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="table-responsive">
                <?php \yii\widgets\Pjax::begin(); ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
//                    'filterModel' => $searchModel,
                    'summary' => false,
                    'layout' => "{summary}\n{items}\n<div align='right'>{pager}</div>",
                    'tableOptions' => [
                        'class' => 'table table-hover manage-u-table'
                    ],
                    'columns' => [
                        [
                            'attribute'=>'id',
                            'contentOptions' =>['class' => 'text-center'],
                            'headerOptions' => ['class' => 'text-center','style'=>'width: 80px;']
                        ],
                        [
                            'label'=>'Пользователь',
                            'value' => function($data){
                                    return $data->user->name;
                            }
                        ],
                        [
                            'label' => 'Выбрать',
                            'format' => 'raw',
                            'value' => function ($data) {
                                    return Html::checkbox('id[]',false, ['value' => $data->id, 'class' => 'filled-in', 'id' => 'basic_checkbox_'.$data->id]).'<label for="basic_checkbox_'.$data->id.'">выбрать</label>';
                            },
                        ],
                ],
                ]); ?>

                <?php \yii\widgets\Pjax::end(); ?>
            </div>
        </div>
    </div>
</div>
<?= Html::endForm() ?>

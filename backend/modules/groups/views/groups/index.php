<?
use yii\grid\GridView;
use \yii\helpers\Html;
use \yii\helpers\Url;
use \common\models\Users;

$this->title = 'Группы';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $this->beginBlock('navBlock'); ?>
<? echo Html::a('<i class="fa fa-plus m-r-5"></i> Добавить группу ', Url::to(['create']), [
    'class' => 'btn btn-success pull-right m-l-10 waves-effect waves-light'
]) ?>
<?php $this->endBlock(); ?>

<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="table-responsive">
                <?php \yii\widgets\Pjax::begin(); ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
//                    'filterModel' => $searchModel,
                    'summary' => false,
                    'layout' => "{summary}\n{items}\n<div align='right'>{pager}</div>",
                    'tableOptions' => [
                        'class' => 'table table-hover manage-u-table'
                    ],
                    'columns' => [
                        [
                            'attribute'=>'id',
                            'contentOptions' =>['class' => 'text-center'],
                            'headerOptions' => ['class' => 'text-center','style'=>'width: 80px;']
                        ],
                        [
                            'attribute'=>'name',
                        ],
                        [
                            'label'=>'Курс',
                            'value' => function($model){
                                return $model->courses->title;
                            },
                        ],
                        [
                            'header' => 'Управление',
                            'class' => 'backend\components\grid\CustomActionColumn',
                            'contentOptions' =>['class' => 'text-center'],
                            'headerOptions' => ['class' => 'text-center', 'width' => '80'],
                            'filterOptions' => ['class' => 'text-center'],
//                            'filter' => Url::to(['index']),
                            'template' => '{update}{view}',
                            'buttons' => [
                            ]
                        ],
                ],
                ]); ?>
                <?php \yii\widgets\Pjax::end(); ?>
            </div>
        </div>
    </div>
</div>

<?php

namespace backend\modules\groups;

use frontend\modules\users\assets\UsersAsset;
use Yii;
use yii\web\View;

/**
 * users module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'backend\modules\groups\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}

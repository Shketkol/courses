<?php
use \yii\bootstrap\ActiveForm;
use \yii\helpers\Url;
use \yii\helpers\Html;
use \common\models\Users;

$this->title = 'Оплаты';
$this->params['breadcrumbs'][] = (!empty($model->courses_id))?$model->courses->title :'Оплаты';
?>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <? $form = ActiveForm::begin([
                'id' => 'users-update',
                'enableClientValidation'=> false,
                'validateOnSubmit' => true,
//                'action' => Url::to(['update', 'id' => $model->id]),
                'options' => [
//                    'class' => 'form-horizontal',
                    'autocomplete' => 'off',
                ],
                'fieldConfig' => [
                    'template' => "{label}<div class=\"form-line\">{input}<span class=\"help-block hidden\"></span></div>",
                    'labelOptions' => ['class' => 'col-md-12'],
                ],
            ]); ?>
            <div class="header">
                <h3 class="box-title m-b-0">Оплата</h3>
            </div>
            <div class="body">
                <div class="row">
                    <div class="col-md-12">
                        <?=$form->errorSummary($model);?>
                        <?= $form->field($model, 'users_id')->dropDownList(
                            \yii\helpers\ArrayHelper::map(\common\models\Users::find()->orderBy('name')->all(), 'id', 'email'),
                            array('prompt' => 'Выберите значение', 'required' => true)
                        ) ?>
                        <?= $form->field($model, 'courses_id')->dropDownList(
                            \yii\helpers\ArrayHelper::map(\common\models\Courses::find()->orderBy('title')->where(['not', ['status' => 0]])->all(), 'id', 'title'),
                            array('prompt' => 'Выберите значение', 'required' => true)
                        ) ?>
                        <?= $form->field($model, 'status')->dropDownList(
                            [
                                    1 => 'Новый',
                                    2 => 'Неоплачен',
                                    3 => 'Оплачен',
                            ],
                            array('prompt' => 'Выберите значение', 'required' => true)
                        ) ?>

                        <button type="submit" class="btn btn-lg btn-success waves-effect waves-light m-t-10 m-r-10"><i class="fa fa-spin fa-circle-o-notch hidden"></i>&nbsp; Сохранить &nbsp;</button>
                    </div>
                </div>


            </div>


            <? ActiveForm::end(); ?>
        </div>

    </div>
</div>
<?
use yii\grid\GridView;
use \yii\helpers\Html;
use \yii\helpers\Url;
use \common\models\Users;

$this->title = 'Оплаты';
$this->params['breadcrumbs'][] = $this->title;
?>
<? if (Yii::$app->user->identity->users_roles_id != Users::TYPE_TEACHER) : ?>
    <?php $this->beginBlock('navBlock'); ?>
        <? echo Html::a('<i class="fa fa-plus m-r-5"></i> Добавить оплату ', Url::to(['create']), [
            'class' => 'btn btn-success pull-right m-l-10 waves-effect waves-light'
        ]) ?>
    <?php $this->endBlock(); ?>
<?php endif;?>

<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="table-responsive">
                <?php \yii\widgets\Pjax::begin(); ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
//                    'filterModel' => $searchModel,
                    'summary' => false,
                    'layout' => "{summary}\n{items}\n<div align='right'>{pager}</div>",
                    'tableOptions' => [
                        'class' => 'table table-hover manage-u-table'
                    ],
                    'columns' => [
                        [
                            'attribute'=>'id',
                            'contentOptions' =>['class' => 'text-center'],
                            'headerOptions' => ['class' => 'text-center','style'=>'width: 80px;']
                        ],
                        [
                            'label'=>'Пользователь',
                            'value' => function($model){
                                return $model->users->email;
                            },
                        ],
                        [
                            'label'=>'Курс',
                            'value' => function($model){
                                return $model->courses->title;
                            },
                        ],
                        [
                            'label'=>'Статус',
                            'value' => function($data){
                                if($data->status == 1){
                                    return 'Новый';
                                }
                                if($data->status == 2){
                                    return 'Неоплачен';
                                }
                                if($data->status == 3){
                                    return 'Оплачен';
                                }
                            }
                        ],
                        'created_at',
                        [
                            'header' => 'Управление',
                            'class' => 'backend\components\grid\CustomActionColumn',
                            'contentOptions' =>['class' => (Yii::$app->user->identity->users_roles_id == Users::TYPE_TEACHER) ? 'hidden' : 'text-center'],
                            'headerOptions' => ['class' => 'text-center', 'width' => '80'],
                            'filterOptions' => ['class' => 'text-center'],
//                            'filter' => Url::to(['index']),
                            'template' => '{update}{delete}',
                            'buttons' => [
                            ]
                        ],
                ],
                ]); ?>
                <?php \yii\widgets\Pjax::end(); ?>
            </div>
        </div>
    </div>
</div>

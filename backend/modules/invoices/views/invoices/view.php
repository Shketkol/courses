<?
use yii\grid\GridView;
use \yii\helpers\Html;
use \yii\helpers\Url;
use \common\models\Users;

$this->title = 'Отчеты';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $this->beginBlock('navBlock'); ?>
<? echo Html::a('<i class="fa fa-plus m-r-5"></i> Добавить отчет ', Url::to(['/intent/reportes/create', 'id' => $_GET['id']]), [
    'class' => 'btn btn-success pull-right m-l-10 waves-effect waves-light'
]) ?>
<?php $this->endBlock(); ?>

<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="table-responsive">
                <?php \yii\widgets\Pjax::begin(); ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
//                    'filterModel' => $searchModel,
                    'summary' => false,
                    'layout' => "{summary}\n{items}\n<div align='right'>{pager}</div>",
                    'tableOptions' => [
                        'class' => 'table table-hover manage-u-table'
                    ],
                    'columns' => [
                        [
                            'attribute'=>'id',
                            'contentOptions' =>['class' => 'text-center'],
                            'headerOptions' => ['class' => 'text-center','style'=>'width: 80px;']
                        ],
                        'date',
                        [
                            'label'=>'Статус',
                            'value' => function($data){
                                if($data->test == \common\models\Reportes::REPORT_NEW){
                                    return 'Не проверен';
                                }
                                if($data->test == \common\models\Reportes::REPORT_SUCCESS){
                                    return 'Проверен';
                                }
                                if($data->test == \common\models\Reportes::REPORT_WRONG){
                                    return 'Неверно';
                                }
                            }
                        ],
                        [
                            'label'=>'Вознаграждение',
                            'value' => function($data){
                                    return $data->balls;
                            }
                        ],
                        [
                            'header' => 'Управление',
                            'class' => 'backend\components\grid\CustomActionColumn',
                            'contentOptions' =>['class' => 'text-center'],
                            'headerOptions' => ['class' => 'text-center', 'width' => '80'],
                            'filterOptions' => ['class' => 'text-center'],
//                            'filter' => Url::to(['index']),
                            'template' => '{update}',
                            'buttons' => [
                                'update' => function ($model, $key, $index) {
                                    if($key['test'] == 0){
                                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Url::to(['/intent/reportes/update', 'id' => $key['id']]), [
                                            'data-original-title' => 'Редактировать',
                                            'data-toggle' => 'tooltip',
                                        ]);
                                    }
                                },
                            ]
                        ],
                ],
                ]); ?>
                <?php \yii\widgets\Pjax::end(); ?>
            </div>
        </div>
    </div>
</div>

<?php

namespace backend\modules\invoices\controllers;

use backend\components\controllers\AdminController;
use backend\models\InvoicesSearch;
use backend\models\ReportesSearch;
use common\models\Courses;
use common\models\Groups;
use common\models\Invoices;
use common\models\Reportes;
use common\models\UserGroups;
use yii\helpers\Url;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * Default controller for the `users` module
 */
class InvoicesController extends AdminController
{
    public function actionIndex()
    {
        $searchModel = new InvoicesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = new Invoices();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->price = 0;
            if ($model->save()) {
                if ($model->courses->type == Courses::TYPE_GROUP && $model->status == 3){
                    $group = Groups::findOne(['courses_id' => $model->courses_id]);
                    if (empty($group)){
                        $group = new Groups();
                        $group->name = $model->courses->title.' '.date('Y-m-d', time());
                        $group->courses_id = $model->courses_id;
                        $group->save();
                    }
                    $user = UserGroups::findOne(['groups_id' => $group->id, 'user_id' => $model->users_id]);
                    if (empty($user)){
                        $user = new UserGroups();
                        $user->user_id = $model->users_id;
                        $user->groups_id = $group->id;
                        $user->save();
                    }
                }

                return $this->redirect('index');
            }
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->save()) {

            if ($model->courses->type == Courses::TYPE_GROUP && $model->status == 3){
                $group = Groups::findOne(['courses_id' => $model->courses_id]);
                if (empty($group)){
                    $group = new Groups();
                    $group->name = $model->courses->name.' '.date('Y-m-d', time());
                    $group->courses_id = $model->courses_id;
                    $group->save();
                }
                $user = UserGroups::findOne(['groups_id' => $group->id, 'user_id' => $model->users_id]);
                if (empty($user)){
                    $user = new UserGroups();
                    $user->user_id = $model->users_id;
                    $user->groups_id = $group->id;
                    $user->save();
                }
            }

            return $this->redirect('index');
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        $model = $this->loadModel($id);

        $model->delete();

        return $this->redirect('index');
    }

    public function loadModel($id)
    {
        $model = Invoices::findOne(['id' => $id]);
        if ($model === null) {
            $answer = json_decode(file_get_contents('php://input'), true);
            if (!empty($answer)) {
                return $this->asJson(array('status' => 'success', 'data' => array(
                    'type' => 'popup',
                    'name' => 'sa-basic',
                    'title' => 'Запись не найдена'
                )));
                exit;
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
        return $model;
    }

}




<?

use shketkol\images\src\models\Image;

$user_task = \common\models\UsersAnswers::find()
    ->where(['tasks_id' => $model->id])
    ->andWhere(['users_id' => $user])
    ->one();

$this->title = 'Ответы пользователей';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="row">
    <div class="col-md-12">
        <h1 class="page-heading h2"><?= $lesson->title ?></h1>
        <div class="card">
            <div class="body">
                <?= $lesson->desc ?>
            </div>
            <div class="body">
                <?= $lesson->video ?>
            </div>
        </div>

        <div class="card">
            <div class="header bg-white p-1">
                <div class="media">
                    <div class="media-left media-middle">
                        <h4 class="mb-0"><strong>#<?= $model->position ?></strong></h4>
                    </div>
                    <div class="media-body  media-middle">
                        <h4 class="card-title">
                            <?= $model->title ?>
                        </h4>
                        <div class="card-body">
                            <?= $model->desc ?>
                        </div>
                    </div>
                </div>
            </div>
            <? foreach ($model->questionsTasks as $question) { ?>
                <div class="card-block p-2">
                    <div class="body">
                        <?= $question->desc ?>
                    </div>
                    <? if ($question->format == 1) { ?>
                        <fieldset class="form-group">
                            <label for="exampleTextarea">Ответ
                            <?if($user_task->correct == 1){?>
                                (правильно)
                            <? } elseif($user_task->correct == 0){?>
                                (неправильно)
                            <? }?>
                            </label>
                            <? $answer = \common\models\QuestionsTask::getAnswer($user, $question->id) ?>
                            <textarea class="form-control" id="exampleTextarea"
                                      rows="3"><?= (!empty($answer)) ? trim($answer->answer) : null ?></textarea>

                        </fieldset>
                    <? } ?>
                    <? if ($question->format == 2) { ?>
                        <? foreach ($question->variantsAnswer as $key=>$variant) { ?>
                            <div class="form-group">

                                <label class="custom-control custom-checkbox" for="check_<?=$key?>">
                                    <span class="custom-control-indicator"></span>
                                    <? $variant = \common\models\VariantsAnswer::findOne(['id' => $variant->id]);?>
                                    <span class="custom-control-description" style="color: <?=($variant->correct == 0) ? 'red' : "green"?>"><?= $variant->title ?></span>
                                    <span class="custom-control-description">
                                        <? if (!empty($answer) && $answer->answer == $variant->id && $answer->correct === null) { ?>
                                            (на проверке)
                                        <? } else if (!empty($answer) && $answer->answer == $variant->id && $answer->correct === 1) { ?>
                                            (правильно)
                                        <? } else if (!empty($answer) && $answer->answer == $variant->id && $answer->correct === 0) { ?>
                                            (неправильно)
                                        <? } ?>
                                    </span>
                                </label>
                                    <input type="checkbox" class="custom-control-input" id="check_<?=$key?>"
                                        <? $answer = \common\models\QuestionsTask::getAnswer($user, $question->id, $variant->id); ?>
                                        <? if (!empty($answer->answer) && $answer->answer == $variant->id) { ?>
                                            checked
                                        <? } ?>

                                    >
                            </div>
                        <? } ?>
                    <? } ?>
                    <? if ($question->format == 3) { ?>
                        <?
                        $answer = \common\models\UsersAnswers::findOne(['users_id' => $user, 'questions_task_id' => $question->id]);
                        $file = null;
                        if (!empty($answer)) {
                            $file = Image::findOne(['modelName' => 'Tasks', 'itemId' => $answer->id]);
                            if (!empty($file)) {
                                $file = $file->filePath;
                            }
                        }
                        ?>
                        <? if (!is_null($file)) { ?>
                            <a href="<?= (!empty($file)) ? '/uploads/store/' . $file : null ?>" download>Файл</a>
                        <? } else { ?>
                            Файл не загружен
                        <? } ?>
                    <? } ?>
                </div>
            <? } ?>
        </div>
    </div>
</div>

<?if($user_task->result === null){?>
<div class="card text-center">
    <a href="<?=\yii\helpers\Url::to(['/answers/answers/wrong', 'id' => $user_task->id])?>" class="btn btn-danger">Неправильно</a>
    <a href="<?=\yii\helpers\Url::to(['/answers/answers/success', 'id' => $user_task->id])?>" class="btn btn-success">Правильно</a>
</div>
<? }?>





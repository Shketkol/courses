<?
use yii\grid\GridView;
use \yii\helpers\Html;
use \yii\helpers\Url;
use \common\models\Users;

$this->title = 'Ответы пользователей';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="table-responsive">
                <?php \yii\widgets\Pjax::begin(); ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'summary' => false,
                    'layout' => "{summary}\n{items}\n<div align='right'>{pager}</div>",
                    'tableOptions' => [
                        'class' => 'table table-hover manage-u-table'
                    ],
                    'columns' => [
                        [
                            'attribute'=>'id',
                            'contentOptions' =>['class' => 'text-center'],
                            'headerOptions' => ['class' => 'text-center','style'=>'width: 80px;']
                        ],
                        [
                            'attribute' => 'users_id',
                            'value' => function($model) {
                                $name = $model->users->name;
                                if (!empty($model->users->usersProfile->surname)){
                                    $name = $name.' '.$model->users->usersProfile->surname;
                                }
                                return $name;
                            },

                        ],
                        [
                            'attribute' => 'users_id',
                            'label' => 'Email',
                            'value' => function($model) {
                                return $model->users->email;
                            },

                        ],
                        [
                            'label' => 'Курс',
                            'attribute' => 'courses_id',
                            'value' => function($model) {
                                return $model->tasks->lessons->courses->title;
                            },

                        ],
                        [
                            'label' => 'Статус',
                            'attribute' => 'result',
                            'value' => function($model) {
                                if($model->result == 1){
                                    return 'проверен';
                                } else {
                                    return 'непроверен';
                                }
                            },

                        ],
                        [
                            'attribute' => 'created_at',
                            'label' => 'Дата ответа',
                            'value' => function($model) {
                                return $model->created_at;
                            },

                        ],
                        [
                            'header' => 'Управление',
                            'class' => 'backend\components\grid\CustomActionColumn',
                            'contentOptions' =>['class' => 'text-center'],
                            'headerOptions' => ['class' => 'text-center', 'width' => '80'],
                            'filterOptions' => ['class' => 'text-center'],
                            'filter' => Url::to(['index']),
                            'template' => '{view}',
//                            'buttons' => [
//                                'update' => function ($url, $model) {
//                                    return Html::a('<i class="ti-pencil" aria-hidden="true"></i>', Url::to(['update', 'id' => $model->id]), [
//                                        'data-original-title' => 'Редактировать',
//                                        'data-toggle' => 'tooltip',
//                                        'class' => 'btn btn-sm btn-icon btn-info btn-outline no-pjax'
//                                    ]);
//                                },
//                            ],
                        ],
                    ],
                ]); ?>
                <?php \yii\widgets\Pjax::end(); ?>
            </div>
        </div>
    </div>
</div>

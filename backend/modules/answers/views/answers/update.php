<?php
use \yii\bootstrap\ActiveForm;
use \yii\helpers\Url;
use \yii\helpers\Html;
use \common\models\Users;

?>
<div class="row">
    <div class="col-md-12">
        <? $form = ActiveForm::begin([
            'id' => 'users-update',
            'enableClientValidation'=> false,
            'validateOnSubmit' => true,
            'options' => [
                'class' => 'form-horizontal',
                'autocomplete' => 'off',
            ],
            'fieldConfig' => [
                'template' => "{label}<div class=\"form-line\">{input}<span class=\"help-block hidden\"></span></div>",
            ],
        ]); ?>
        <div class="white-box">
            <div class="row">
                <div class="col-xs-9">
                    <h3 class="box-title m-b-0">Основная информация</h3>
                </div>
            </div>
            <?=$form->errorSummary($model);?>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'users_id')->dropDownList(
                        \yii\helpers\ArrayHelper::map(\common\models\Users::find()->orderBy('name')->all(), 'id', 'name'),
                        array('prompt' => 'Выберите значение')
                    ) ?>
                    <?= $form->field($model, 'cities_id')->dropDownList(
                        \yii\helpers\ArrayHelper::map(\common\models\Cities::find()->orderBy('name')->all(), 'id', 'name'),
                        array('prompt' => 'Выберите значение')
                    ) ?>
                    <?= $form->field($model, 'courses_id')->dropDownList(
                        \yii\helpers\ArrayHelper::map(\common\models\Courses::find()->orderBy('title')->all(), 'id', 'title'),
                        array('prompt' => 'Выберите значение')
                    ) ?>
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-lg btn-success waves-effect waves-light m-t-10 m-r-10"><i class="fa fa-spin fa-circle-o-notch hidden"></i>&nbsp; Сохранить &nbsp;</button>

        <? ActiveForm::end(); ?>
    </div>
</div>
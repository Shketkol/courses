<?php

namespace backend\modules\answers\controllers;

use backend\components\controllers\AdminController;
use backend\models\AnswersSearch;
use backend\models\TeachersSearch;
use common\models\BallsHistory;
use common\models\Lessons;
use common\models\Tasks;
use common\models\Teachers;
use common\models\Users;
use common\models\UsersAnswers;
use common\models\UsersLessons;
use Yii;
use yii\debug\models\search\User;


class AnswersController extends AdminController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex($type)
    {
        $searchModel = new AnswersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, Yii::$app->user->id, $type);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        $model = UsersAnswers::findOne(['id' => $id]);

        $user = $model->users_id;

        $model = Tasks::findOne(['id' => $model->tasks_id]);

        $lesson = Lessons::findOne(['id' => $model->lessons_id]);

        return $this->render('view', [
            'model' => $model,
            'lesson' => $lesson,
            'user' => $user
        ]);
    }

    public function actionSuccess($id)
    {
        $model = UsersAnswers::findOne(['id' => $id]);
        $model->correct = 1;
        $model->save(false);

        $models = UsersAnswers::findAll([
            'users_id' => $model->users_id,
            'tasks_id' => $model->tasks_id,
            'questions_task_id' => $model->questions_task_id
        ]);

        foreach ($models as $value){
            $value->result = 1;
            $value->save(false);
        }
        //добавлять балы
        $user = Users::findOne(['id' => $model->users_id]);
        $user->bals = $user->bals + $model->questionsTask->rating;
        $user->save(false);

        $userBall = new BallsHistory();
        $userBall->user_id = $model->users_id;
        $userBall->courses_id = $model->tasks->lessons->courses_id;
        $userBall->ball = $model->questionsTask->rating;
        $userBall->save(false);

        $tasks = Tasks::find()
            ->where(['lessons_id' => $models[0]->tasks->lessons_id])
            ->all();
        $finish = true;
        foreach ($tasks as $value){
            $is = UsersAnswers::findOne(['users_id' => $model->users_id, 'tasks_id' => $value->id, 'correct' => null]);
            if (!empty($is)){
                $finish = false;
                break;
            }
            $is = UsersAnswers::findOne(['users_id' => $model->users_id, 'tasks_id' => $value->id, 'correct' => 0]);
            if (!empty($is)){
                $finish = false;
                break;
            }
            $is = UsersAnswers::findOne(['users_id' => $model->users_id, 'tasks_id' => $value->id]);
            if (empty($is)){
                $finish = false;
                break;
            }
        }

        if ($finish){
            UsersLessons::updateAll(['status' => 2], [
                'users_id' => $model->users_id,
                'lessons_id' => $models[0]->tasks->lessons_id
            ]);
        }



        return $this->redirect(['/answers/answers/index', 'type' => $userBall->courses->type]);
    }

    public function actionWrong($id)
    {
        $model = UsersAnswers::findOne(['id' => $id]);
        $model->correct = 0;
        $model->save(false);

        $models = UsersAnswers::findAll([
            'users_id' => $model->users_id,
            'tasks_id' => $model->tasks_id,
            'questions_task_id' => $model->questions_task_id
        ]);
        foreach ($models as $value){
            $value->result = 1;
            $value->save(false);
        }

        return $this->redirect(['/answers/answers/index', 'type' => $model->tasks->lessons->courses->type]);
    }

}

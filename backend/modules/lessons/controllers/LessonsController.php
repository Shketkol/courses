<?php

namespace backend\modules\lessons\controllers;

use backend\components\controllers\AdminController;
use backend\models\LessonsSearch;
use common\helpers\Images;
use common\models\Lessons;
use Yii;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * Default controller for the `courses` module
 */
class LessonsController extends AdminController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex($id)
    {
        $searchModel = new LessonsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $id);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'id' => $id
        ]);
    }

    public function actionCreate($id)
    {
        $model = new Lessons();
        $model->courses_id = $id;

        if (Yii::$app->request->post()) {
            $model->load(Yii::$app->request->post());

            if ($model->save()) {

                $model->setImages(Images::TYPE_COURSES);


                return $this->redirect(Url::to(['/courses/courses/view', 'id' => $model->courses_id]));
            }
        }
        return $this->render('create', [
            'model' => $model,
            'id' => $id
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->save()) {
            return $this->redirect(Url::to(['/courses/courses/view', 'id' => $model->courses_id]));
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionView($id)
    {
        $model = $this->loadModel($id);
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        $model = $this->loadModel($id);
        $model->delete();
        return $this->redirect(Url::to(['/courses/courses/view', 'id' => $model->courses_id]));
    }

    public function loadModel($id)
    {
        $model = Lessons::findOne(['id' => $id]);
        if ($model === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        return $model;
    }

    public function actionChangePosition()
    {
        if (Yii::$app->request->post()) {
            $id = Yii::$app->request->post('id');
            $position = Yii::$app->request->post('position');

            $type = Lessons::find()->where(['id' => $id])->one();
            $type->position = $position;
            $type->save();

            $models = Lessons::find()
                ->where('id <> :id', ['id' => $id])
                ->andWhere(['courses_id' => $type->courses_id])
                ->orderBy('position')
                ->all();
            foreach ($models as $model) {
                if ($model->position == $position) {
                    $model->position = (int)$model->position - 1;
                    $model->save();
                }
            }
            return $this->asJson(1);
        }
    }
}

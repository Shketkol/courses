<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->registerCssFile('https://cdn.jsdelivr.net/fontawesome/4.5.0/css/font-awesome.min.css');
$this->registerCssFile('@web/public/examples/css/nestable.min.css');
$this->registerJsFile('@web/source/lib/ckeditorbigger/ckeditor.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/public/vendor/jquery.nestable.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/public/examples/js/nestable.js', ['depends' => [\yii\web\JqueryAsset::className()]]); ?>



<?php $form = ActiveForm::begin([
    'options' => ['enctype' => 'multipart/form-data'],
    'fieldConfig' => [
        'template' => "{label}<div class=\"form-line\">{input}<span class=\"help-block hidden\"></span></div>",
    ],
]); ?>
<div class="card">
    <div class="header">
        <h4 class="card-title">Общая информация</h4>
    </div>
    <div class="body">
        <?=$this->render('@app/views/components/upload', [
            'model' => $model,
            'form' => $form,
            'field' => 'image',
            'type' => \common\helpers\Images::$params[\common\helpers\Images::TYPE_LESSONS],
            'name' => \common\helpers\Images::TYPE_LESSONS,
        ]) ?>
        <?= $form->field($model, 'title')->textInput(['maxlength' => true, 'required' =>true]) ?>
        <?= $form->field($model, 'desc')->textarea(['rows' => 6, 'class' => 'useCKEditor']) ?>
        <?= $form->field($model, 'duration')->textInput(['type' => 'number', 'min' => 0, 'step' => '0.1', 'required' =>true]) ?>
        <?= $form->field($model, 'video')->textInput(['maxlength' => true]) ?>
    </div>
</div>
<?// if (!$model->isNewRecord) { ?>
<!--    <div class="card">-->
<!--        <div class="card-header">-->
<!--            <h4 class="card-title">Задания</h4>-->
<!--        </div>-->
<!--        <div class="card-block">-->
<!--            <p><a href="--><?//= \yii\helpers\Url::to(['/tasks/tasks/create', 'id' => $model->id]) ?><!--"-->
<!--                  class="btn btn-primary">Добавить задания <i class="material-icons">add</i></a></p>-->
<!--            --><?// if (!empty($model->tasks)) { ?>
<!--                <div class="nestable" id="nestable-handles-primary">-->
<!--                    <ul class="nestable-list">-->
<!--                        --><?// $tasks = \common\models\Tasks::find()
//                            ->where(['lessons_id' => $model->id])
//                            ->orderBy('position')
//                            ->all(); ?>
<!--                        --><?// foreach ($tasks as $task) { ?>
<!--                            <li class="nestable-item nestable-item-handle" data-id="--><?//= $task->id ?><!--" data-position="--><?//= $task->position ?><!--">-->
<!--                                <div class="nestable-handle"><i class="material-icons">menu</i></div>-->
<!--                                <div class="nestable-content">-->
<!--                                    <div class="media">-->
<!--                                        <div class="media-body media-middle">-->
<!--                                            <h5 class="card-title h6 mb-0">-->
<!--                                                <a href="--><?//= \yii\helpers\Url::to(['/tasks/tasks/update', 'id' => $task->id]) ?><!--">--><?//= $task->title ?><!--</a>-->
<!--                                            </h5>-->
<!--                                        </div>-->
<!--                                        <div class="media-right media-middle">-->
<!--                                            <a href="--><?//= \yii\helpers\Url::to(['/tasks/tasks/update', 'id' => $task->id]) ?><!--"-->
<!--                                               class="btn btn-white btn-sm"><i class="material-icons">edit</i></a>-->
<!--                                        </div>-->
<!--                                        <div class="media-right media-middle">-->
<!--                                            <a href="--><?//= \yii\helpers\Url::to(['/tasks/tasks/delete', 'id' => $task->id]) ?><!--"-->
<!--                                               class="btn btn-white btn-sm"><i class="material-icons">delete</i></a>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </li>-->
<!--                        --><?// } ?>
<!--                    </ul>-->
<!--                </div>-->
<!--            --><?// } ?>
<!--        </div>-->
<!--    </div>-->
<?// } ?>
<?if (Yii::$app->user->identity->users_roles_id != \common\models\Users::TYPE_TEACHER):?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
<?endif;?>
<?php ActiveForm::end(); ?>

<?
$url = \yii\helpers\Url::to(['/tasks/tasks/change-position']);
$script = <<< JS

$('#nestable-handles-primary').on('change', function(event) {
   $(this).find('li').each(function(p1, p2) {
     var i = p1+1;
     if($(this).data('position') != i){
         $.post('$url',
         {
         "id": $(this).data('id'),
          "position": i,
          },
         function(result) {}
         );
     }
   })  
})
         
JS;
$this->registerJs($script, yii\web\View::POS_END);
?>

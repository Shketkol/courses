<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Leagues */

$this->title = 'Добавить';
$this->params['breadcrumbs'][] = ['label' => 'Курсы', 'url' => ['/courses/courses/index']];
$this->params['breadcrumbs'][] = ['label' => $model->courses->title, 'url' => ['/courses/courses/view', 'id' => $model->courses_id]];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-12">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>

</div>
<?php

$this->registerCssFile('https://cdn.jsdelivr.net/fontawesome/4.5.0/css/font-awesome.min.css');
$this->registerCssFile('@web/public/examples/css/nestable.min.css');
$this->registerJsFile('@web/source/lib/ckeditorbigger/ckeditor.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/public/vendor/jquery.nestable.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/public/examples/js/nestable.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

$this->title = 'Просмотр';
$this->params['breadcrumbs'][] = ['label' => 'Курсы', 'url' => ['/courses/courses/index']];
$this->params['breadcrumbs'][] = ['label' => $model->courses->title, 'url' => ['/courses/courses/view', 'id' => $model->courses_id]];
$this->params['breadcrumbs'][] = $this->title;

$image = $model->getImagesByName(\common\helpers\Images::TYPE_COURSES)
?>

<h1 class="page-heading h2"><?=$model->title?></h1>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="embed-responsive embed-responsive-16by9">
                <img src="<?=(!empty($image)) ? $image[0]->getUrl('500x300') : null?>">
            </div>
            <div class="card-block">
                <?=$model->desc?>
                <?=$model->video?>
            </div>
        </div>
        <? if (!$model->isNewRecord) { ?>
            <div class="card">
                <div class="header">
                    <h4 class="card-title">Задания</h4>
                </div>
                <div class="body">
                    <p>
                        <?if (Yii::$app->user->identity->users_roles_id != \common\models\Users::TYPE_TEACHER):?>
                            <a href="<?= \yii\helpers\Url::to(['/tasks/tasks/create', 'id' => $model->id]) ?>"
                               class="btn btn-primary">Добавить задания <i class="material-icons">add</i></a>
                        <?endif;?>
                    </p>
                    <? if (!empty($model->tasks)) { ?>
                        <div class="nestable" id="nestable-handles-primary">
                            <ul class="nestable-list">
                                <? $tasks = \common\models\Tasks::find()
                                    ->where(['lessons_id' => $model->id])
                                    ->orderBy('position')
                                    ->all(); ?>
                                <? foreach ($tasks as $task) { ?>
                                    <li class="nestable-item nestable-item-handle" data-id="<?= $task->id ?>" data-position="<?= $task->position ?>">
                                        <div class="nestable-handle"><i class="material-icons">menu</i></div>
                                        <div class="nestable-content">
                                            <div class="media">
                                                <div class="media-body media-middle">
                                                    <h5 class="card-title h6 mb-0">
                                                        <a href="<?= \yii\helpers\Url::to(['/tasks/tasks/update', 'id' => $task->id]) ?>"><?= $task->title ?></a>
                                                    </h5>
                                                </div>
                                            <?if (Yii::$app->user->identity->users_roles_id != \common\models\Users::TYPE_TEACHER):?>

                                                <div class="media-right media-middle">
                                                    <a href="<?= \yii\helpers\Url::to(['/tasks/tasks/update', 'id' => $task->id]) ?>"
                                                       class="btn btn-white btn-sm"><i class="material-icons">edit</i></a>
                                                </div>
                                                <div class="media-right media-middle">
                                                    <a href="<?= \yii\helpers\Url::to(['/tasks/tasks/delete', 'id' => $task->id]) ?>"
                                                       class="btn btn-white btn-sm"><i class="material-icons">delete</i></a>
                                                </div>
                                            <?endif;?>
                                            </div>
                                        </div>
                                    </li>
                                <? } ?>
                            </ul>
                        </div>
                    <? } ?>
                </div>
            </div>
        <? } ?>
    </div>


</div>
<?
$url = \yii\helpers\Url::to(['/tasks/tasks/change-position']);
$script = <<< JS

$('#nestable-handles-primary').on('change', function(event) {
   $(this).find('li').each(function(p1, p2) {
     var i = p1+1;
     if($(this).data('position') != i){
         $.post('$url',
         {
         "id": $(this).data('id'),
          "position": i,
          },
         function(result) {}
         );
     }
   })  
})
         
JS;
$this->registerJs($script, yii\web\View::POS_END);
?>
<?php

namespace backend\modules\teachers\controllers;

use backend\components\controllers\AdminController;
use backend\models\TeachersSearch;
use common\models\Groups;
use common\models\Model;
use common\models\Teachers;
use common\models\TeachersCourses;
use common\models\TeachersGroups;
use common\models\Users;
use Yii;


class TeachersController extends AdminController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new TeachersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = new Teachers();
        $teachers = [new TeachersCourses()];
        $groups = [[new TeachersGroups()]];
        if($model->load(Yii::$app->request->post()) && $model->save()){

            $teacher = Model::createMultiple(TeachersCourses::classname());
            Model::loadMultiple($teacher, Yii::$app->request->post());

            foreach ($teacher as $key => $item) {
                $item->teacher_id = $model->id;
                $item->save(false);

                if (isset($_POST['TeachersGroups'][$key]) && is_array($_POST['TeachersGroups'][$key])) {
                    foreach ($_POST['TeachersGroups'][$key] as $k => $value) {
                        $options = new TeachersGroups();
                        $options->teachers_id = $model->id;
                        $options->teachers_courses_id = $item->id;
                        $options->groups_id = $value['groups_id'];
                        $options->save(false);
                    }
                }
            }


            $user = Users::findOne(['id' => $model->users_id]);
            $user->users_roles_id = Users::TYPE_TEACHER;
            $user->save(false);
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
            'teachers' => $teachers,
            'groups' => $groups
        ]);
    }

    public function actionUpdate($id)
    {
        $model = Teachers::findOne(['id' => $id]);
        $teachers = $model->courses;
        $groups = [];

        if (!empty($teachers)) {
            foreach ($teachers as $key => $value) {
                $option = TeachersGroups::findAll(['teachers_courses_id' => $value->id]);
                if (!empty($option)) {
                    $groups[$key] = $option;
                }
            }
        }

        if (empty($groups)){
            $groups = [[new TeachersGroups()]];
        }

        if(!empty($_POST)){
            if($model->load(Yii::$app->request->post()) && $model->save(false)){

                $teachers = Model::createMultiple(TeachersCourses::classname());
                Model::loadMultiple($teachers, Yii::$app->request->post());

                TeachersCourses::deleteAll(['teacher_id' => $model->id]);
                TeachersCourses::deleteAll(['teacher_id' => $model->id]);

                foreach ($teachers as $key => $item) {
                    $item->teacher_id = $model->id;
                    $item->save(false);

                    if (isset($_POST['TeachersGroups'][$key]) && is_array($_POST['TeachersGroups'][$key])) {
                        foreach ($_POST['TeachersGroups'][$key] as $k => $value) {
                            $options = new TeachersGroups();
                            $options->teachers_id = $model->id;
                            $options->teachers_courses_id = $item->id;
                            $options->groups_id = $value['groups_id'];
                            $options->save(false);
                        }
                    }
                }


                $user = Users::findOne(['id' => $model->users_id]);
                $user->users_roles_id = Users::TYPE_TEACHER;
                $user->save(false);
                return $this->redirect(['index']);
            }
        }


        return $this->render('update', [
            'model' => $model,
            'teachers' => $teachers,
            'groups' => $groups
        ]);
    }

    public function actionGroup()
    {
        if (Yii::$app->request->isAjax){
            $id = $_GET['id'];
            $model = Groups::findAll(['courses_id' => $id]);

            $group = null;
            $id_teacher = $_SERVER['HTTP_REFERER'];
            if (!empty($id_teacher)){
                $id_teacher = str_replace('http://'.$_SERVER['SERVER_NAME'].'/admin/teachers/teachers/update?id=', '', $id_teacher);
                $groups = Teachers::findOne(['id' => $id_teacher]);
                if (!empty($groups)){
                    foreach ($groups->courses as $value){
                        if ($value->courses_id == $id){
                            $group = $value->groups_id;
                        }
                    }
                }
            }
            return $this->renderPartial('_gropups', [
                'model' => $model,
                'group' => $group
            ]);
        }
    }

}

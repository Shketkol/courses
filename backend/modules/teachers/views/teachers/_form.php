<?php

use yii\helpers\Html;
use wbraganca\dynamicform\DynamicFormWidget;

$js = ' jQuery("body").on( "click", "#dynamic-form .add-room", function(e, item) {
            console.log(11)
            var id = jQuery(this).parents(".row").find(".js_course option:selected").val(),
                    element = jQuery(this);
                $.ajax({
                    type: "GET",
                    url: "/admin/teachers/teachers/group",
                    data: "id="+id,
                    success: function(data) {
                            element.parents(".dynamicform_inner").find(".room-item:last .js_group").html(data);
                    }
                });
        });
               
        jQuery(".dynamicform_inner").on("afterInsert", function(e, item) {
        });

        jQuery("#dynamic-form").on("afterDelete", function(e) {
        });
        
        jQuery("body").on("change", ".js_course", function(e, item) {
            console.log(222)
            var id = jQuery(this).find("option:selected").val(),
                    element = jQuery(this);
                $.ajax({
                    type: "GET",
                    url: "/admin/teachers/teachers/group",
                    data: "id="+id,
                    success: function(data) {
                            element.parents(".card-block").find(".js_group").html(data)
                    }
                });
        })
        
        ';

$this->registerJs($js);

?>

<?// var_dump($modelsOptions);die;?>
<?php DynamicFormWidget::begin([
    'widgetContainer' => 'dynamicform_inner',
    'widgetBody' => '.container-rooms',
    'widgetItem' => '.room-item',
    'limit' => 20,
    'min' => 0,
    'insertButton' => '.add-room',
    'deleteButton' => '.remove-room',
    'model' => $groups[0],
    'formId' => 'dynamic-form',
    'formFields' => [
        'title',
        'correct'
    ],
]); ?>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Группы</th>
            <th class="text-center">
                <button type="button" class="add-room btn btn-success btn-xs"><span class="glyphicon glyphicon-plus"></span></button>
            </th>
        </tr>
        </thead>
        <tbody class="container-rooms">
        <?php foreach ($groups as $indexQuestion => $group): ?>
            <tr class="room-item">
                <td class="vcenter">
                    <?php
                    if (! $group->isNewRecord) {
                        $groupsArr = [];
                        $course = \common\models\Groups::findOne(['id' => $group->groups_id]);
                        $arr = \common\models\Groups::findAll(['courses_id' => $course->courses_id]);
                        foreach ($arr as $v){
                            $groupsArr[$v->id] = $v->name;
                        }

                        echo Html::activeHiddenInput($group, "[{$indexQuestion}][{$indexQuestion}]id");
                    } else {
                        $groupsArr = [];
                    }
                    ?>
                    <?= $form->field($group, "[{$indexQuestion}][{$indexQuestion}]groups_id")->dropDownList($groupsArr,
                        array('class' => 'form-control js_group')
                    ) ?>
                </td>
                <td class="text-center vcenter" style="width: 90px;">
                    <button type="button" class="remove-room btn btn-danger btn-xs"><span class="glyphicon glyphicon-minus"></span></button>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
<?php DynamicFormWidget::end(); ?>
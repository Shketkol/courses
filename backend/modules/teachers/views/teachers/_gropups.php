<?php
use \yii\helpers\Html;
?>
<?if(!empty($model)):?>
    <?foreach ($model as $value):?>
        <option
                <?if (!is_null($group) && $value->id == $group):?>
                    selected
                <?endif;?>
                value="<?=$value->id?>"
        ><?=$value->name?></option>
    <?endforeach;?>
<?endif;?>


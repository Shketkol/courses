<?php
use \yii\bootstrap\ActiveForm;
use \yii\helpers\Url;
use \yii\helpers\Html;
use \common\models\Users;

//$this->registerJsFile('@web/source/js/group.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
    $js = '            
//            jQuery("#dynamic-form .js_course").each(function(i) {
//                var id = jQuery(this).find("option:selected").val(),
//                    element = jQuery(this);
//                $.ajax({
//                    type: "GET",
//                    url: "/admin/teachers/teachers/group",
//                    data: "id="+id,
//                    success: function(data) {
//                            jQuery("#dynamic-form .js_group").eq(i).html(data);
//                    }
//                });
//            });
//            
//           jQuery("#dynamic-form .js_course").on("change", function(i) {
//                var id = jQuery(this).find("option:selected").val(),
//                    element = jQuery(this);
//                $.ajax({
//                    type: "GET",
//                    url: "/admin/teachers/teachers/group",
//                    data: "id="+id,
//                    success: function(data) {
//                            element.parents(".item").find(".js_group").html(data);
//                    }
//                });
//            });

        ';

    $this->registerJs($js);

?>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="body">
                <? $form = ActiveForm::begin([
                    'id' => 'dynamic-form',
                    'enableClientValidation'=> false,
                    'validateOnSubmit' => true,
                    'options' => [
//                        'class' => 'form-horizontal',
                        'autocomplete' => 'off',
                    ],
                    'fieldConfig' => [
                        'template' => "{label}<div class=\"form-line\">{input}<span class=\"help-block hidden\"></span></div>",
                    ],
                ]); ?>
                        <div class="header">
                            <h3 class="box-title m-b-0">Основная информация</h3>
                        </div>
                    <?=$form->errorSummary($model);?>
                            <?= $form->field($model, 'users_id')->dropDownList(
                                \yii\helpers\ArrayHelper::map(\common\models\Users::find()->where(['status' => Users::STATUS_ACTIVE])->orderBy('name')->all(), 'id', 'email'),
                                array('required' => true)
                            ) ?>
                            <?= $form->field($model, 'cities_id')->dropDownList(
                                \yii\helpers\ArrayHelper::map(\common\models\Cities::find()->where(['status' => 10])->orderBy('name')->all(), 'id', 'name')
                                ) ?>
                <div class="card">
                    <div class="card">
                        <h4 class="header">Курсы</h4>
                    </div>
                    <div class="body">
                        <div class="panel-body">
                            <?php \wbraganca\dynamicform\DynamicFormWidget::begin([
                                'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                                'widgetBody' => '.container-items', // required: css class selector
                                'widgetItem' => '.item', // required: css class
                                'limit' => 100, // the maximum times, an element can be cloned (default 999)
                                'min' => 1, // 0 or 1 (default 1)
                                'insertButton' => '.add-item', // css class
                                'deleteButton' => '.remove-item', // css class
                                'model' => $teachers[0],
                                'formId' => 'dynamic-form',
                                'formFields' => [
                                    'courses_id',
                                ],
                            ]); ?>

                            <div class="container-items"><!-- widgetContainer -->
                                <?php foreach ($teachers as $i => $teacher): ?>
                                    <div class="item panel panel-default card-block"><!-- widgetBody -->
                                        <div class="panel-heading">
                                            <!--                            <h3 class="panel-title pull-left">Вопрос</h3>-->
                                            <div class="pull-right">
                                                <button type="button" class="add-item btn btn-success btn-xs"><i
                                                            class="glyphicon glyphicon-plus"></i></button>
                                                <button type="button" class="remove-item btn btn-danger btn-xs"><i
                                                            class="glyphicon glyphicon-minus"></i></button>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="body">
                                            <?php
                                            // necessary for update action.
                                            if (!$model->isNewRecord) {
                                                echo Html::activeHiddenInput($teacher, "[{$i}]id");
                                            }
                                            ?>
                                            <div class="row">

                                                <?= $form->field($teacher, "[{$i}]courses_id")->dropDownList(
                                                    \yii\helpers\ArrayHelper::map(\common\models\Courses::find()->where(['status' => 10])->orderBy('title')->all(), 'id', 'title'),
                                                    array('required' => true, 'class' => 'form-control js_course')
                                                ) ?>
<!--                                                --><?//= $form->field($teacher, "[{$i}]groups_id")->dropDownList([],
//                                                    array('class' => 'form-control js_group')
//                                                ) ?>
                                                <?= $this->render('_form', [
                                                    'form' => $form,
                                                    'indexQuestion' => $i,
                                                    'groups' => (!empty($groups[$i])) ? $groups[$i] : $groups[0],
                                                ]) ?>
                                            </div><!-- .row -->
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                            <?php \wbraganca\dynamicform\DynamicFormWidget::end(); ?>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-lg btn-success waves-effect waves-light m-t-10 m-r-10"><i class="fa fa-spin fa-circle-o-notch hidden"></i>&nbsp; Сохранить &nbsp;</button>

                <? ActiveForm::end(); ?>
            </div>
        </div>

    </div>
</div>
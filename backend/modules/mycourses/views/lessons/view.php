<?php
$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Курсы', 'url' => ['/mycourses/courses/index']];
$this->params['breadcrumbs'][] = ['label' => $model->courses->title, 'url' => ['/mycourses/courses/view', 'id' => $model->courses->id]];
$this->params['breadcrumbs'][] = $this->title;

$image = $model->getImagesByName(\common\helpers\Images::TYPE_COURSES);

$this->registerJsFile('@web/public/vendor/moment.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/public/vendor/jquery.countdown.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/public/examples/js/countdown.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

$this->registerJsFile('@web/source/js/tasks.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

?>
<h1 class="page-heading h2"><?= $model->title ?></h1>

<div class="row">
    <div class="col-md-8">
        <? if ($lesson_user->read === 0) { ?>
            <div class="card">
                <div class="body">
                    <?= $model->desc ?>
                </div>
                <div class="body">
                    <?= $model->video ?>
                </div>
            </div>
            <?
            $lesson_user->read = 1;
            $lesson_user->save(false);
            ?>
        <? } else { ?>
            <div class="card-block">
                <button class="btn btn-success" data-toggle="collapse" data-target="#cource">Прочитать описание</button>
            </div>
            <div id="cource" class="collapse">
                <div class="card">
                    <div class="body">
                        <?= $model->desc ?>
                    </div>
                    <div class="body">
                        <?= $model->video ?>
                    </div>
                </div>
            </div>
        <? } ?>


        <div class="js_form_task">
            <? if (!empty($task)) { ?>
                <?= $this->render('@app/modules/mycourses/views/tasks/view', array('model' => $task)) ?>
            <? } else { ?>
                <? if ($lesson_user->status === 1) { ?>
                    <div class="card-footer">
                        <button data-id="<?= $lesson_user->id ?>" class="btn btn-success float-xs-right js_finish_lessons">
                            Ознакомился <i
                                    class="material-icons btn__icon--right">send</i></button>
                    </div>
                <? } else { ?>
                    <div class="card card-success text-xs-center">
                        <div class="card-block">
                            <p>Урок пройден</p>
                        </div>
                    </div>
                <? } ?>
            <? } ?>
        </div>
    </div>
    <div class="col-md-4">
        <!-- Sidebar -->
        <div class="mdk-drawer mdk-js-drawer" align="end">
            <div class="mdk-drawer__content">
                <div class="sidebar-p-y">
                    <? if ($model->duration != 0) { ?>
                        <div class="sidebar-heading">Осталось времени</div>
                        <div class="countdown" data-value="<?= $model->duration ?>" data-unit="hours"></div>
                    <? } ?>

                    <? if (!empty($tasks)) { ?>
                        <div class="sidebar-heading">Задания</div>
                        <ul class="list-group list-group-fit js_list_tasks">
                            <? foreach ($tasks as $key => $task_item) { ?>
                                <li class="list-group-item <?= ($task->id == $task_item->id) ? 'active' : null ?>">
                                    <a data-id="<?= $task_item->id ?>" class="js_tasks">
                                <span class="media">
                                  <span class="media-left">
                                    <span class="btn btn-white btn-circle">#<?= $key + 1 ?></span>
                                  </span>
                                  <span class="media-body media-middle">
                                    <?= $task_item->title ?>
                                  </span>
                                </span>
                                    </a>
                                </li>
                            <? } ?>
                        </ul>
                    <? } ?>

                </div>
            </div>
        </div>
        <!-- // END Sidebar -->
    </div>
</div>






<?php
$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Курсы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$image = $model->getImagesByName(\common\helpers\Images::TYPE_COURSES)
?>

<h1 class="page-heading h2"><?=$model->title?></h1>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="embed-responsive embed-responsive-16by9">
                <img src="<?=(!empty($image)) ? $image[0]->getUrl('500x300') : null?>" alt="" style="width:100%;">
            </div>
            <div class="body">
                <?=$model->desc?>
            </div>
        </div>

        <!-- Lessons -->
        <h2 class="page-heading h3">Уроки</h2>
        <ul class="card list-group list-group-fit">
            <?foreach ($lessons as $key=>$lesson){?>
                <li class="list-group-item">
                    <div class="">
                        <?if($show){?>
                            <? $href = '#';
                            if($lesson['active'] == 1){
                                $href = \yii\helpers\Url::to(['/mycourses/lessons/view', 'id' => $lesson['id']]);
                             } else if($lesson['active'] == 2){
                                $href = \yii\helpers\Url::to(['/mycourses/lessons/view', 'id' => $lesson['id']]);
                             } else if($lesson['active'] == 3){
                                $href = '#';
                             } ?>
                            <a href="<?=$href?>">
                                <div class="media-left">
                                    <h4><?=$key+1?>.</h4>
                                </div>
                                <div class="media-body">
                                    <h4><?=$lesson['title']?></h4>
                                </div>
                                <div class="media-right">
                                    <small class="btn bg-<?=\common\models\Lessons::$types[$lesson['active']]?> waves-effect">
                                        <? if($lesson['active'] == 1){?>
                                            активен
                                        <? } else if($lesson['active'] == 2){ ?>
                                            пройден
                                        <? } else if($lesson['active'] == 3){ ?>
                                            неактивен
                                        <? } ?>

                                    </small>
                                </div>
                            </a>
                        <? } else {?>
                            <a href="#" onclick="void 0">
                                <div class="media-left">
                                    <div class="text-muted"><?=$key+1?>.</div>
                                </div>
                                <div class="media-body">
                                    <div class="text-muted-light"><?=$lesson['title']?></div>
                                </div>
                                <div class="media-right" class="btn bg-purple waves-effect">
                                    <small class="tag tag-default">неактивен</small>
                                </div>
                            </a>
                        <? }?>
                    </div>
                </li>
            <? }?>
        </ul>
    </div>
</div>

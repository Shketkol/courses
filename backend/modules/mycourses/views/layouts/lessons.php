<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use frontend\assets\AdminMainAsset;
use frontend\assets\AdminHeadAsset;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

AdminHeadAsset::register($this);
AdminMainAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="top-navbar">
<?php $this->beginBody() ?>

<?//=\frontend\widgets\alert\MessageWidget::widget()?>

<?=$this->render('@app/views/layouts/main/header')?>

<div class="mdk-drawer-layout mdk-js-drawer-layout" push has-scrolling-region>
    <div class="mdk-drawer-layout__content">
        <div class="container-fluid">

            <?
            echo Breadcrumbs::widget([
                'options' => ['class' => 'breadcrumb'],
                'homeLink' => ['label' => 'Главная панель', 'url' => ['/']],
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]);
            ?>

            <?= $content ?>


    <?=$this->render('@app/views/layouts/main/navigation1')?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

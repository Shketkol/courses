<?php

namespace backend\modules\mycourses\controllers;
use backend\components\controllers\AdminController;
use common\helpers\Images;
use common\models\Courses;
use common\models\Lessons;
use common\models\QuestionsTask;
use common\models\Tasks;
use common\models\Users;
use common\models\UsersAnswers;
use common\models\UsersLessons;
use common\models\VariantsAnswer;
use frontend\components\controllers\DefaultController;
use shketkol\images\src\models\Image;
use Yii;
use yii\web\NotFoundHttpException;


class TasksController extends AdminController
{

    public function actionView()
    {
        if (Yii::$app->request->isAjax) {
            $id = $_GET['id'];
            $model = $this->loadModel($id);

            return $this->renderPartial('view', [
                'model' => $model,
            ]);
        }
    }

    public function actionSend()
    {
        if (Yii::$app->request->isAjax) {
            $post = $_POST;

//            echo '<pre>';
//            var_dump($_POST);
//            echo '</pre>';
//            exit;

            $task = $post['task'];
            $questions = $post['questions'];

            $model = $this->loadModel($task);

            $lesson = UsersLessons::find()
                ->where(['users_id' => Yii::$app->user->id])
                ->andWhere(['lessons_id' => $model->lessons_id])
                ->one();

            $time_model = strtotime($lesson->created_at);
            $sec = $lesson->lessons->duration * 3600;
            $time = date('U', time()) - $time_model;
            $left = $sec - $time;

            if ($left > (float)0) {
                if (!empty($_FILES)) {
                    $task_user = new UsersAnswers();
                    $task_user->users_id = Yii::$app->user->id;
                    $task_user->tasks_id = $task;
                    $task_user->questions_task_id = $questions;
                    $task_user->save(false);
                    foreach ($_FILES as $key => $value) {
                        $file_name = $value['name'];
//                        require ('../../../web/uploads/files/')
                        if (!file_exists($_SERVER['DOCUMENT_ROOT'] . '/frontend/web/uploads/store/Tasks/Tasks' . $task . '/')){
                            mkdir($_SERVER['DOCUMENT_ROOT'] . '/frontend/web/uploads/store/Tasks/Tasks' . $task . '/');
                        }
                        $path = $_SERVER['DOCUMENT_ROOT'] . '/frontend/web/uploads/store/Tasks/Tasks' . $task . '/' . $file_name;
                        move_uploaded_file($value['tmp_name'], $path);

                        $file = new Image();
                        $file->filePath = 'Tasks/Tasks' . $task . '/' . $file_name;
                        $file->itemId = $task_user->id;
                        $file->isMain = 1;
                        $file->modelName = 'Tasks';
                        $file->urlAlias = time();
                        $file->name = Images::TYPE_TASKS;
                        $file->save(false);

                    }

                } else {
                    foreach ($questions as $key => $question) {


                        //вставка автопроверки с добавлением болов сразу пользователю
                        $question_task = QuestionsTask::findOne(['id' => $key]);
                        if ($question_task->format == 2 || $question_task->format == 4 || $question_task->format == 5) {

                            $correct_answer = 0;
                            foreach ($question['variants'] as $variant) {
                                $task_user = new UsersAnswers();
                                $task_user->users_id = Yii::$app->user->id;
                                $task_user->tasks_id = $task;
                                $task_user->questions_task_id = $key;
                                $task_user->answer = $variant;

                                if ($question_task->test == 1) {

                                    $correct = VariantsAnswer::findOne(['id' => $variant]);
                                    if ($correct->correct == 1) {
                                        $task_user->correct = 1;
                                        $task_user->result = 1;
                                        $correct_answer++;
                                    } else if ($correct->correct == 0) {
                                        $task_user->correct = 0;
                                        $task_user->result = 1;
                                    }

                                }
                                $task_user->save(false);
                            }

                            if ($correct_answer == count(VariantsAnswer::findAll(['questions_task_id' => $key, 'correct' => 1]))) {
                                //добавлять балы
                                $user = Users::findOne(['id' => Yii::$app->user->id]);
                                $user->bals = $user->bals + $question_task->rating;
                                $user->save(false);
                            }
                        }

                        if ($question_task->format == 1) {
                            $task_user = new UsersAnswers();
                            $task_user->users_id = Yii::$app->user->id;
                            $task_user->tasks_id = $task;
                            $task_user->questions_task_id = $key;
                            $task_user->answer = $question;
                            $task_user->save(false);
                        }

                    }
                }
            } else {
                if ($lesson->status == 1) {
                    $lesson->status = 2;
                    $lesson->save(false);
                }
            }

            $task_model = Tasks::findOne(['id' => $task]);
            $lessons_model = Lessons::find()
                ->where(['courses_id' => $task_model->lessons->courses_id])
                ->orderBy('position DESC')
                ->one();


            if($lessons_model->id === $task_model->lessons_id){
                if ($lesson->status == 1) {
                    $lesson->status = 2;
                    $lesson->save(false);
                }
            }

            $tasks_lesson = Tasks::find()
                ->where(['lessons_id' => $task_model->lessons_id])
                ->orderBy('position DESC')
                ->one();
            if($tasks_lesson->id === $task_model->lessons_id){
                if ($lesson->status == 1) {
                    $lesson->status = 2;
                    $lesson->save(false);
                }
            }

            return $this->renderPartial('view', [
                'model' => $model,
            ]);
        }
    }

    public function loadModel($id)
    {
        $model = Tasks::findOne(['id' => $id]);
        if ($model === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        return $model;
    }
}

<?php

namespace backend\modules\mycourses\controllers;

use backend\components\controllers\AdminController;
use common\models\Courses;
use common\models\Invoices;
use common\models\Lessons;
use common\models\UsersLessons;
use \frontend\components\controllers\DefaultController;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * Default controller for the `referal` module
 */
class CoursesController extends AdminController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $models = Courses::find()
            ->join('LEFT JOIN', 'invoices', 'invoices.courses_id = courses.id')
            ->where([
                'courses.type' => Courses::TYPE_TEACHER,
//                'invoices.status' => 3
            ])
            ->andWhere(['not', ['courses.status' => 0]])
            ->andWhere(['courses.active' => 1])
            ->all();
        return $this->render('index', [
            'models' => $models,
        ]);
    }

    public function actionView($id)
    {
        $model = $this->loadModel($id);

        $lessons = Lessons::find()
            ->where(['courses_id' => $model->id])
            ->orderBy('position')
            ->asArray()
            ->all();

        $show = false;
        if (!empty(Invoices::findOne(['users_id' => Yii::$app->user->id, 'courses_id' => $model->id, 'status' => 3]))) {
            $show = true;
        }
        if (!empty($model->start) && $model->start > date('Y-m-d', time()) && date('Y-m-d', time()) < $model->finish) {
            $show = false;
        }

        if ($show) {
            $users_lessons = UsersLessons::find()
                ->where(['courses_id' => $model->id])
                ->andWhere(['users_id' => Yii::$app->user->id])
                ->all();

            if (!empty($users_lessons)) {
                $active_lessons_array = array();
                $finish_lessons_array = array();
                foreach ($users_lessons as $users_lesson) {
                    if ($users_lesson->status == 1) {
                        $active_lessons_array[] = $users_lesson->lessons_id;
                    }
                    if ($users_lesson->status == 2) {
                        $finish_lessons_array[] = $users_lesson->lessons_id;
                    }
                }


                foreach ($lessons as $key => $lesson) {
                    if (array_search($lesson['id'], $active_lessons_array) !== false) {
                        $lessons[$key]['active'] = 1;
                    }

                    if (array_search($lesson['id'], $finish_lessons_array) !== false) {
                        $lessons[$key]['active'] = 2;
                    }
                }


                if (count($active_lessons_array) != 0) {
                    foreach ($lessons as $key => $lesson) {
                        if (!empty($lesson['active']) && $lesson['active'] != 1 && $lesson['active'] != 2) {
                            $lessons[$key]['active'] = 3;
                        }
                        if (empty($lesson['active'])) {
                            $lessons[$key]['active'] = 3;
                        }
                    }

                } else {
                    foreach ($lessons as $key => $lesson) {
                        if(!isset($lesson['active'])){
                            $lessons[$key]['active'] = 1;
                            break;
                        } else {
                            if ($lesson['active'] != 1 && $lesson['active'] != 2) {
                                $lessons[$key]['active'] = 1;
                                break;
                            }
                        }
                    }
                    foreach ($lessons as $key => $lesson) {
                        if (!isset($lesson['active']) || ($lesson['active'] != 1 && $lesson['active'] != 2)) {
                            $lessons[$key]['active'] = 3;
                        }
                    }
                }
            } else {
                foreach ($lessons as $key => $lesson) {
                    if ($key == 0) {
                        $lessons[$key]['active'] = 1;
                    } else {
                        $lessons[$key]['active'] = 3;
                    }
                }
            }
        }



        return $this->render('view', [
            'model' => $model,
            'lessons' => $lessons,
            'show' => $show
        ]);
    }

    public function loadModel($id)
    {
        $model = Courses::findOne(['id' => $id]);
        if ($model === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        return $model;
    }
}

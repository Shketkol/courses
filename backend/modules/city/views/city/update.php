<?php
use \yii\bootstrap\ActiveForm;
use \yii\helpers\Url;
use \yii\helpers\Html;
use \common\models\Users;

?>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <? $form = ActiveForm::begin([
                'id' => 'users-update',
                'enableClientValidation'=> false,
                'validateOnSubmit' => true,
                'options' => [
//                'class' => 'form-horizontal',
                    'autocomplete' => 'off',
                ],
                'fieldConfig' => [
                    'template' => "{label}<div class=\"form-line\">{input}<span class=\"help-block hidden\"></span></div>",
                ],
            ]); ?>

            <div class="header">
                <h3 class="box-title m-b-0">Основная информация</h3>
            </div>
            <div class="body">
                <?=$form->errorSummary($model);?>
                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($model, 'name')->textInput(['placeholder' => 'Название', 'required' => true]) ?>
                    </div>
                </div>
                <button type="submit" class="btn btn-lg btn-success waves-effect waves-light m-t-10 m-r-10"><i class="fa fa-spin fa-circle-o-notch hidden"></i>&nbsp; Сохранить &nbsp;</button>
            </div>

            <? ActiveForm::end(); ?>
        </div>
    </div>
</div>
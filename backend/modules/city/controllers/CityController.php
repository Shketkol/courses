<?php

namespace backend\modules\city\controllers;

use backend\components\controllers\AdminController;
use backend\models\CitySearch;
use backend\models\TeachersSearch;
use backend\models\UsersSearch;
use common\models\Cities;
use common\models\Teachers;
use common\models\Users;
use Yii;


class CityController extends AdminController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new CitySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = new Cities();

        if($model->load(Yii::$app->request->post()) && $model->save()){
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = Cities::findOne(['id' => $id]);

        if($model->load(Yii::$app->request->post()) && $model->save()){
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        $model = Cities::findOne(['id' => $id]);
        $model->status = 0;
        $model->save();
            return $this->redirect(['index']);
    }

}

<?php

namespace backend\modules\intent\controllers;

use backend\components\controllers\AdminController;
use backend\models\ReportesSearch;
use common\models\Reportes;
use yii\helpers\Url;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * Default controller for the `users` module
 */
class ReportesController extends AdminController
{
    public function actionIndex()
    {
        $searchModel = new ReportesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = new Reportes();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->save()) {
                return $this->redirect(Url::to(['/intent/intent/view', 'id' => $model->intent_id]));
            }
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->save()) {
            return $this->redirect(Url::to(['/intent/intent/view', 'id' => $model->intent_id]));
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionSuccess($id)
    {
        $model = $this->loadModel($id);
        $model->test = Reportes::REPORT_SUCCESS;
        $model->balls = 1;
        $model->save();

        return $this->redirect('index');
    }

    public function actionWrong($id)
    {
        $model = $this->loadModel($id);
        $model->test = Reportes::REPORT_WRONG;
        $model->save();

        return $this->redirect('index');
    }

    public function loadModel($id)
    {
        $model = Reportes::findOne(['id' => $id]);
        if ($model === null) {
            $answer = json_decode(file_get_contents('php://input'), true);
            if (!empty($answer)) {
                return $this->asJson(array('status' => 'success', 'data' => array(
                    'type' => 'popup',
                    'name' => 'sa-basic',
                    'title' => 'Запись не найдена'
                )));
                exit;
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
        return $model;
    }

}




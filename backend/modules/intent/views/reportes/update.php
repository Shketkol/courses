<?php
use \yii\bootstrap\ActiveForm;
use \yii\helpers\Url;
use \yii\helpers\Html;
use \common\models\Users;

$this->title = 'Отчет';
$this->params['breadcrumbs'][] = (!empty($model->date))?$model->date :'Отчет';

$this->registerJsFile('@web/source/lib/ckeditorbigger/ckeditor.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="body">
                <div class="row">
                    <div class="col-md-12">
                        <p>Пользователь: <?=$model->user->name?></p>
                        <p>Цель: <?=$model->intent->content?></p>
                        <textarea name="" class="useCKEditor" id="ddd" cols="30" rows="10"><?=$model->content?></textarea>
                        <a href="<?=Url::to(['/intent/reportes/success' ,'id' => $model->id])?>" class="btn btn-lg btn-success waves-effect waves-light m-t-10 m-r-10"><i class="fa fa-spin fa-circle-o-notch hidden"></i>&nbsp; Правильно &nbsp;</a>
                        <a href="<?=Url::to(['/intent/reportes/wrong' ,'id' => $model->id])?>" class="btn btn-lg btn-danger waves-effect waves-light m-t-10 m-r-10"><i class="fa fa-spin fa-circle-o-notch hidden"></i>&nbsp; Неправильно &nbsp;</a>
                    </div>
                </div>


            </div>
        </div>

    </div>
</div>
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->registerCssFile('https://cdn.jsdelivr.net/fontawesome/4.5.0/css/font-awesome.min.css');
$this->registerCssFile('@web/public/examples/css/nestable.min.css');
$this->registerJsFile('@web/source/lib/ckeditorbigger/ckeditor.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/public/vendor/jquery.nestable.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/public/examples/js/nestable.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>
<?php $this->registerCssFile('@web/public/lib/air-datepicker-master/dist/css/datepicker.min.css', ['position' => \yii\web\View::POS_BEGIN]); ?>
<?php $this->registerJsFile('@web/public/lib/air-datepicker-master/dist/js/datepicker.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); ?>

<?php $form = ActiveForm::begin([
    'options' => ['enctype' => 'multipart/form-data'],
    'fieldConfig' => [
        'template' => "{label}<div class=\"form-line\">{input}<span class=\"help-block hidden\"></span></div>",
    ],
]); ?>
<div class="card">
    <div class="header">
        <h4 class="card-title">Общая информация</h4>
    </div>
    <div class="body">
        <?=$form->errorSummary($model)?>
        <? if ($model->isNewRecord){
           $model->active = 0;
        }?>
        <?= $form->field($model, "active")->radioList([
                0 => 'не отображать',
                1 => 'отображать',
        ],[
            'item' => function ($index, $label, $name, $checked, $value) {
                return
                    Html::radio($name, $checked, ['value' => $value, 'id' =>'radio_'.$value]).'<label for="radio_'.$value.'">'.$label.'</label>';
            },
        ]) ?>
        <?=$this->render('@app/views/components/upload', [
            'model' => $model,
            'form' => $form,
            'field' => 'image',
            'type' => \common\helpers\Images::$params[\common\helpers\Images::TYPE_COURSES],
            'name' => \common\helpers\Images::TYPE_COURSES,
        ]) ?>

        <?= $form->field($model, 'type')->dropDownList(\common\models\Courses::$types) ?>
        <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'price')->textInput(['maxlength' => true, 'type' => 'number', 'min' => 0]) ?>
        <?= $form->field($model, 'currencies_id')->dropDownList(
            \yii\helpers\ArrayHelper::map(\common\models\Currencies::find()->all(), 'id', 'name')
        ) ?>
        <?= $form->field($model, 'lead')->textarea(['rows' => 6]) ?>
        <?= $form->field($model, 'desc')->textarea(['rows' => 6, 'class' => 'useCKEditor']) ?>
        <?
        if (empty($model->getErrors())){
            if (!empty($model->start)) {
                $start = explode('-', $model->start);
                $model->start = $start[2] . '.' . $start[1] . '.' . $start[0];
            }
        }
        if (empty($model->getErrors())) {
            if (!empty($model->finish)) {
                $finish = explode('-', $model->finish);
                $model->finish = $finish[2] . '.' . $finish[1] . '.' . $finish[0];
            }
        }
        ?>
        <?= $form->field($model, 'start')->textInput(['class' => 'datepicker-here form-control']) ?>
        <?= $form->field($model, 'finish')->textInput(['class' => 'datepicker-here form-control']) ?>
    </div>
</div>

<?//if(!$model->isNewRecord){?>
<!--    <div class="card">-->
<!--        <div class="card-header">-->
<!--            <h4 class="card-title">Уроки</h4>-->
<!--        </div>-->
<!--        <div class="card-block">-->
<!--            <p><a href="--><?//=\yii\helpers\Url::to(['/lessons/lessons/create', 'id' => $model->id])?><!--" class="btn btn-primary">Добавить урок <i class="material-icons">add</i></a></p>-->
<!--            --><?//if(!empty($model->lessons)){?>
<!--                <div class="nestable" id="nestable-handles-primary">-->
<!--                    <ul class="nestable-list">-->
<!--                        --><?//
//                        $lessons = \common\models\Lessons::find()
//                        ->where(['courses_id' => $model->id])
//                        ->orderBy('position')
//                        ->all();
//                        ?>
<!--                        --><?//foreach ($lessons as $lesson){?>
<!--                            --><?// $image = $lesson->getImagesByName(\common\helpers\Images::TYPE_LESSONS)?>
<!--                            <li class="nestable-item nestable-item-handle" data-id="--><?//= $lesson->id ?><!--" data-position="--><?//= $lesson->position ?><!--">-->
<!--                                <div class="nestable-handle"><i class="material-icons">menu</i></div>-->
<!--                                <div class="nestable-content">-->
<!--                                    <div class="media">-->
<!--                                        <div class="media-left media-middle">-->
<!--                                            <img src="--><?//=(!empty($image)) ? $image[0]->getUrl() : null?><!--" alt="" width="100" class="rounded">-->
<!--                                        </div>-->
<!--                                        <div class="media-body media-middle">-->
<!--                                            <h5 class="card-title h6 mb-0">-->
<!--                                                <a href="--><?//=\yii\helpers\Url::to(['/lessons/lessons/update', 'id' => $lesson->id])?><!--">--><?//=$lesson->title?><!--</a>-->
<!--                                            </h5>-->
<!--                                            <small class="text-muted">изменен --><?//=date('d.m.Y', strtotime($lesson->updated_at))?><!--</small>-->
<!--                                        </div>-->
<!--                                        <div class="media-right media-middle">-->
<!--                                            <a href="--><?//=\yii\helpers\Url::to(['/lessons/lessons/update', 'id' => $lesson->id])?><!--" class="btn btn-white btn-sm"><i class="material-icons">edit</i></a>-->
<!--                                        </div>-->
<!--                                        <div class="media-right media-middle">-->
<!--                                            <a href="--><?//=\yii\helpers\Url::to(['/lessons/lessons/delete', 'id' => $lesson->id])?><!--" class="btn btn-white btn-sm"><i class="material-icons">delete</i></a>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </li>-->
<!--                        --><?// }?>
<!--                    </ul>-->
<!--                </div>-->
<!--            --><?// }?>
<!--        </div>-->
<!--    </div>-->
<?// } ?>

<div class="form-group">
    <?= Html::submitButton('Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>

<?
$url = \yii\helpers\Url::to(['/lessons/lessons/change-position']);
$script = <<< JS

$('#nestable-handles-primary').on('change', function(event) {
   $(this).find('li').each(function(p1, p2) {
     var i = p1+1;
     if($(this).data('position') != i){
         $.post('$url',
         {
         "id": $(this).data('id'),
          "position": i,
          },
         function(result) {}
         );
     }
   })  
})
         
JS;
$this->registerJs($script, yii\web\View::POS_END);
?>
<?php

$this->registerCssFile('https://cdn.jsdelivr.net/fontawesome/4.5.0/css/font-awesome.min.css');
$this->registerCssFile('@web/public/examples/css/nestable.min.css');
$this->registerJsFile('@web/source/lib/ckeditorbigger/ckeditor.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/public/vendor/jquery.nestable.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/public/examples/js/nestable.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Курсы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$image = $model->getImagesByName(\common\helpers\Images::TYPE_COURSES)
?>

<h1 class="page-heading h2"><?=$model->title?></h1>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="embed-responsive embed-responsive-16by9">
                <img src="<?=(!empty($image)) ? $image[0]->getUrl('500x300') : null?>">
            </div>
            <div class="body">
                <?=$model->desc?>
            </div>
        </div>
        <?if(!$model->isNewRecord){?>
            <div class="card">
                <div class="header">
                    <h4 class="title">Уроки</h4>
                </div>
                <div class="body">
                    <p>
                    <?if (Yii::$app->user->identity->users_roles_id != \common\models\Users::TYPE_TEACHER):?>
                        <a href="<?=\yii\helpers\Url::to(['/lessons/lessons/create', 'id' => $model->id])?>" class="btn btn-primary">Добавить урок <i class="material-icons">add</i></a>
                    <?endif;?>
                    </p>
                    <?if(!empty($model->lessons)){?>
                        <div class="nestable" id="nestable-handles-primary">
                            <ul class="nestable-list">
                                <?
                                $lessons = \common\models\Lessons::find()
                                    ->where(['courses_id' => $model->id])
                                    ->orderBy('position')
                                    ->all();
                                ?>
                                <?foreach ($lessons as $key=>$lesson){?>
                                    <? $image = $lesson->getImagesByName(\common\helpers\Images::TYPE_LESSONS)?>
                                    <li class="nestable-item nestable-item-handle" data-id="<?= $lesson->id ?>" data-position="<?= $lesson->position ?>">
                                        <div class="nestable-handle"><i class="material-icons">menu</i></div>
                                        <div class="nestable-content">
                                            <div class="media">
                                                <div class="media-left media-middle">
                                                    <img src="<?=(!empty($image)) ? $image[0]->getUrl() : null?>" alt="" width="100" class="rounded">
                                                </div>
                                                <div class="media-body media-middle">
                                                    <h5 class="card-title h6 mb-0">
                                                        <a href="<?=\yii\helpers\Url::to(['/lessons/lessons/view', 'id' => $lesson->id])?>"><?=$key+1?>. <?=$lesson->title?></a>
                                                    </h5>
                                                    <small class="text-muted">изменен <?=date('d.m.Y', strtotime($lesson->updated_at))?></small>
                                                </div>
                                                    <?if (Yii::$app->user->identity->users_roles_id != \common\models\Users::TYPE_TEACHER):?>
                                                        <div class="media-right media-middle">
                                                            <a href="<?=\yii\helpers\Url::to(['/lessons/lessons/update', 'id' => $lesson->id])?>" class="btn btn-white btn-sm"><i class="material-icons">edit</i></a>
                                                        </div>
                                                        <div class="media-right media-middle">
                                                            <a href="<?=\yii\helpers\Url::to(['/lessons/lessons/delete', 'id' => $lesson->id])?>" class="btn btn-white btn-sm"><i class="material-icons">delete</i></a>
                                                        </div>
                                                    <?endif;?>
                                            </div>
                                        </div>
                                    </li>
                                <? }?>
                            </ul>
                        </div>
                    <? }?>
                </div>
            </div>
        <? } ?>
    </div>


</div>
<?
$url = \yii\helpers\Url::to(['/lessons/lessons/change-position']);
$script = <<< JS

$('#nestable-handles-primary').on('change', function(event) {
   $(this).find('li').each(function(p1, p2) {
     var i = p1+1;
     if($(this).data('position') != i){
         $.post('$url',
         {
         "id": $(this).data('id'),
          "position": i,
          },
         function(result) {}
         );
     }
   })  
})
         
JS;
$this->registerJs($script, yii\web\View::POS_END);
?>
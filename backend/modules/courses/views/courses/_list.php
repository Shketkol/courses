<? $image = $model->getImagesByName(\common\helpers\Images::TYPE_COURSES)?>
<div class="col-lg-4">
    <div class="card" style="display: block; max-height: 250px;overflow: hidden;">
        <div class="header">
            <div class="media">
                <div class="media-left media-middle">
                    <img src="<?=(!empty($image)) ? $image[0]->getUrl('100x100') : null?>" alt=""  class="rounded">
                </div>
                <div class="media-body media-middle">
                    <h4 class="card-title"><a href="<?=\yii\helpers\Url::to(['view', 'id' => $model->id])?>"><?=$model->title?></a></h4>
                    <span class="btn bg-indigo waves-effect">
                    <i class="material-icons">money</i>
                    <span><?=$model->price?></span>
                </span>
                    <!--                <span class="tag tag-default">5 SALES</span>-->
                </div>
                <div class="media-right media-middle">
                    <?if (Yii::$app->user->identity->users_roles_id != \common\models\Users::TYPE_TEACHER):?>
                        <a href=" <?=\yii\helpers\Url::to(['update', 'id' => $model->id])?>" class="btn bg-pink btn-circle waves-effect waves-circle waves-float">
                            <i class="material-icons">border_color</i>
                        </a>
                    <? endif;?>

                    <a href=" <?=\yii\helpers\Url::to(['view', 'id' => $model->id])?>" class="btn bg-light-blue btn-circle waves-effect waves-circle waves-float">
                        <i class="material-icons">visibility</i>
                    </a>
                    <?if (Yii::$app->user->identity->users_roles_id != \common\models\Users::TYPE_TEACHER):?>
                        <a href=" <?=\yii\helpers\Url::to(['delete', 'id' => $model->id])?>" class="btn bg-light-green btn-circle waves-effect waves-circle waves-float">
                            <i class="material-icons">delete</i>
                        </a>
                    <? endif;?>

                </div>
            </div>
        </div>
    </div>
</div>

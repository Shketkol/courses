<?
use yii\grid\GridView;
use \yii\helpers\Html;
use \yii\helpers\Url;

$this->title = 'Курсы';
$this->params['breadcrumbs'][] = $this->title;
?>
<?if (Yii::$app->user->identity->users_roles_id != \common\models\Users::TYPE_TEACHER):?>
    <?php $this->beginBlock('navBlock'); ?>
        <? echo Html::a('<i class="fa fa-plus m-r-5"></i> Добавить ', Url::to(['create']), [
            'class' => 'btn btn-success pull-right m-l-10 waves-effect waves-light'
        ]) ?>
    <?php $this->endBlock(); ?>
<?php endif;?>

<?php \yii\widgets\Pjax::begin(); ?>
    <h1 class="page-heading h2"><?=$this->title?></h1>
    <div class="card-columns">
        <?= \yii\widgets\ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '_list',
            'summary' => false
        ]);?>
    </div>
<?php \yii\widgets\Pjax::end(); ?>
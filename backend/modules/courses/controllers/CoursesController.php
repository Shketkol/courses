<?php

namespace backend\modules\courses\controllers;

use backend\components\controllers\AdminController;
use backend\models\CoursesSearch;
use common\helpers\Images;
use common\models\Courses;
use Yii;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

/**
 * Default controller for the `courses` module
 */
class CoursesController extends AdminController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new CoursesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = new Courses();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            $model->setImages(Images::TYPE_COURSES);
            return $this->redirect(Url::to(['index']));
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
        if ($model->load(Yii::$app->request->post())) {

            if($model->validate()){
                if(!empty($model->start)){
                    $start = explode('.', $model->start);
                    $model->start = $start[2] . '-' . $start[1] . '-' . $start[0];
                }
                if(!empty($model->finish)){
                    $finish = explode('.', $model->finish);
                    $model->finish = $finish[2] . '-' . $finish[1] . '-' . $finish[0];
                }
                $model->save(false);
                return $this->redirect(['index']);
            }
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionView($id)
    {
        $model = $this->loadModel($id);
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        $model = $this->loadModel($id);
        $model->status = 0;
        $model->save();
        return $this->redirect(['index']);
    }

    public function loadModel($id)
    {
        $model = Courses::findOne(['id' => $id]);
        if ($model === null){
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        return $model;
    }
}

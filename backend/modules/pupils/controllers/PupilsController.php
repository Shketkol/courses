<?php

namespace backend\modules\pupils\controllers;

use backend\components\controllers\AdminController;
use backend\models\TeachersSearch;
use backend\models\UsersSearch;
use common\models\Teachers;
use common\models\Users;
use Yii;


class PupilsController extends AdminController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->search_user(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = new Teachers();

        if($model->load(Yii::$app->request->post()) && $model->save()){
            $user = Users::findOne(['id' => $model->users_id]);
            $user->users_roles_id = Users::TYPE_TEACHER;
            $user->save(false);
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = Teachers::findOne(['id' => $id]);

        if($model->load(Yii::$app->request->post()) && $model->save()){
            $user = Users::findOne(['id' => $model->users_id]);
            $user->users_roles_id = Users::TYPE_TEACHER;
            $user->save(false);
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

}

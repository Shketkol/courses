<?
use yii\grid\GridView;
use \yii\helpers\Html;
use \yii\helpers\Url;
use \common\models\Users;

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="table-responsive">
                <?php \yii\widgets\Pjax::begin(); ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'summary' => false,
                    'layout' => "{summary}\n{items}\n<div align='right'>{pager}</div>",
                    'tableOptions' => [
                        'class' => 'table table-hover manage-u-table'
                    ],
                    'columns' => [
                        [
                            'attribute'=>'id',
                            'contentOptions' =>['class' => 'text-center'],
                            'headerOptions' => ['class' => 'text-center','style'=>'width: 80px;']
                        ],
                        [
                            'label' => 'Имя',
                            'attribute'=>'name',
                        ],
                        [
                            'label' => 'Email',
                            'attribute'=>'email',
                        ],
                        [
                            'attribute' => 'phone',
                            'value' => function($model) {
                                return (!empty($model->usersProfile->phone)) ? $model->usersProfile->phone : null;
                            },

                        ],
                        [
                            'label' => 'Количество пройденых уроков',
                            'value'=> function($model) {
                                return Users::getCountFinish($model->id);
                            }
                        ],
                        [
                            'label' => 'Количество правильных ответов',
                            'value'=> function($model) {
                                return Users::getCountFinishSuccess($model->id);
                            }
                        ],
                        [
                        'header' => 'Управление',
                        'class' => 'backend\components\grid\CustomActionColumn',
                        'contentOptions' =>['class' => 'text-center'],
                        'headerOptions' => ['class' => 'text-center', 'width' => '80'],
                        'filterOptions' => ['class' => 'text-center'],
                        'filter' => Url::to(['index']),
                        'template' => '{update}',
                        'buttons' => [
                            'update' => function ($model, $key, $index) {
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Url::to(['/users/users/update', 'id' => $key['id']]), [
                                    'data-original-title' => 'Редактировать',
                                    'data-toggle' => 'tooltip',
                                    'class' => (Yii::$app->user->identity->users_roles_id == Users::TYPE_TEACHER) ? 'hidden' : null
                                ]);
                            },
                        ],
                    ],
                    ],
                ]); ?>
                <?php \yii\widgets\Pjax::end(); ?>
            </div>
        </div>
    </div>
</div>

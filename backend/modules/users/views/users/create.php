<?php
use \yii\bootstrap\ActiveForm;
use \yii\helpers\Url;
use \yii\helpers\Html;
use \common\models\Users;

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = (!empty($model->name))?$model->surname.' '.$model->name :'Пользователь';
?>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <? $form = ActiveForm::begin([
                'id' => 'users-update',
                'enableClientValidation'=> false,
                'validateOnSubmit' => true,
                'action' => Url::to(['create']),
                'options' => [
//                    'class' => 'form-horizontal',
                    'autocomplete' => 'off',
                ],
                'fieldConfig' => [
                    'template' => "{label}<div class=\"form-line\">{input}<span class=\"help-block hidden\"></span></div>",
                    'labelOptions' => ['class' => 'col-md-12'],
                ],
            ]); ?>
            <div class="header">
                <h3 class="box-title m-b-0">Основная информация</h3>
            </div>
            <div class="body">
                <div class="row">
                    <div class="col-md-12">
                        <?=$form->errorSummary($model);?>
                        <?$model->status = 10;?>
                        <?= $form->field($model, 'status')->radioList(Users::$Statuses, ['required' => true,
                            'item' => function ($index, $label, $name, $checked, $value) {
                                return
                                    Html::radio($name, $checked, ['value' => $value, 'id' =>'radio_'.$index]).'<label for="radio_'.$index.'">'.$label.'</label>';
                            },
                        ]) ?>
                        <?= $form->field($model, 'email')->textInput(['type' => 'email', 'placeholder' => 'E-mail', 'required' => true]) ?>
                        <?= $form->field($model, 'name')->textInput(['placeholder' => 'Имя', 'required' => true]) ?>
                        <?= $form->field($model, 'surname')->textInput(['placeholder' => 'Фамилия', 'required' => true]) ?>
                        <?= $form->field($model, 'phone')->textInput(['placeholder' => 'Телефон', 'required' => true]) ?>
                        <?= $form->field($model, 'password_hash')->textInput(['type' => 'password','placeholder' => 'Пароль', 'required' => true]) ?>
                        <?= $form->field($model, 'cities_id')->dropDownList(
                            \yii\helpers\ArrayHelper::map(\common\models\Cities::find()->where(['status' => 10])->orderBy('name')->all(), 'id', 'name')
                        ) ?>
                        <?= $form->field($model, 'users_roles_id')->dropDownList(
                            \yii\helpers\ArrayHelper::map(\common\models\UsersRoles::find()->orderBy('name')->all(), 'id', 'name'),
                            array('required' => true)
                        ) ?>

                        <button type="submit" class="btn btn-lg btn-success waves-effect waves-light m-t-10 m-r-10"><i class="fa fa-spin fa-circle-o-notch hidden"></i>&nbsp; Сохранить &nbsp;</button>
                    </div>
                </div>


            </div>


            <? ActiveForm::end(); ?>
        </div>

    </div>
</div>
<?
use yii\grid\GridView;
use \yii\helpers\Html;
use \yii\helpers\Url;
use \common\models\Users;

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $this->beginBlock('navBlock'); ?>
<? echo Html::a('<i class="fa fa-plus m-r-5"></i> Пригласить ', Url::to(['create']), [
    'class' => 'btn btn-success pull-right m-l-10 waves-effect waves-light'
]) ?>
<?php $this->endBlock(); ?>

<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="table-responsive">
                <?php \yii\widgets\Pjax::begin(); ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'summary' => false,
                    'layout' => "{summary}\n{items}\n<div align='right'>{pager}</div>",
                    'tableOptions' => [
                        'class' => 'table table-hover manage-u-table'
                    ],
                    'columns' => [
                        [
                            'attribute'=>'id',
                            'contentOptions' =>['class' => 'text-center'],
                            'headerOptions' => ['class' => 'text-center','style'=>'width: 80px;']
                        ],

                        'name',
                        [
                            'attribute' => 'surname',
                            'value' => function($model) {
                                return (!empty($model->usersProfile->surname)) ? $model->usersProfile->surname : null;
                            },

                        ],
                        'email',
                        [
                            'attribute' => 'phone',
                            'value' => function($model) {
                                return (!empty($model->usersProfile->phone)) ? $model->usersProfile->phone : null;
                            },

                        ],
                        [
                            'attribute' => 'users_roles_id',
                            'label' => 'Роль',
                            'value' => 'usersRoles.name',
                            'filter' => \yii\helpers\ArrayHelper::map(\common\models\UsersRoles::find()->orderBy('name')->asArray()->all(), 'id', 'name'),
                        ],
                        'created_at',
                        [
                            'header' => 'Управление',
                            'class' => 'backend\components\grid\CustomActionColumn',
                            'contentOptions' =>['class' => 'text-center'],
                            'headerOptions' => ['class' => 'text-center', 'width' => '80'],
                            'filterOptions' => ['class' => 'text-center'],
                            'filter' => Url::to(['index']),
                            'template' => '{update}',
                            'buttons' => [

                            ],
                        ],
                    ],
                ]); ?>
                <?php \yii\widgets\Pjax::end(); ?>
            </div>
        </div>
    </div>
</div>

<?php $this->registerCssFile('@web/public/lib/air-datepicker-master/dist/css/datepicker.min.css', ['position' => \yii\web\View::POS_BEGIN]); ?>
<?php $this->registerJsFile('@web/public/lib/air-datepicker-master/dist/js/datepicker.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); ?>
<?php
use \yii\bootstrap\ActiveForm;
use \yii\helpers\Url;
use \yii\helpers\Html;
use \common\models\Users;

$this->title = Yii::$app->user->identity->name;
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['profile']];
$this->params['breadcrumbs'][] = (!empty($model->name)) ? $model->surname . ' ' . $model->name : 'Пользователь';
?>
    <div class="row">
        <div class="col-md-12">
            <div class="card" style="display: flow-root;">
                <div class="body">
                    <? $form = ActiveForm::begin([
                        'id' => 'users-update',
                        'enableClientValidation' => false,
                        'validateOnSubmit' => true,
                        'action' => Url::to(['update', 'id' => $model->id]),
                        'options' => [
//        'class' => 'form-horizontal',
                            'autocomplete' => 'off',
                        ],
                        'fieldConfig' => [
                            'template' => "{label}<div class=\"form-line\">{input}<span class=\"help-block hidden\"></span></div>",
                            'labelOptions' => ['class' => 'col-md-12'],
                        ],
                    ]); ?>
                    <!--                <div class="body">-->
                    <div class="col-md-5">
                        <div class="header">
                            <h3 class="box-title m-b-0">Основная информация</h3>
                        </div>
                        <?= $form->errorSummary($model); ?>
                        <?= $form->field($model, 'email')->textInput(['type' => 'email', 'placeholder' => 'E-mail', 'required' => true]) ?>
                        <?= $form->field($model, 'name')->textInput(['placeholder' => 'Имя', 'required' => true]) ?>
                        <?= $form->field($model, 'surname')->textInput(['placeholder' => 'Фамилия', 'required' => true, 'value' => $model->usersProfile->surname]) ?>
                        <?= $form->field($model, 'phone')->textInput(['placeholder' => 'Телефон', 'required' => true, 'value' => $model->usersProfile->phone]) ?>

                        <? if (!empty($model->birthday) && empty($model->getErrors())) {
                            $birthday = explode('-', $model->birthday);
                            $model->birthday = $birthday[2] . '.' . $birthday[1] . '.' . $birthday[0];
                        } ?>
                        <?= $form->field($model, 'birthday')->textInput(['class' => 'datepicker-here form-control', 'placeholder' => 'Дата рождения', 'required' => true]) ?>

                        <?= $form->field($model, 'sex')->radioList(array(
                            1 => 'Мужской',
                            2 => 'Женский',
                        ), [
                            'item' => function ($index, $label, $name, $checked, $value) {
                                return
                                    Html::radio($name, $checked, ['value' => $value, 'id' => 'radio_' . $index]) . '<label for="radio_' . $index . '">' . $label . '</label>';
                            }
                        ]) ?>

                        <?= $form->field($model, 'cities_id')->dropDownList(
                            \yii\helpers\ArrayHelper::map(\common\models\Cities::find()->where(['status' => 10])->orderBy('name')->all(), 'id', 'name'),
                            array('prompt' => 'Выберите значение')
                        ) ?>

                        <?= $form->field($model, 'country')->textInput(['placeholder' => 'Страна', 'required' => true]) ?>
                    </div>
                    <div class="col-md-5">
                        <div class="title title_user">
                            Загрузить аватар
                        </div>
                        <div class="user_img">
                            <? if (!empty($model->images)) { ?>
                                <? if (strpos($model->images, 'http') === false) { ?>
                                    <img src="/public/common/user_img.png" class="img-responsive" alt="">
                                <? } else { ?>
                                    <img src="<?= $model->images ?>" class="img-responsive" alt="">
                                <? } ?>
                            <? } else { ?>
                                <? $image = $model->getImagesByName(\common\helpers\Images::TYPE_USERS); ?>
                                <? if (!empty($image[0])) { ?>
                                    <img src="<?= $image[0]->getUrl('209x209') ?>" class="img-responsive" alt="">
                                <? } else { ?>
                                    <img src="/public/common/user_img.png" class="img-responsive" alt="">
                                <? } ?>
                            <? } ?>

                        </div>

                        <div class="button_img">
                            <?= $form->field($model, 'img_file')->fileInput(['onchange' => 'previewFile()'])->label(false) ?>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-success">Сохранить</button>
                    </div>
                    <!--                </div>-->
                    <? $form->end(); ?>
                </div>
            </div>

        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <div class="row">
                    <div class="col-xs-9">
                        <h3 class="box-title m-b-0">Смена пароль</h3>
                    </div>
                </div>
                <div class="card" style="display: flow-root;">
                    <? $form = ActiveForm::begin([
                        'id' => 'users-update',
                        'action' => Url::to(['/users/profile/password', 'id' => $modelPass->id]),
                        'enableClientValidation' => false,
                        'validateOnSubmit' => true,
                        'options' => [
//                                        'class' => 'form-horizontal',
                            'autocomplete' => 'off',
                        ],
                        'fieldConfig' => [
                            'template' => "{label}<div class=\"form-line\">{input}<span class=\"help-block hidden\"></span></div>",
                            'labelOptions' => ['class' => 'col-md-12'],
                        ],
                    ]); ?>
                    <div class="body">
                        <?= $form->errorSummary($modelPass); ?>
                        <?= $form->field($modelPass, 'password_hash')->textInput(['type' => 'password', 'value' => '', 'required' => true])->error(false) ?>
                        <?= $form->field($modelPass, 'newPassword')->textInput(['type' => 'password', 'value' => '', 'required' => true])->label('Новый пароль')->error(false) ?>
                        <div class="form-group field-users-confirm_password">
                            <label class="col-md-12" for="confirm_password">Повторить пароль</label>
                            <div class="form-line">
                                <input id="confirm_password" class="form-control" value="" type="password" required>
                                <span class="help-block hidden"></span>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-success">Сохранить</button>
                        </div>
                    </div>
                </div>
                <? $form->end(); ?>
            </div>
        </div>
    </div>


    <script>
        var password = document.getElementById("users-newpassword")
            , confirm_password = document.getElementById("confirm_password");

        function validatePassword() {
            if (password.value != confirm_password.value) {
                confirm_password.setCustomValidity("Пароли не совпадают");
            } else {
                confirm_password.setCustomValidity('');
            }
        }

        password.onchange = validatePassword;
        confirm_password.onkeyup = validatePassword;

        function previewFile() {
            var preview = document.querySelector('.user_img img');
            var file = document.querySelector('input[type=file]').files[0];
            var reader = new FileReader();

            reader.addEventListener("load", function () {
                preview.src = reader.result;
            }, false);

            if (file) {
                reader.readAsDataURL(file);
            }
        }
    </script>


<?
$script = <<< JS
   $(document).ready(function() {
       $("select").styler();
   });
JS;
$this->registerJs($script, yii\web\View::POS_END);
?>
<?php

namespace backend\modules\users\assets;


use yii\web\AssetBundle;

class UsersAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $css = [
        'admin/plugins/bower_components/typeahead.js-master/jquery.typeahead.min.css'
    ];
    public $js = [
        'admin/plugins/bower_components/typeahead.js-master/jquery.typeahead.min.js',
//        'admin/source/controllers/users/usersController.js',
    ];
    public $depends = [
        'frontend\assets\MainAsset'
    ];
    public $cssOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];
}

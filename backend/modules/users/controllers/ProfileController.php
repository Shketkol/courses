<?php

namespace backend\modules\users\controllers;

use backend\components\controllers\AdminController;
use backend\models\UsersSearch;
use common\helpers\Images;
use common\models\Customers;
use common\models\Users;
use common\models\UsersProfile;
use frontend\components\controllers\DefaultController;
use yii\filters\AccessControl;
use yii\helpers\Url;
use Yii;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\web\User;

/**
 * Default controller for the `users` module
 */
class ProfileController extends AdminController
{
    public function actionProfile(){
        $model = $this->loadModel(Yii::$app->user->id);
        return $this->render('profile', [
            'model' => $model,
        ]);
    }

    public function actionUpdate()
    {
        $id = Yii::$app->user->id;
        $model = $this->loadModel($id);
        $modelPass = $this->loadModel($id);
        $model->cities_id = $model->usersProfile->cities_id;
        $model->sex = $model->usersProfile->sex;
        $model->country = $model->usersProfile->country;
        if(!empty($model->usersProfile->birthday)){
            $model->birthday = $model->usersProfile->birthday;
        }
        if(!empty($model->usersProfile->type)){
            $model->content = $model->usersProfile->content;
        }

        if(Yii::$app->request->post()){
            if (!empty($_POST['Users']['password_hash'])) {
                if ($model->validatePass($_POST['Users']['password_hash']) && $model->validate('password_hash')) {
                    $model->password_hash = Yii::$app->security->generatePasswordHash($_POST['Users']['newPassword']);
                    $model->auth_key = Yii::$app->security->generateRandomString();
                    if ($model->save(false)) {
                        return $this->redirect(['profile']);
                    } else {
                        //Alert::addError(\help\bt('Incorrect Old Password', 'models/user'));
                        return $this->render('update', [
                            'model' => $model,
                            'pass' => true,
                        ]);
                    }
                } else {
                    //Alert::addError(\help\bt('Incorrect Old Password', 'models/user'));
                    return $this->render('update', [
                        'model' => $model,
                        'pass' => true,
                    ]);
                }
            } else {
                if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->save()) {

                    $image = UploadedFile::getInstance($model, 'img_file');
                    if(!empty($image)){

                        $model->removeImages();

                        $path = $_SERVER['DOCUMENT_ROOT'].'/frontend/web/uploads/files/'.$model->id.'.'.$image->name;
                        move_uploaded_file($image->tempName, $path);
                        $model->attachImage($path, false, Images::TYPE_USERS);
                        unlink($path);
                        $model->images = null;
                    }

                    $userProfile = UsersProfile::findOne(['users_id' => $model->id]);
                    $userProfile->surname = $model->surname;
                    $userProfile->phone = $model->phone;
                    $userProfile->cities_id = $model->cities_id;
                    if(!empty($model->birthday)){
                        $bidthday = explode('.', $model->birthday);
                        $userProfile->birthday = $bidthday[2] . '-' . $bidthday[1] . '-' . $bidthday[0];

                        if (strtotime($userProfile->birthday) > time()){
                            $model->addError('birthday', 'Дата рождения установлена позже текущей даты');
                            return $this->render('update', [
                                'model' => $model,
                                'modelPass' => $modelPass
                            ]);
                        }
                    }
                    $userProfile->sex = $model->sex;
                    $userProfile->country = $model->country;
                    if($userProfile->save()){
                        return $this->redirect(['profile']);
                    } else {
                        var_dump($userProfile->errors);die;
                    }
                }
            }
        }



        return $this->render('update', [
            'model' => $model,
            'modelPass' => $modelPass
        ]);
    }

    public function actionPassword($id)
    {
        $modelPass = $this->loadModel($id);
        $model = $this->loadModel($id);
        $model->cities_id = $model->usersProfile->cities_id;
        $model->sex = $model->usersProfile->sex;
        $model->country = $model->usersProfile->country;
        if(!empty($model->usersProfile->birthday)){
            $model->birthday = $model->usersProfile->birthday;
        }
        if(!empty($model->usersProfile->type)){
            $model->content = $model->usersProfile->content;
        }
        if (Yii::$app->request->post()){
            if ($modelPass->validatePass($_POST['Users']['password_hash'])
                && $modelPass->validatePassLenth($_POST['Users']['newPassword'])
                && $modelPass->validateBreak($_POST['Users']['newPassword'])) {
                $modelPass->password_hash = Yii::$app->security->generatePasswordHash($_POST['Users']['newPassword']);
                $modelPass->auth_key = Yii::$app->security->generateRandomString();
                $modelPass->save(false);
                return $this->redirect(['profile']);
            } else {
                return $this->render('update', [
                    'model' => $model,
                    'pass' => true,
                    'modelPass' => $modelPass
                ]);
            }
        } else {
            return $this->redirect(['update']);
        }
    }

    public function loadModel($id)
    {
        $model = Users::findOne(['id' => $id]);
        if ($model === null) {
                throw new NotFoundHttpException('The requested page does not exist.');
        }
        return $model;
    }


}




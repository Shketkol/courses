<?php

namespace backend\modules\users\controllers;

use backend\components\controllers\AdminController;
use backend\models\UsersSearch;
use common\models\Customers;
use common\models\Users;
use common\models\UsersProfile;
use yii\filters\AccessControl;
use yii\helpers\Url;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * Default controller for the `users` module
 */
class UsersController extends AdminController
{

    public function actionIndex()
    {
        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->search_admin(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = new Users();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            if ($model->status == Users::STATUS_INVITED){
                $model->addError('status', 'Значение «Статус» неверно.". Данные пользователя не изменены');
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
            $model->password_hash = Yii::$app->security->generatePasswordHash($model->password_hash);
            $model->save();
            $userProfile = new UsersProfile();
            $userProfile->users_id = $model->id;
            $userProfile->surname = $model->surname;
            $userProfile->phone = $model->phone;
            $userProfile->cities_id = $model->cities_id;
            if (!$userProfile->save()){

            }

            return $this->redirect(['/users/users/index']);
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
        if (!empty($model->usersProfile->type)) {
            $model->content = $model->usersProfile->content;
        }
        $model->cities_id = $model->usersProfile->cities_id;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            if ($model->status == Users::STATUS_INVITED){
                $model->addError('status', 'Значение «Статус» неверно.". Данные пользователя не изменены');
                return $this->render('update', [
                    'model' => $model,
                ]);
            }

            $model->save();
            $userProfile = UsersProfile::findOne(['users_id' => $model->id]);
            if (empty($userProfile)) {
                $userProfile = new UsersProfile();
                $userProfile->users_id = $model->id;

            }
            $userProfile->surname = $model->surname;
            $userProfile->phone = $model->phone;
            $userProfile->cities_id = $model->cities_id;

            if ($userProfile->save()) {
                return $this->redirect(['index']);
            } else {

            }
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        if (Users::deleteUser($id)) {
            Yii::$app->session->setFlash('success_message', array('title' => 'Пользователь удален', 'desc' => 'Данный пользователь удален'));
            return $this->asJson(array('status' => 'success', 'data' => array(
                'type' => 'url',
                'href' => Url::to(['index']),
            )));
        } else {
            return $this->asJson(array('status' => 'error'));
        }
    }

    public function actionInvite($id)
    {
        $model = $this->loadModel($id);
        if ($model->sendInvite()) {
            return $this->asJson(array('status' => 'success', 'data' => array(
                'type' => 'popup',
                'name' => 'sa-success',
                'title' => 'Приглашение отправлено',
                'desc' => 'Данному пользователю приглашение отправлено повторно'
            )));
        } else {
            return $this->asJson(array('status' => 'error'));
        }
    }

    public function loadModel($id)
    {
        $model = Users::findOne(['id' => $id]);
        if ($model === null) {
            $answer = json_decode(file_get_contents('php://input'), true);
            if (!empty($answer)) {
                return $this->asJson(array('status' => 'success', 'data' => array(
                    'type' => 'popup',
                    'name' => 'sa-basic',
                    'title' => 'Запись не найдена'
                )));
                exit;
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
        return $model;
    }

    public function formField($post, $model, $create = true)
    {
        if (!empty($post)) {

            if ($post['status'] == Users::STATUS_INVITED) {
                $model->scenario = Users::SCENARIO_INVITE;
                $model->email = $post['email'];
            } elseif ($post['status'] == Users::STATUS_REQUESTED) {
                $model->scenario = Users::SCENARIO_REQUEST;
            } else {
                $model->scenario = Users::SCENARIO_USER;
            }

            if ($model->customers_id == null && $model->status == Users::STATUS_REQUESTED) {
                $model->customers_id = (isset($post['customers_id'])) ? $post['customers_id'] : null;
            }

            $model->status = (isset($post['status'])) ? $post['status'] : $model->status;
            $model->users_roles_id = $post['role'];
            $model->name = (isset($post['name'])) ? $post['name'] : null;
            $model->surname = (isset($post['surname'])) ? $post['surname'] : null;
            $model->patronymic = (isset($post['patronymic'])) ? $post['patronymic'] : null;
            $model->phone = (isset($post['phone'])) ? $post['phone'] : null;
            $model->post = (isset($post['post'])) ? $post['post'] : null;

            if ($model->validate()) {

                if ($create) {
                    $model->email = $post['email'];
                    $model->email_confirmed = 0;
                    $model->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
                    $model->auth_key = Yii::$app->security->generateRandomString();
                    $model->type = Users::TYPE_USER;
                }

                if ($model->save()) {
                    $user = null;
                    if ($create === true) {
                        $user = new UsersProfile();
                    } else {
                        $user = UsersProfile::findOne(['users_id' => $model->id]);
                        if (empty($user)) {
                            $user = new UsersProfile();
                        }
                    }
                    $user->users_id = $model->id;
                    $user->surname = (!empty($model->surname)) ? $model->surname : null;
                    $user->patronymic = (!empty($model->patronymic)) ? $model->patronymic : null;
                    $user->phone = (!empty($model->phone)) ? $model->phone : null;
                    $user->post = (!empty($model->post)) ? $model->post : null;
                    if ($user->save()) {
                        return $this->asJson(array('status' => 'success', 'data' => array(
                            'type' => 'popup',
                            'name' => 'sa-success',
                            'title' => 'Данные сохранены',
                            'desc' => 'Данные пользователя сохранены.',
                            'refresh' => true
                        )));
                    } else {
                        $result = [];
                        foreach ($user->errors as $key => $value) {
                            $result['users-' . $key] = $value;
                        }
                        return $this->asJson(array('status' => 'error', 'data' => $result));
                    }
                }
            } else {
                $result = [];
                foreach ($model->errors as $key => $value) {
                    $result['users-' . $key] = $value;
                }
                return $this->asJson(array('status' => 'error', 'data' => $result));
            }
        }
    }

}




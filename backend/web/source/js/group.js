$('#teachers-courses_id').on('change', function (event) {
    var id = $(this).find('option:selected').val();
    $.ajax({
        type: "GET",
        url: '/admin/teachers/teachers/group',
        data: 'id='+id,
        success: function(data) {
            $('.js_group').html(data);
        }
    });
});


$('#teachers-courses_id').ready(function (event) {
    var id = 0;
    $(this).find('option').each(function () {
        if($(this).is(':selected')){
            id = $(this).val();
        }
    })
    $.ajax({
        type: "GET",
        url: '/admin/teachers/teachers/group',
        data: 'id='+id,
        success: function(data) {
            $('.js_group').html(data);
        }
    });
});
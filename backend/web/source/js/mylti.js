var count = 0;
$('html').on('click', '.add-item', function() {

    var field = $(this).parent('.panel-heading').parent('.panel-default').find('.content_field').find('.content-add').html();
    count +=1;
    field = field.replace(/[[]]/g, '['+count+']');

    field = '<div class="panel-body container-items">'+
        field+
        '</div>';
    $('.content_field').append(field);

});

$('body').on('click', '.remove-item', function () {
    var field = $(this).parent('.panel-heading').parent('.item.panel.panel-default').parent('.panel-body');
    field.remove();
});

$('body').on('change', '.input', function () {
    count = $(this).length;
    console.log(count);
});
$('body').ready(function () {
    count = $(this).find('.input').length;
});
<?php

namespace backend\models;

use common\models\Invoices;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Users;

/**
 * UsersSearch represents the model behind the search form about `common\models\Users`.
 */
class InvoicesSearch extends Invoices
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status'], 'integer'],
            [['name', 'email', 'created_at', 'user_id', 'courses_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Invoices::find();
//            ->joinWith(['usersProfile'])
//            ->andWhere(['users.users_roles_id' => Users::TYPE_SCHOLAR]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'royalti' => [
                        'asc' => [
                            'id' => SORT_ASC,
                            'email' => SORT_ASC,
                            'created_at' => SORT_ASC,
                            'user_id' => SORT_ASC,
                            'courses_id' => SORT_ASC,
                        ],
                        'desc' => [
                            'id' => SORT_DESC,
                            'email' => SORT_DESC,
                            'created_at' => SORT_DESC,
                            'user_id' => SORT_DESC,
                            'courses_id' => SORT_DESC,
                        ],
                        'default' => SORT_DESC,
                    ],
                ],
            ],
            'pagination' => ['pageSize' => 20],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
//        $query->andFilterWhere([
//            'users.id' => $this->id,
//            'users.status' => $this->status,
//            'users.users_roles_id' => $this->users_roles_id,
//            'email_confirmed' => $this->email_confirmed,
//            'created_at' => $this->created_at,
//            'updated_at' => $this->updated_at,
//        ]);
//
//        $query->andFilterWhere(['like', 'users.name', $this->name])
//            ->andFilterWhere(['like', 'email', $this->email])
//            ->andFilterWhere(['like', 'surname', $this->surname])
//            ->andFilterWhere(['like', 'phone', $this->phone]);

        return $dataProvider;
    }
}

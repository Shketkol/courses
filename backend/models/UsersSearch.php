<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Users;

/**
 * UsersSearch represents the model behind the search form about `common\models\Users`.
 */
class UsersSearch extends Users
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status'], 'integer'],
            [['name', 'email', 'created_at', 'updated_at', 'surname', 'phone', 'role', 'users_roles_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search_user($params)
    {
        $query = Users::find()
            ->joinWith(['usersProfile'])
            ->andWhere(['users.users_roles_id' => Users::TYPE_SCHOLAR]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                        'asc' => [
                            'id' => SORT_ASC,
                            'name' => SORT_ASC,
                            'email' => SORT_ASC,
                            'phone' => SORT_ASC,
                            'created_at' => SORT_ASC,
                        ],
                        'desc' => [
                            'id' => SORT_DESC,
                            'name' => SORT_DESC,
                            'email' => SORT_DESC,
                            'phone' => SORT_DESC,
                            'created_at' => SORT_DESC,
                        ],
                        'default' => SORT_DESC,
                ],
            ],
            'pagination' => ['pageSize' => 20],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'users.id' => $this->id,
            'users.status' => $this->status,
            'users.users_roles_id' => $this->users_roles_id,
            'email_confirmed' => $this->email_confirmed,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'users.name', $this->name])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'surname', $this->surname])
            ->andFilterWhere(['like', 'created_at', $this->created_at])
            ->andFilterWhere(['like', 'phone', $this->phone]);

        return $dataProvider;
    }

    public function search_admin($params)
    {
        $query = Users::find()
            ->joinWith(['usersProfile']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'asc' => [
                        'id' => SORT_ASC,
                        'name' => SORT_ASC,
                        'email' => SORT_ASC,
                        'phone' => SORT_ASC,
                        'created_at' => SORT_ASC,
                    ],
                    'desc' => [
                        'id' => SORT_DESC,
                        'name' => SORT_DESC,
                        'email' => SORT_DESC,
                        'phone' => SORT_DESC,
                        'created_at' => SORT_DESC,
                    ],
                    'default' => SORT_DESC,
                ],
            ],
            'pagination' => ['pageSize' => 20],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'users.id' => $this->id,
            'users.status' => $this->status,
            'users.users_roles_id' => $this->users_roles_id,
            'email_confirmed' => $this->email_confirmed,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'users.name', $this->name])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'surname', $this->surname])
            ->andFilterWhere(['like', 'created_at', $this->created_at])
            ->andFilterWhere(['like', 'phone', $this->phone]);

        return $dataProvider;
    }


}

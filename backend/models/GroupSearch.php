<?php

namespace backend\models;

use common\models\Groups;
use common\models\Invoices;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Users;

/**
 * UsersSearch represents the model behind the search form about `common\models\Users`.
 */
class GroupSearch extends Groups
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['courses_id', 'id'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Groups::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => false,
            'pagination' => ['pageSize' => 20],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
//        $query->andFilterWhere([
//            'users.id' => $this->id,
//            'users.status' => $this->status,
//            'users.users_roles_id' => $this->users_roles_id,
//            'email_confirmed' => $this->email_confirmed,
//            'created_at' => $this->created_at,
//            'updated_at' => $this->updated_at,
//        ]);
//
//        $query->andFilterWhere(['like', 'users.name', $this->name])
//            ->andFilterWhere(['like', 'email', $this->email])
//            ->andFilterWhere(['like', 'surname', $this->surname])
//            ->andFilterWhere(['like', 'phone', $this->phone]);

        return $dataProvider;
    }
}

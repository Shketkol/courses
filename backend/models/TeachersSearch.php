<?php

namespace backend\models;

use common\models\Courses;
use common\models\Teachers;
use common\models\Users;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;

/**
 * UsersSearch represents the model behind the search form about `common\models\Users`.
 */
class TeachersSearch extends Teachers
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'users_id', 'cities_id'], 'integer'],
            [['email', 'phone'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $data = array();
        foreach (Teachers::find()->all() as $key=>$value){
            $ar = array(
                'id' => $value->id,
                'name' => $value->users->name,
                'email' => $value->users->email,
                'phone' => (!empty($value->users->usersProfile->phone)) ? $value->users->usersProfile->phone : null,
                'count_course' => Teachers::getCountCourses($value->id),
                'count_pupils' => Teachers::getCountPupils($value->users_id)
            );
            $data[$key] = $ar;
        }



        // add conditions that should always apply here

        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'sort' => [
                'attributes' => [
                    'royalti' => [
                        'asc' => [
                            'id' => SORT_ASC,
                            'name' => SORT_ASC,
                            'email' => SORT_ASC,
                            'phone' => SORT_ASC,
                            'count_course' => SORT_ASC,
                            'count_pupils' => SORT_ASC,
                        ],
                        'desc' => [
                            'id' => SORT_DESC,
                            'name' => SORT_DESC,
                            'email' => SORT_DESC,
                            'phone' => SORT_DESC,
                            'count_course' => SORT_DESC,
                            'count_pupils' => SORT_DESC,
                            ],
                        'default' => SORT_DESC,
                    ],
                ],
                'defaultOrder' => array(
                    'royalti' => SORT_DESC
                )
            ],
            'pagination' => [ 'pageSize' => 20 ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        return $dataProvider;
    }
}

<?php

namespace backend\models;

use common\models\Courses;
use common\models\Teachers;
use common\models\Users;
use common\models\UsersAnswers;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * UsersSearch represents the model behind the search form about `common\models\Users`.
 */
class AnswersSearch extends Teachers
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'users_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params, $teacher, $type)
    {
        $teacher_model = Teachers::findOne(['users_id' => $teacher]);

        $teacher_all = [];
        if (!empty($teacher_model->courses)){
            foreach ($teacher_model->courses  as $key=>$item){
                $teacher_all[] = $item->courses_id;
            }
        }

        if(Yii::$app->user->identity->users_roles_id == Users::TYPE_ADMIN){
            $query = UsersAnswers::find()
                ->join('LEFT JOIN', 'users', 'users.id = users_answers.users_id')
                ->join('LEFT JOIN', 'users_profile', 'users.id = users_answers.users_id')
                ->join('LEFT JOIN', 'tasks', 'tasks.id = users_answers.tasks_id')
                ->join('LEFT JOIN', 'lessons', 'lessons.id = tasks.lessons_id')
                ->join('LEFT JOIN', 'courses', 'courses.id = lessons.courses_id')
                ->where(['courses.type' => $type])
                ->groupBy('users_answers.tasks_id, users_answers.questions_task_id, users.id')
                ->orderBy('users_answers.result ASC')
            ;
        }

        if(Yii::$app->user->identity->users_roles_id == Users::TYPE_TEACHER){
            $query = UsersAnswers::find()
                ->join('LEFT JOIN', 'users', 'users.id = users_answers.users_id')
                ->join('LEFT JOIN', 'users_profile', 'users.id = users_answers.users_id')
                ->join('LEFT JOIN', 'tasks', 'tasks.id = users_answers.tasks_id')
                ->join('LEFT JOIN', 'lessons', 'lessons.id = tasks.lessons_id')
                ->join('LEFT JOIN', 'courses', 'courses.id = lessons.courses_id')
                ->where(['users_profile.cities_id' => $teacher_model->users->usersProfile->cities_id])
                ->andWhere(['courses.type' => $type])
                ->andWhere(['IN', 'courses.id', $teacher_all])
                ->groupBy('users_answers.tasks_id, users_answers.questions_task_id, users.id')
                ->orderBy('users_answers.result ASC')
                ->andWhere(['IN', 'courses.id', $teacher_all])
            ;
        }



        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [ 'pageSize' => 20 ],
            'sort' => false
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'users_id' => $this->users_id,
        ]);


        return $dataProvider;
    }
}

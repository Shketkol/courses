var count = 0;
$('html').on('click', '.add-item', function() {

    var field = $(this).parent('.panel-heading').parent('.panel-default').find('.content_field').find('.content-add').html();
    count +=1;
    field = field.replace(/[[]]/g, '['+count+']');

    field = '<div class="panel-body container-items">'+
            field+
            '</div>';
    $('.content_field').append(field);

});

$('body').on('click', '.remove-item', function () {
    var field = $(this).parent('.panel-heading').parent('.item.panel.panel-default').parent('.panel-body');
    field.remove();
});

$('body').on('change', '.input', function () {
    count = $(this).length;
    console.log(count);
});
$('body').ready(function () {
    count = $(this).find('.input').length;
});

$('.useCKEditor').each(function () {
    CKEDITOR.replace($(this).attr('id'));
});

$('.balance-history-index .js_success').on('click', function () {
    $('.balance-history-index form').attr('action', '/admin/balance-history/output');
    $('.balance-history-index form').submit();
})

$('.balance-history-index .js_error').on('click', function () {
    $('.balance-history-index form').attr('action', '/admin/balance-history/error');
    $('.balance-history-index form').submit();
})

$('.js_test').on('change', function () {
   if($(this).is(':checked')){
       $.ajax({
           type: "GET",
           url: "/admin/site/checkbox",
           data: 'type=1',
           success: function(data) {
               if(data.success){

               }else {
                   alert(data.desc);
               }
           }
       });
   } else {
       $.ajax({
           type: "GET",
           url: "/admin/site/checkbox",
           data: 'type=0',
           success: function(data) {
               if(data.success){
               }else {
                   alert(data.desc);
               }
           }
       });
   }
});
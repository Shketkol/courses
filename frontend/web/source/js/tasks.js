$('.js_tasks').on('click', function (event) {

    var id = $(this).data('id');
    var li = $(this).parent('li');

    $.ajax({
        type: "GET",
        url: '/tasks/tasks/view',
        data: 'id='+id,
        success: function(data) {
            $('.js_form_task').html(data);

            $('.js_list_tasks li').each(function () {
               $(this).removeClass('active');
            });

            li.addClass('active');
        }
    });
})

$('.js_finish_lessons').on('click', function (event) {

    var id = $(this).data('id');

    $.ajax({
        type: "GET",
        url: '/lessons/lessons/send',
        data: 'id='+id,
        success: function(data) {
        }
    });
})

$(document).on('submit', '.questions', function (event) {
    event.preventDefault();

    var form_data = $(this).serialize();

    console.log($('#exampleInputFile').length)
    if($('#exampleInputFile').length != 0){
        var file = $('#exampleInputFile')[0].files;

        var form_data = new FormData();
        if(file.length != 0){
            form_data.append('file', file[0]);
            form_data.append('task', $('.js_file_task').val());
            form_data.append('questions', $('.js_file_questlion').val());
        }

        $.ajax({
            type: "POST",
            url: '/tasks/tasks/send',
            data: form_data,
            dataType: 'text',
            cache: false,
            contentType: false,
            processData: false,
            success: function(data) {
                $('.js_form_task').html(data);
            }
        });
    } else {
        $.ajax({
            type: "POST",
            url: '/tasks/tasks/send',
            data: form_data,
            success: function(data) {
                $('.js_form_task').html(data);
            }
        });
    }





    return false;
})
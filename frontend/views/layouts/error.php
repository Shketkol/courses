<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use frontend\assets\AdminMainAsset;
use frontend\assets\AdminHeadAsset;

AdminHeadAsset::register($this);
AdminMainAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" ng-app="myApp">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= Html::encode($this->title) ?></title>
    <!-- Bootstrap Core CSS -->
    <link href="/public/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- animation CSS -->
    <link href="/public/css/animate.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="/public/css/style.css" rel="stylesheet">
    <!-- color CSS -->
    <link href="/public/css/colors/default.css" id="theme"  rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<!-- Preloader -->

<section id="wrapper" class="error-page">
    <div class="error-box">
        <div class="error-body text-center">
            <h1 class="text-info">404</h1>
            <h3 class="text-uppercase">Такой страницы не существует</h3>
            <p class="text-muted m-t-30 m-b-30">Вероятно, ссылка, по которой вы сюда попали, устарела, или вы ошиблись, когда набирали адрес.</p>
            <a href="<?=Yii::$app->getHomeUrl();?>" class="btn btn-info waves-effect waves-light m-b-40">На главную страницу</a> </div>
        <footer class="footer text-center">2017 © B2B Portal.</footer>
    </div>
</section>
<!-- jQuery -->
<script src="/plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="/public/bootstrap/dist/js/bootstrap.min.js"></script>

</body>
</html>
<?php $this->endPage() ?>
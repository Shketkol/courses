<?
use \yii\helpers\Url;
?>
<!-- Sidebar -->
<div class="mdk-drawer mdk-js-drawer" id="default-drawer">
    <div class="mdk-drawer__content ">
        <div class="sidebar sidebar-left sidebar-light sidebar-transparent-sm-up sidebar-p-y">
            <ul class="sidebar-menu">
                <li class="sidebar-menu-item">
                    <a class="sidebar-menu-button" href="<?=Url::to(['/courses/courses/index'])?>">
                        <i class="sidebar-menu-icon material-icons">import_contacts</i> Курсы
                    </a>
                </li>
                <li class="sidebar-menu-item">
                    <a class="sidebar-menu-button" href="<?=Url::to(['/progress/progress/index'])?>">
                        <i class="sidebar-menu-icon material-icons">trending_up</i> Успеваемость
                    </a>
                </li>
                <li class="sidebar-menu-item">
                    <a class="sidebar-menu-button" href="<?=Url::to(['/rating/rating/index'])?>">
                        <i class="sidebar-menu-icon material-icons">poll</i> Рейтинг
                    </a>
                </li>
                <li class="sidebar-menu-item">
                    <a class="sidebar-menu-button" href="<?=Url::to(['/users/users/profile'])?>">
                        <i class="sidebar-menu-icon material-icons">account_box</i> Профиль
                    </a>
                </li>
                <li class="sidebar-menu-item">
                    <a class="sidebar-menu-button" href="/logout">
                        <i class="sidebar-menu-icon material-icons">lock_open</i> Выход
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- // END Sidebar -->
<!-- #Top Bar -->
<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <!-- User Info -->
        <div class="user-info">
            <div class="image">
                <!--                <img src="../../images/user.png" width="48" height="48" alt="User" />-->
            </div>
            <div class="info-container">
                <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?=Yii::$app->user->identity->name?></div>
                <div class="email"><?=Yii::$app->user->identity->email?></div>
                <div class="btn-group user-helper-dropdown">
                    <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                    <ul class="dropdown-menu pull-right">
                        <li><a href="<?=\yii\helpers\Url::to(['/users/users/profile'])?>"><i class="material-icons">person</i>Профиль</a></li>
                        <li role="seperator" class="divider"></li>
                        <li><a href="<?=\yii\helpers\Url::to(['/intent/intent/index'])?>"><i class="material-icons">group</i>Цели</a></li>
<!--                        <li><a href="javascript:void(0);"><i class="material-icons">shopping_cart</i>Sales</a></li>-->
<!--                        <li><a href="javascript:void(0);"><i class="material-icons">favorite</i>Likes</a></li>-->
                        <li role="seperator" class="divider"></li>
                        <li><a href="/logout"><i class="material-icons">input</i>Выход</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- #User Info -->
        <!-- Menu -->
        <div class="menu">
            <ul class="list">
                <li class="header">НАВИГАЦИЯ</li>
                <li>
                    <a href="<?=\yii\helpers\Url::to(['/courses/courses/index'])?>">
                        <i class="material-icons">import_contacts</i>
                        <span>Курсы</span>
                    </a>
                </li>
                <li>
                    <a href="<?=\yii\helpers\Url::to(['/progress/progress/index'])?>">
                        <i class="material-icons">trending_up</i>
                        <span>Успеваемость</span>
                    </a>
                </li>
                <li>
                    <a href="<?=\yii\helpers\Url::to(['/rating/rating/index'])?>">
                        <i class="material-icons">poll</i>
                        <span>Рейтинг</span>
                    </a>
                </li>
                <li>
                    <a href="<?=\yii\helpers\Url::to(['/users/users/profile'])?>">
                        <i class="material-icons">account_box</i>
                        <span>Профиль</span>
                    </a>
                </li>
                <li>
                    <a href="/logout">
                        <i class="material-icons">lock_open</i>
                        <span>Выход</span>
                    </a>
                </li>
            </ul>
        </div>
        <!-- #Menu -->
        <!-- Footer -->
        <div class="legal">
            <div class="copyright">
                &copy; 2016 - <?=date('Y', time())?> <a href="javascript:void(0);">Поколение Лидеров</a>.
            </div>
        </div>
        <!-- #Footer -->
    </aside>
    <!-- #END# Left Sidebar -->
    <!-- Right Sidebar -->
</section>


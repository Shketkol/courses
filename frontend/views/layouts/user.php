<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use frontend\assets\HeadAsset;
use frontend\assets\MainAsset;

HeadAsset::register($this);
MainAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Referal</title>
    <?= Html::csrfMetaTags() ?>
    <?php $this->head() ?>
</head>
<body
        <?if(Yii::$app->controller->action->id == 'login'){?>
            class="login-page"
        <? } else if(Yii::$app->controller->action->id == 'signup'){?>
            class="signup-page"
        <? }else if(Yii::$app->controller->action->id == 'register-success'){?>
            class="signup-page"
        <? }?>
>
<?php $this->beginBody() ?>

<?= $content ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

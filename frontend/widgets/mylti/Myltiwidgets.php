<?php

namespace frontend\widgets\mylti;

use yii\bootstrap\Widget;

class Myltiwidgets extends Widget{

    public $model;
    public $attribute;
    public $fields;
    public $form;

    public $name;


    public function init()
    {
        parent::init();


    }

    public function run(){

        $model = $this->model;
        $model_temp = explode('\\', $model::className());
        $model_class = $model_temp[count($model_temp)-1];

        return $this->render("myltiwidgets/index", array(
            'form' => $this->form,
            'name' => $this->name,
            'attribute' => $this->attribute,
            'model' => $this->model,
            'fields' => $this->fields,
            'model_class' => $model_class
        ));
    }



}
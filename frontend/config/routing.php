<?php
return [
    '/' => 'site/index',

    //auth begin
    'login' => 'auth/users/login',
    'login/error' => 'auth/users/login-error',
    'logout' => 'auth/users/logout',

    'forgot-password' => 'auth/users/forgot',
    'forgot-password/success' => 'auth/users/forgot-success',
    'password-recovery' => 'auth/users/password-recovery',

    'signup' => 'auth/users/signup',
    'signup/success' => 'auth/users/register-success',
    'email' => 'auth/users/email',

    'invite' => 'auth/users/invite',
    //auth end

    //users begin
    '/profile' => 'users/users/profile',
    '/users' => 'users/users/index',
    '/users/create' => 'users/users/create',
    '/users/update' => 'users/users/update',
    '/users/delete' => 'users/users/delete',
    //users end

    'courses' => 'courses/courses/index',
    'courses/<id:[\d]+>' => 'courses/courses/view',

    'lessons/<id:[\d]+>' => 'lessons/lessons/view',

    'progress' => 'progress/progress/index',

    'rating' => 'rating/rating/index'
];

<?php
return [
    //custom
    'upload' => [
        'class' => 'frontend\modules\upload\Module',
    ],
    'cron' => [
        'class' => 'frontend\modules\cron\Module',
    ],

    //users
    'auth' => [
        'class' => 'frontend\modules\auth\Module',
    ],
    'users' => [
        'class' => 'frontend\modules\users\Module',
    ],
    'payments' => [
        'class' => 'frontend\modules\payments\Module',
    ],
    'courses' => [
        'class' => 'frontend\modules\courses\Module',
    ],
    'lessons' => [
        'class' => 'frontend\modules\lessons\Module',
    ],
    'tasks' => [
        'class' => 'frontend\modules\tasks\Module',
    ],
    'progress' => [
        'class' => 'frontend\modules\progress\Module',
    ],
    'rating' => [
        'class' => 'frontend\modules\rating\Module',
    ],
    'intent' => [
        'class' => 'frontend\modules\intent\Module',
    ],
];
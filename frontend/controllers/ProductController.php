<?php
namespace frontend\controllers;

use common\models\Invoices;
use common\models\Products;
use common\models\Users;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Site controller
 */
class ProductController extends Controller
{

    public function actionIndex($user, $product)
    {
        $result = $this->loadModel($user, $product);
        $user = $result['user'];
        $product =$result['product'];

        $invoice = new Invoices();
        $invoice->users_id = $user->id;
        $invoice->products_id = $product->id;
        $invoice->price = $product->price;
        $invoice->save();

        return $this->render('index', [
            'id' => $invoice->id
        ]);
    }

    protected function loadModel($user, $product){
        $user = Users::findOne(['password_reset_token' => $user]);
        $product = Products::findOne(['token' => $product]);
        if(empty($user) || empty($product)){
            throw new NotFoundHttpException('The requested page does not exist.');
        }else {
            return array('user' => $user, 'product' => $product);
        }
    }
}

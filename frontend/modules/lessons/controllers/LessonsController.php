<?php

namespace frontend\modules\lessons\controllers;

use common\models\Lessons;
use common\models\Tasks;
use common\models\UsersLessons;
use \frontend\components\controllers\DefaultController;
use Yii;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;


class LessonsController extends DefaultController
{
//    public $layout = 'lessons';

    public function actionView($id)
    {
        $model = $this->loadModel($id);

        $lesson_user = UsersLessons::findOne(['users_id' => Yii::$app->user->id, 'lessons_id' => $id]);
        if (!empty($lesson_user)) {
            $time_model = strtotime($lesson_user->created_at);
            $sec = $model->duration * 3600;
            $time = date('U', time()) - $time_model;
            $left = $sec - $time;
            if ($left <= (float)0) {
                $model->duration = 0.00;
            } else {
                $model->duration = $left / 3600;
            }

        } else {
            $lesson_user = new UsersLessons();
            $lesson_user->users_id = Yii::$app->user->id;
            $lesson_user->lessons_id = $model->id;
            $lesson_user->courses_id = $model->courses_id;
            $lesson_user->status = 1;
            $lesson_user->read = 0;
            $lesson_user->save(false);
        }

        $task = Tasks::find()
            ->where(['lessons_id' => $model->id])
            ->orderBy('position')
            ->one();

        $tasks = Tasks::find()
            ->where(['lessons_id' => $model->id])
            ->orderBy('position')
            ->all();

        return $this->render('view', [
            'model' => $model,
            'task' => $task,
            'tasks' => $tasks,
            'lesson_user' => $lesson_user
        ]);
    }

    public function actionSend()
    {
        if(Yii::$app->request->isAjax){
            $id =$_GET['id'];

            $model = UsersLessons::findOne(['id' => $id]);
            $model->status = 2;
            $model->save(false);

            $count_lesson = Lessons::find()
                ->where(['courses_id' => $model->courses_id])
                ->count();
            if(Lessons::getCountFinish($model->users_id, $model->courses_id) === $count_lesson){
                return $this->redirect(Url::to(['/courses/courses/index']));
            } else {
                return $this->redirect(Url::to(['/courses/courses/view', 'id' => $model->courses_id]));
            }
        }
    }

    public function loadModel($id)
    {
        $model = Lessons::findOne(['id' => $id]);
        if ($model === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        return $model;
    }
}

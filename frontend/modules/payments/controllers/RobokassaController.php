<?php

namespace frontend\modules\payments\controllers;

use common\components\lib\Robokassa;
use common\models\Courses;
use common\models\Groups;
use common\models\Invoices;
use common\models\Payments;
use common\models\UserGroups;
use common\models\UsersCourses;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use Yii;

/**
 * Default controller for the `payments` module
 */
class RobokassaController extends Controller
{
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent:: beforeAction($action);
    }

    public function actionIndex($id) {

        $course = Courses::findOne(['id' => $id]);

        $invoice = new Invoices();
        $invoice->users_id = Yii::$app->user->id;
        $invoice->courses_id = $course->id;
        $invoice->price = $course->price;
        $invoice->currencies_id = $course->currencies_id;
        $invoice->status = 1;

        if ($invoice->save(false)){
            $payment = new Payments();
            $payment->invoices_id = $invoice->id;
            $payment->status = 0;
            $payment->save(false);

            $kassa = new Robokassa();
            $kassa->inv_id = $payment->id;
            $kassa->out_summ = (float)$invoice->price;
            $kassa->inv_desc = 'Покупка курса';
            if($invoice->currency->code != 'RUB'){
                $kassa->OutSumCurrency = $invoice->currency->code;
            }

            return $this->redirect($kassa->getRedirectURL());
        };
    }

    public function actionSuccess($InvId){
        $model = Payments::findOne(['id' => $InvId]);

        $invoice = Invoices::findOne(['id' => $model->invoices_id]);
        if($invoice->status == 1){
            $invoice->status = 3;
            $invoice->save(false);

            $model->status = 1;
            $model->result = json_encode($_GET);
            $model->save(false);

            $user = new UsersCourses();
            $user->courses_id = $invoice->courses_id;
            $user->users_id = $invoice->users_id;
            $user->save(false);
        }
        return $this->redirect(Url::to(['/courses/courses/view', 'id' => $invoice->courses_id]));
    }

    public function actionFail($InvId){
        $model = Payments::findOne(['id' => $InvId]);

        $invoice = Invoices::findOne(['id' => $model->invoices_id]);
        if($invoice->status == 1){
            $invoice->status = 2;
            $invoice->save(false);

            $model->result = json_encode($_GET);
            $model->save(false);
        }
        return $this->redirect(Url::to(['/courses/courses/view', 'id' => $invoice->courses_id]));
    }

    public function actionResult(){

        $kassa = new Robokassa(false);
        /** назначение параметров */
        $kassa->out_summ = $_REQUEST["OutSum"];
        $kassa->inv_id = $_REQUEST["InvId"];
        $kassa->shp_item = $_REQUEST["Shp_item"];
        $crc = $_REQUEST["SignatureValue"];
        $result = http_build_query($_REQUEST);
        $paymentId = (int)$_REQUEST["InvId"];


        $f=@fopen("./uploads/order.log","a+") or die("error");
        fputs($f,$result."\n");
        $payment = Payments::findOne(['id' => $paymentId]);

        if($kassa->checkSuccess($crc)){
            $result2 = $kassa->getPaymentStatusXML($paymentId);
            $out = simplexml_load_string($result2);
            if($out->Result->Code == 0) {
                $payment->status = 1;
                if($out->State->Code == 100){
                    $invoice = Invoices::findOne(['id' => $payment->invoices_id]);
                    $invoice->status = 3;
                    $invoice->save();

                    $user = new UsersCourses();
                    $user->courses_id = $invoice->courses_id;
                    $user->users_id = $invoice->users_id;
                    $user->save(false);

                    if ($invoice->courses->type == Courses::TYPE_GROUP && $invoice->status == 3){
                        $group = Groups::findOne(['courses_id' => $invoice->courses_id]);
                        if (empty($group)){
                            $group = new Groups();
                            $group->name = $invoice->courses->name.' '.date('Y-m-d', time());
                            $group->courses_id = $invoice->courses_id;
                            $group->save();
                        }
                        $user = UserGroups::findOne(['groups_id' => $group->id, 'user_id' => $invoice->users_id]);
                        if (empty($user)){
                            $user = new UserGroups();
                            $user->user_id = $invoice->users_id;
                            $user->groups_id = $group->id;
                            $user->save();
                        }
                    }

                }
            }

        }else{
            $result2 = 'bad signature';
        }

        fclose($f);

        $payment->result = $result;
        $payment->save();



    }
}

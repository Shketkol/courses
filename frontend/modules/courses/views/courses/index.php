<?
$this->title = 'Курсы';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <? foreach ($models as $model) { ?>
        <div class="col-md-4">
            <? $image = $model->getImagesByName(\common\helpers\Images::TYPE_COURSES) ?>
            <div class="card" style="display: block; height: 390px">
                <div class="card-header bg-white text-xs-center">
                    <h4 class="header">
                        <a href="<?= \yii\helpers\Url::to(['/courses/courses/view', 'id' => $model->id]) ?>"><?= $model->title ?></a>
                    </h4>
                </div>
                <a
                    <? if (count($model->lessons) != \common\models\Lessons::getCountFinish(Yii::$app->user->id, $model->id)) { ?>
                        href="<?= \yii\helpers\Url::to(['/courses/courses/view', 'id' => $model->id]) ?>"
                    <? } else { ?>
                        onclick="javascript:void 0"
                    <? } ?>
                >
                    <img src="<?= (!empty($image)) ? $image[0]->getUrl('500x200') : null ?>" alt=""
                         class="img-responsive">
                </a>
                <div class="body">
                    <span class="js-lead">
<!--                        --><?//= $model->lead ?>
                    </span>
                    <br>
                    <div class="pull-bottom">
                        <span class="btn btn-success btn-block waves-effect">Цена <?= (int)$model->price ?> <?= $model->currency->name ?></span>
                        <? if (count($model->lessons) != \common\models\Lessons::getCountFinish(Yii::$app->user->id, $model->id)) { ?>
                            <span class="btn btn-primary btn-block waves-effect">Количество уроков <?= count($model->lessons) ?>
                                , пройдено уроков <?= \common\models\Lessons::getCountFinish(Yii::$app->user->id, $model->id) ?></span>
                        <? } else { ?>
                            <span class="btn btn-warning btn-block waves-effect">Пройден</span>
                        <? } ?>
                        <span class="btn btn-danger btn-block waves-effect"><?= \common\models\Courses::getUserPay(Yii::$app->user->id, $model->id) ?></span>

                    </div>
                </div>
            </div>
        </div>
    <? } ?>
</div>

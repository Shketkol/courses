<?php

namespace frontend\modules\intent\controllers;

use common\helpers\Images;
use common\models\Intent;
use common\models\Reportes;
use frontend\models\IntentSearch;
use frontend\models\ReportesSearch;
use frontend\models\UsersSearch;
use common\models\Customers;
use common\models\Users;
use common\models\UsersProfile;
use frontend\components\controllers\DefaultController;
use yii\filters\AccessControl;
use yii\helpers\Url;
use Yii;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\web\User;

/**
 * Default controller for the `users` module
 */
class ReportesController extends DefaultController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ]
        ];
    }

    public function actionCreate()
    {
        $model = new Reportes();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->save()) {
                return $this->redirect(Url::to(['/intent/intent/view', 'id' => $model->intent_id]));
            }
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->save()) {
            return $this->redirect(Url::to(['/intent/intent/view', 'id' => $model->intent_id]));
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function loadModel($id)
    {
        $model = Reportes::findOne(['id' => $id]);
        if ($model === null) {
            $answer = json_decode(file_get_contents('php://input'), true);
            if (!empty($answer)) {
                return $this->asJson(array('status' => 'success', 'data' => array(
                    'type' => 'popup',
                    'name' => 'sa-basic',
                    'title' => 'Запись не найдена'
                )));
                exit;
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
        return $model;
    }

}




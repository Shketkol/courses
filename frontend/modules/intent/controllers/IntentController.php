<?php

namespace frontend\modules\intent\controllers;

use common\helpers\Images;
use common\models\Intent;
use common\models\Reportes;
use frontend\models\IntentSearch;
use frontend\models\ReportesSearch;
use frontend\models\UsersSearch;
use common\models\Customers;
use common\models\Users;
use common\models\UsersProfile;
use frontend\components\controllers\DefaultController;
use yii\filters\AccessControl;
use yii\helpers\Url;
use Yii;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\web\User;

/**
 * Default controller for the `users` module
 */
class IntentController extends DefaultController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ]
        ];
    }

    public function actionIndex()
    {
        $searchModel = new IntentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        $searchModel = new ReportesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $id);

        return $this->render('view', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = new Intent();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->save()) {
                return $this->redirect(Url::to(['/intent/intent/index']));
            }
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->save()) {
                return $this->redirect(['index']);
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function loadModel($id)
    {
        $model = Intent::findOne(['id' => $id]);
        if ($model === null) {
            $answer = json_decode(file_get_contents('php://input'), true);
            if (!empty($answer)) {
                return $this->asJson(array('status' => 'success', 'data' => array(
                    'type' => 'popup',
                    'name' => 'sa-basic',
                    'title' => 'Запись не найдена'
                )));
                exit;
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
        return $model;
    }

}




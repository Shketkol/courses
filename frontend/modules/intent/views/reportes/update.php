<?php
use \yii\bootstrap\ActiveForm;
use \yii\helpers\Url;
use \yii\helpers\Html;
use \common\models\Users;

$this->title = 'Отчет';
$this->params['breadcrumbs'][] = (!empty($model->date))?$model->date :'Отчет';

$this->registerJsFile('@web/source/lib/ckeditorbigger/ckeditor.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/source/js/main.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <? $form = ActiveForm::begin([
                'id' => 'users-update',
                'enableClientValidation'=> false,
                'validateOnSubmit' => true,
//                'action' => Url::to(['update', 'id' => $model->id]),
                'options' => [
//                    'class' => 'form-horizontal',
                    'autocomplete' => 'off',
                ],
                'fieldConfig' => [
                    'template' => "{label}<div class=\"form-line\">{input}<span class=\"help-block hidden\"></span></div>",
                    'labelOptions' => ['class' => 'col-md-12'],
                ],
            ]); ?>
            <div class="body">
                <div class="row">
                    <div class="col-md-12">
                        <?=$form->errorSummary($model);?>
                        <?= $form->field($model, 'user_id')->hiddenInput(['value' => Yii::$app->user->id])->label(false) ?>
                        <?= $form->field($model, 'intent_id')->hiddenInput(['value' => $_GET['id']])->label(false) ?>
                        <?= $form->field($model, 'intent_id')->dropDownList(
                            \yii\helpers\ArrayHelper::map(\common\models\Intent::find()->where(['user_id' => Yii::$app->user->id])->all(), 'id', 'content'),
                            array('required' => true)
                        ) ?>
                        <?= $form->field($model, 'content')->textarea(['placeholder' => 'Описание', 'required' => true, 'class' => 'useCKEditor']) ?>

                        <button type="submit" class="btn btn-lg btn-success waves-effect waves-light m-t-10 m-r-10"><i class="fa fa-spin fa-circle-o-notch hidden"></i>&nbsp; Сохранить &nbsp;</button>
                    </div>
                </div>


            </div>


            <? ActiveForm::end(); ?>
        </div>

    </div>
</div>
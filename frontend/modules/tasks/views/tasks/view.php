<?

use shketkol\images\src\models\Image;

$user_task = \common\models\UsersAnswers::find()
    ->where(['tasks_id' => $model->id])
    ->andWhere(['users_id' => Yii::$app->user->id])
    ->one();

$lessons_finish = \common\models\UsersLessons::findOne([
    'users_id' => Yii::$app->user->id,
    'lessons_id' => $model->lessons_id
]);

if ($lessons_finish->status == 2) {
    if (empty($user_task)) {
        $user_task = 1;
    }
}

?>

<? if (empty($user_task)) { ?>
    <form class="questions" enctype="multipart/form-data" data-id="<?= $model->id ?>">
        <div class="card">
            <div class="header bg-white p-1">
                <div class="media">
                    <div class="media-left media-middle">
                        <h4 class="mb-0"><strong>#<?= $model->position ?></strong></h4>
                    </div>
                    <div class="media-body  media-middle">
                        <h4 class="header">
                            <?= $model->title ?>
                        </h4>
                        <div class="body">
                            <?= $model->desc ?>
                        </div>
                    </div>
                </div>
            </div>
            <? foreach ($model->questionsTasks as $question) { ?>
            <input type="hidden" name="task" value="<?= $model->id ?>">
            <div class="card-block p-2">
                <div class="body">
                    <?= $question->desc ?>
                </div>
                <div class="body">
                <? if ($question->format == 1) { ?>
                    <fieldset class="form-group">
                        <label for="exampleTextarea">Ответ</label>
                        <textarea name="questions[<?= $question->id ?>]" class="form-control" id="exampleTextarea"
                                  rows="3"></textarea>
                    </fieldset>
                <? } ?>
                <? if ($question->format == 2) { ?>
                    <? foreach ($question->variantsAnswer as $key=>$variant) { ?>
                        <div class="form-group">
                        <input type="checkbox" class="filled-in" id="check_<?=$key?>"
                               name="questions[<?= $question->id ?>][variants][]"
                               value="<?= $variant->id ?>">
                        <label class="" for="check_<?=$key?>">
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-description"><?= $variant->title ?></span>
                            </label>
                        </div>
                    <? } ?>
                <? } ?>
                <? if ($question->format == 3) { ?>
                    <fieldset class="form-group">
                        <input type="hidden" class="js_file_task" value="<?= $model->id ?>">
                        <input type="hidden" class="js_file_questlion" value="<?= $question->id ?>">
                        <label for="exampleInputFile">Загрузите файл</label>
                        <input type="file" class="form-control-file" name="questions[<?= $question->id ?>]"
                               id="exampleInputFile">
                    </fieldset>
                <? } ?>
                <? if ($question->format == 4) { ?>
                    <? foreach ($question->variantsAnswer as $key=>$variant) { ?>
                        <div class="form-group">
                            <input type="radio" class="custom-control-input" id="radio_<?=$key?>"
                                   name="questions[<?= $question->id ?>][variants][]"
                                   value="<?= $variant->id ?>">
                            <label class="custom-control custom-radio" for="radio_<?=$key?>">
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-description"><?= $variant->title ?></span>
                            </label>

                        </div>
                    <? } ?>
                <? } ?>

                <? if ($question->format == 5) { ?>
                <div class="form-group">
                    <label for="custom-select">Варианты ответов</label>
                    <select id="custom-select" class="form-control custom-select"
                            name="questions[<?= $question->id ?>][variants][]">
                        <? foreach ($question->variantsAnswer as $variant) { ?>
                            <option value="<?= $variant->id ?>"><?= $variant->title ?></option>
                        <? } ?>
                    </select>
                    <? } ?>
                </div>
                <? } ?>

                    <? if (\common\models\UsersAnswers::getKeep(Yii::$app->user->id, $model->id)):?>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-success float-xs-right">Отправить <i
                                        class="material-icons btn__icon--right">send</i></button>
                        </div>
                    <?else:?>
                        <p>У Вас предыдущие задания либо на проверке, либо неправильные. Продолжить учебу невозможно</p>
                    <?endif;?>
                </div>

            </div>
    </form>
<? } else { ?>
    <?$show = false;?>
<div class="card">
    <div class="card-header bg-white p-1">
        <div class="media">
            <div class="media-left media-middle">
                <h4 class="mb-0"><strong>#<?= $model->position ?></strong></h4>
            </div>
            <div class="media-body  media-middle">
                <h4 class="header">
                    <?= $model->title ?>
                </h4>
                <div class="body">
                    <?= $model->desc ?>
                </div>
            </div>
        </div>
    </div>
    <form class="questions" enctype="multipart/form-data" data-id="<?= $model->id ?>">
        <input type="hidden" name="task" value="<?= $model->id ?>">
    <? foreach ($model->questionsTasks as $question) { ?>
    <div class="body">
        <br class="card-block p-2">
            <div class="body">
                <?= $question->desc ?>
            </div>
            <? if ($question->format == 1) { ?>
                <? if ($question->test == 2) { ?>
                    <? $answer = \common\models\QuestionsTask::getAnswer(Yii::$app->user->id, $question->id) ?>
                    <?if (!empty($answer) && $answer->correct === null):?>
                        <div class="card card-success text-xs-center">
                            <div class="card-block">
                                <p>Задание на проверке</p>
                            </div>
                        </div>
                    <?endif;?>
                <? } ?>
                <fieldset class="form-group">
                    <label for="exampleTextarea">Ответ</label>
                    <? $answer = \common\models\QuestionsTask::getAnswer(Yii::$app->user->id, $question->id) ?>
                    <textarea class="form-control" id="exampleTextarea" name="questions[<?= $question->id ?>]"
                              rows="3"><?= (!empty($answer)) ? trim($answer->answer) : null ?></textarea>
                    <?if (!empty($answer) && $answer->correct === 0):?>
                        <? $show = true;?>
                        <p>(неправильно)</p>
                    <?elseif (!empty($answer) && $answer->correct === 1):?>
                        <p>(правильно)</p>
                    <?endif;?>

                </fieldset>
            <? } ?>
            <? if ($question->format == 2) { ?>

                <? foreach ($question->variantsAnswer as $key=>$variant) { ?>
                    <? $answer = \common\models\QuestionsTask::getAnswer(Yii::$app->user->id, $question->id, $variant->id); ?>
                    <? if ($key == 0 && $question->test == 2 && !empty($answer) && $answer->correct == null) { ?>
                        <p>Задание на проверке</p>
                    <? } ?>
                    <div class="form-group">
                    <input type="checkbox" class="custom-control-input" id="check_<?=$key?>"

                        <? if (!empty($answer) && $answer->answer == $variant->id) { ?>
                            checked
                        <? } ?>
                           name="questions[<?= $question->id ?>][variants][]"
                           value="<?= $variant->id ?>">
                    <label class="custom-control custom-checkbox" for="check_<?=$key?>">
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description"><?= $variant->title ?>
                                <? if (!empty($answer) && $answer->answer == $variant->id && $answer->correct === null) { ?>
                                    (на проверке)
                                <? } else if (!empty($answer) && $answer->answer == $variant->id && $answer->correct === 1) { ?>
                                    (правильно)
                                <? } else if (!empty($answer) && $answer->answer == $variant->id && $answer->correct === 0) { ?>
                                    <? $show = true;?>
                                    (неправильно)
                                <? } ?>
                                </span>
                        </label>

                    </div>
                <? } ?>
            <? } ?>
            <? if ($question->format == 3) { ?>

                <?
                $answer = \common\models\UsersAnswers::findOne(['users_id' => Yii::$app->user->id, 'questions_task_id' => $question->id]);
                $file = null;
                if (!empty($answer)) {
                    $file = Image::findOne(['modelName' => 'Tasks', 'itemId' => $answer->id]);
                    if (!empty($file)) {
                        $file = $file->filePath;
                    }
                }
                ?>
                <? if ($question->test == 2 && !empty($answer) && $answer->result != 1) { ?>
                    <div class="card card-success text-xs-center">
                        <div class="card-block">
                            <p>Задание на проверке</p>
                        </div>
                    </div>
                <? } ?>
                <? if (!is_null($file)) { ?>
                    <a href="<?= (!empty($file)) ? '/uploads/store/' . $file : null ?>" download>Файл</a>
                <? } else { ?>
                    Файл не загружен
                <? } ?>

                <? if (!empty($answer) && $answer-> correct === 1) { ?>
                    Ответ верный
                <? }?>

                <? if (!empty($answer) && $answer-> correct === 0) { ?>
                    Ответ не верный
                <? }?>

                <? if (!empty($answer) && $answer-> correct === 0) { ?>
                    <? $show = true; ?>
                    <fieldset class="form-group">
                        <input type="hidden" class="js_file_task" value="<?= $model->id ?>">
                        <input type="hidden" class="js_file_questlion" value="<?= $question->id ?>">
                        <label for="exampleInputFile">Загрузите файл</label>
                        <input type="file" class="form-control-file" name="questions[<?= $question->id ?>]"
                               id="exampleInputFile">
                    </fieldset>
                <? } ?>

            <? } ?>
            <? if ($question->format == 4) { ?>

                <? foreach ($question->variantsAnswer as $key=>$variant) { ?>
                    <? $answer = \common\models\QuestionsTask::getAnswer(Yii::$app->user->id, $question->id, $variant->id); ?>
                    <? if ($key == 0 && $question->test == 2 && (!empty($answer)) && $answer->correct == null) { ?>
                        <div class="card card-success text-xs-center">
                            <div class="card-block">
                                <p>Задание на проверке</p>
                            </div>
                        </div>
                    <? } ?>
                    <div class="form-control">
                    <input type="radio" class="custom-control-input" id="radio_<?=$key?>"

                        <? if (!empty($answer) && $answer->answer == $variant->id) { ?>
                            checked
                        <? } ?>
                           name="questions[<?= $question->id ?>][variants][]"
                           value="<?= $variant->id ?>">
                        <label class="custom-control custom-radio" for="radio_<?=$key?>">
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description"><?= $variant->title ?>
                                <? if (!empty($answer) && $answer->answer == $variant->id && $answer->correct === null) { ?>
                                    (на проверке)
                                <? } else if (!empty($answer) && $answer->answer == $variant->id && $answer->correct === 1) { ?>
                                    (правильно)
                                <? } else if (!empty($answer) && $answer->answer == $variant->id && $answer->correct === 0) { ?>
                                    <? $show = true;?>
                                    (неправильно)
                                <? } ?>
                                </span>
                        </label>

                    </div>
                <? } ?>
            <? } ?>
            <? if ($question->format == 5) { ?>
<!--            --><?// if ($question->test == 2) { ?>
<!--                <div class="card card-success text-xs-center">-->
<!--                    <div class="card-block">-->
<!--                        <p>Задание на проверке</p>-->
<!--                    </div>-->
<!--                </div>-->
<!--            --><?// } ?>
            <div class="form-group">
                <label for="custom-select">Варианты ответа</label>
                <select id="custom-select" class="form-control custom-select form-line"
                        name="questions[<?= $question->id ?>][variants][]">
                    <? foreach ($question->variantsAnswer as $variant) { ?>
                        <? $answer = \common\models\QuestionsTask::getAnswer(Yii::$app->user->id, $question->id, $variant->id); ?>

                        <option value="<?= $variant->id ?>"
                            <? if (!empty($answer) && $answer->answer == $variant->id) { ?>
                                selected
                            <? } ?>><?= $variant->title ?></option>


                        <? if ($question->test == 1) {
                            $test = false;
                            if (!empty($answer) && $answer->answer == $variant->id && $answer->correct === 1) {
                                $test = true;
                            } else if (!empty($answer) && $answer->answer == $variant->id && $answer->correct === 0) {
                                if (!$test) {
                                    $answer = false;
                                }
                            } ?>
                        <? } ?>

                    <? } ?>
                </select>

                <? if ($question->test == 1) { ?>
                    <? if ($test) { ?>
                        <p class="form-check-label" style="color: green">
                            Правильно
                        </p>
                    <? } else { ?>
                        <? $show = true;?>
                        <p class="form-check-label" style="color: red">
                            Неправильно
                        </p>
                    <? } ?>

                <? } ?>
                <? } ?>
                </div>
                <?if ($show):?>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-success float-xs-right">Отправить <i
                                        class="material-icons btn__icon--right">send</i></button>
                        </div>
                <?endif;?>
    </div>

        <? } ?>
    </form>
    <? } ?>





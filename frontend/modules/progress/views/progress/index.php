<?
$this->title = 'Успеваемость';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="card-columns">
    <div class="card">
        <div class="card-header bg-white">
            <h4 class="card-title">Пройденные уроки</h4>
        </div>
        <table class="table table-middle">
            <thead>
            <tr>
                <th>Курс</th>
                <th>Урок</th>
                <th>Задание</th>
                <th>Балы</th>
            </tr>
            </thead>
            <tbody>
            <?foreach ($models as $model){?>
                <tr>
                    <td><?=$model->tasks->lessons->courses->title?></td>
                    <td><?=$model->tasks->lessons->title?></td>
                    <td><?=$model->tasks->title?></td>
                    <td><?=$model->questionsTask->rating?></td>
                </tr>
            <? }?>
            </tbody>
        </table>
    </div>
</div>

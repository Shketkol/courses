<?php

namespace frontend\modules\progress\controllers;

use common\models\Courses;
use common\models\Invoices;
use common\models\Lessons;
use common\models\UsersAnswers;
use common\models\UsersLessons;
use \frontend\components\controllers\DefaultController;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * Default controller for the `referal` module
 */
class ProgressController extends DefaultController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $id_arr = [];
        $models = UsersAnswers::find()
            ->select('id, tasks_id')
            ->where(['users_id' => Yii::$app->user->id])
            ->andWhere(['correct' => 1])
            ->asArray()
            ->all();

        $tasks = [];
        foreach ($models as $model){
            if (array_search($model['tasks_id'], $tasks) === false){
                $tasks[] = $model['tasks_id'];
                $id_arr[] = $model['id'];
            }
        }

        $models = UsersAnswers::find()
            ->where(['IN', 'id', $id_arr])
            ->all();

        return $this->render('index', [
            'models' => $models,
        ]);
    }

}

<?php

namespace frontend\modules\rating\controllers;

use common\models\Courses;
use common\models\Invoices;
use common\models\Lessons;
use common\models\Users;
use common\models\UsersLessons;
use \frontend\components\controllers\DefaultController;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * Default controller for the `referal` module
 */
class RatingController extends DefaultController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex($id=null)
    {
        $courses = Invoices::find()
            ->where(['users_id' => Yii::$app->user->id])
            ->andWhere(['status' => 3])
            ->all();

        if (is_null($id)) {
            if(!empty($courses)){
                $id = $courses[0]->courses_id;
            }
        }

        $users = Users::find()
            ->join('LEFT JOIN', 'invoices', 'users.id=invoices.users_id')
            ->where(['invoices.status' => 3, 'invoices.courses_id' => $id])
            ->orderby('bals DESC')
            ->all();

        return $this->render('index', [
            'courses' => $courses,
            'users' => $users,
            'id' => $id
        ]);
    }

}

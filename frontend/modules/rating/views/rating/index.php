<?
$this->title = 'Рейтинг';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <?php \yii\widgets\Pjax::begin(); ?>
    <div class="col-lg-4">
        <div class="card">
            <div class="header bg-white">
                <div class="media">
                    <div class="media-body">
                        <h4 class="card-title">Курсы</h4>
                    </div>
                </div>
            </div>
            <ul class="list-group list-group-fit mb-0">
                <?foreach ($courses AS $cours){ ?>
                    <li class="list-group-item">
                        <div class="media">
                            <div class="media-body media-middle">
                                <a href="<?=\yii\helpers\Url::to(['/rating/rating/index', 'id' => $cours->courses->id])?>"><?=$cours->courses->title?></a>
                            </div>
                        </div>
                    </li>
                <? } ?>
            </ul>
        </div>
    </div>
    <div class="col-lg-8">
        <div class="card">
            <div class="header bg-white">
                <h4 class="card-title">Рейтинг учеников</h4>
            </div>
            <table class="table table-middle">
                <thead>
                <tr>
                    <th>Имя</th>
                    <th>Балы</th>
                </tr>
                </thead>
                <tbody>
                <?foreach ($users as $user){?>
                    <tr>
                        <td><?=$user->name?></td>
                        <td><?=\common\models\BallsHistory::getCountBalls($user->id, $id)?></td>
                    </tr>
                <? }?>
                </tbody>
            </table>
        </div>
    </div>
    <?php \yii\widgets\Pjax::end(); ?>
</div>
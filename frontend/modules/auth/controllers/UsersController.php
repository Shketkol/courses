<?php

namespace frontend\modules\auth\controllers;

use common\models\LoginForm;
use common\models\Users;
use common\models\UsersProfile;
use frontend\components\controllers\DefaultController;
use frontend\models\PasswordResetRequestForm;
use yii\base\InvalidParamException;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\helpers\Url;
use Yii;

/**
 * Default controller for the `users` module
 */
class UsersController extends DefaultController
{
    public function beforeAction($action)
    {
        $this->layout = '@app/views/layouts/user.php';
        return parent:: beforeAction($action);
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
//                        'actions' => ['login', 'error', ''],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionSignup()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new Users();

        if ($model->load(Yii::$app->request->post())) {
            $model->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
            $model->status = Users::STATUS_REQUESTED;
            $model->users_roles_id = Users::TYPE_SCHOLAR;
            $model->email = trim($model->email);
            $model->name = trim($model->name);
            $model->surname = trim($model->surname);
            $model->country = trim($model->country);
            if ($model->validate() && $model->password_hash === $model->confirm_pass) {
                $model->password_hash = Yii::$app->security->generatePasswordHash($model->password_hash);
                $model->auth_key = Yii::$app->security->generateRandomString();
                if ($model->save()) {
                    $user = new UsersProfile();
                    $user->users_id = $model->id;
                    $user->surname = $model->surname;
                    $user->cities_id = $model->cities_id;
                    $user->phone = $model->phone;
                    if ($user->save()) {

                        $model->sendEmail();

//                        Yii::$app->user->login($model, 3600 * 24 * 30);
                        return $this->redirect(Url::to('/auth/users/register-success'));
                    }
                }
            }
        }
        return $this->render('signup', array(
            'model' => $model
        ));
    }

    public function actionRegisterSuccess(){
        if(!Yii::$app->user->isGuest){
            return $this->goHome();
        }
        return $this->render('register_success');
    }


    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                if($model->getStatus() == Users::STATUS_REQUESTED) {
                    return $this->redirect(Url::to(['login-error']));
                }
                $model->login();
                return $this->redirect(Url::to([Yii::$app->getHomeUrl()]));
            }
        }

        return $this->render('login', array(
            'model' => $model
        ));
    }

    public function actionLoginError(){
        return $this->render('login_error');
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->redirect(['login']);
    }

    public function actionForgot()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                $model->sendEmail();
                return $this->redirect(Url::to(['forgot-success']));
            }
        }
        return $this->render('forgot', array(
            'model' => $model
        ));
    }

    public function actionForgotSuccess()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        return $this->render('forgot_success');
    }

    public function actionEmail($token)
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        if (empty($token) || !is_string($token)) {
            throw new InvalidParamException('Token cannot be blank.');
        }
        $user = Users::findByPasswordResetToken($token);
        if (!$user) {
            throw new InvalidParamException('Wrong Token.');
        } else {
            if ($user->active = 0) {
                $user->active = 1;
                $user->save(false);
            }
        }

        return $this->render('success');
    }


    public function actionPasswordRecovery($token)
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        if (empty($token) || !is_string($token)) {
            throw new NotFoundHttpException('Token cannot be blank.');
        }
        $model = Users::findByPasswordResetToken($token);
        if (!$model) {
            throw new NotFoundHttpException('Wrong Token.');
        }

        if($model->load(Yii::$app->request->post()) && $model->validate()){
            $model->password_hash = Yii::$app->security->generatePasswordHash($model->password_hash);
            $model->save();
            if (Yii::$app->user->login($model, 3600 * 24 * 30)) {
                return $this->redirect([Url::to([Yii::$app->getHomeUrl()])]);
            }
        }

        return $this->render('password_recovery', array(
            'token' => $token,
            'model' => $model
        ));
    }

}

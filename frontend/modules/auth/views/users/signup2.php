<?php
use \yii\bootstrap\ActiveForm;
?>

<div class="row">
    <div class="col-sm-8 push-sm-1 col-md-4 push-md-3 col-lg-4 push-lg-4">
        <div class="text-xs-center m-2">
            <div class="icon-block rounded-circle">
                <i class="material-icons md-36 text-muted">edit</i>
            </div>
        </div>
        <div class="card">
            <div class="card-header bg-white text-xs-center">
                <h4 class="card-title">Регистрация в кабинете</h4>
                <p class="card-subtitle">Создание нового аккаунта</p>
            </div>
            <div class="p-2">
                <? $form = ActiveForm::begin([
                    'id' => 'loginform',
                    'enableClientValidation'=> false,
                    'validateOnSubmit' => true,
                    'action' => \yii\helpers\Url::to(['/auth/users/signup']),
                    'options' => [
                        'autocomplete' => 'off',
                    ]
                ]); ?>
                <?=$form->errorSummary($model);?>
                <?= $form->field($model, 'name')->textInput(['placeholder' => 'Имя', 'required' => true])->label(false) ?>
                <?= $form->field($model, 'surname')->textInput(['placeholder' => 'Фамилия', 'required' => true])->label(false) ?>
                <?= $form->field($model, 'phone')->textInput(['placeholder' => 'Телефон', 'required' => true])->label(false) ?>
                <?= $form->field($model, 'cities_id')->dropDownList(
                    \yii\helpers\ArrayHelper::map(\common\models\Cities::find()->orderBy('name')->all(), 'id', 'name'),
                    array('prompt' => 'Выберите значение')
                ) ?>
                <?= $form->field($model, 'email')->textInput(['type' => 'email', 'placeholder' => 'E-mail', 'required' => true])->label(false) ?>
                <?= $form->field($model, 'password_hash')->textInput(['type' => 'password', 'placeholder' => 'Пароль', 'required' => true])->label(false) ?>
                <?= $form->field($model, 'confirm_pass')->textInput(['type' => 'password', 'placeholder' => 'Подтвердите пароль', 'required' => true])->label(false) ?>
                    <p class="text-xs-center">
                        <button type="submit" class="btn btn-primary btn-block">
                            <span class="btn-block-text">Регистрация</span>
                        </button>
                    </p>
                    <div class="text-xs-center">Уже зарегистрированы? <a href="<?=\yii\helpers\Url::to(['/auth/users/login'])?>">Войти в аккаунт</a></div>
                <? ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>

<script>
    var password = document.getElementById("users-password_hash")
        , confirm_password = document.getElementById("users-confirm_pass");

    function validatePassword() {
        if (password.value != confirm_password.value) {
            confirm_password.setCustomValidity("Пароли не совпадают");
        } else {
            confirm_password.setCustomValidity('');
        }
    }

    password.onchange = validatePassword;
    confirm_password.onkeyup = validatePassword;
</script>

<style>
    .new-login-register .new-login-box {
        margin-top: 5%;
    }
</style>
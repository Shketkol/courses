<?php
use \yii\bootstrap\ActiveForm;
?>
<div class="signup-page">
    <div class="signup-box">
        <div class="logo">
            <a href="javascript:void(0);" class="text-center"><img style="margin: 0 auto" src="/static/images/logo.png" alt="" width="250px" class="img-responsive"></a>
        </div>
        <div class="card">
            <div class="body">
                <? $form = ActiveForm::begin([
                    'id' => 'sign_up',
                    'enableClientValidation'=> false,
                    'validateOnSubmit' => true,
                    'action' => \yii\helpers\Url::to(['/auth/users/signup']),
                    'options' => [
                        'autocomplete' => 'off',
                    ]
                ]); ?>
                <?=$form->errorSummary($model);?>
                <div class="msg">Регистрация нового аккаунта</div>
                <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                    <div class="form-line">
                        <?= $form->field($model, 'name')->textInput(['placeholder' => 'Имя', 'required' => true])->label(false) ?>
                    </div>
                </div>
                <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">person</i>
                            </span>
                    <div class="form-line">
                        <?= $form->field($model, 'surname')->textInput(['placeholder' => 'Фамилия', 'required' => true])->label(false) ?>
                    </div>
                </div>
                <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">phone</i>
                        </span>
                    <div class="form-line">
                        <?= $form->field($model, 'phone')->textInput(['placeholder' => 'Телефон', 'required' => true])->label(false) ?>
                    </div>
                </div>
                <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">person</i>
                                </span>
                    <div class="form-line">
                        <?= $form->field($model, 'cities_id')->dropDownList(
                            \yii\helpers\ArrayHelper::map(\common\models\Cities::find()->where(['status' => 10])->orderBy('name')->all(), 'id', 'name'),
                            array('prompt' => 'Выберите значение', 'required' => true)
                        ) ?>
                    </div>
                </div>
                <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">email</i>
                        </span>
                    <div class="form-line">
                        <?= $form->field($model, 'email')->textInput(['type' => 'email', 'placeholder' => 'Email', 'required' => true])->label(false) ?>
                    </div>
                </div>
                <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                    <div class="form-line">
                        <?= $form->field($model, 'password_hash')->textInput(['type' => 'password', 'placeholder' => 'Пароль', 'required' => true])->label(false) ?>
                    </div>
                </div>
                <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                    <div class="form-line">
                        <?= $form->field($model, 'confirm_pass')->textInput(['type' => 'password', 'placeholder' => 'Подтвердите пароль', 'required' => true])->label(false) ?>
                    </div>
                </div>


                <button class="btn btn-block btn-lg bg-pink waves-effect" type="submit">РЕГИСТРАЦИЯ</button>

                <div class="m-t-25 m-b--5 align-center">
                    <a href="<?=\yii\helpers\Url::to(['/auth/users/login'])?>">Уже зарегистрированы?</a>
                </div>
                <? ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>

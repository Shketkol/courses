<?php
use \yii\bootstrap\ActiveForm;
?>
<div class="login-box">
    <div class="logo">
        <a href="javascript:void(0);" class="text-center"><img style="margin: 0 auto" src="/static/images/logo.png" alt="" width="250px" class="img-responsive"></a>
    </div>
    <div class="card">
        <div class="body">
            <form id="sign_in" method="POST">
                <? $form = ActiveForm::begin([
                    'id' => 'sign_in',
                    'enableClientValidation'=> false,
                    'validateOnSubmit' => true,
                    'action' => \yii\helpers\Url::to(['/auth/users/login']),
                    'options' => [
                        'autocomplete' => 'off',
                    ]
                ]); ?>
                <?=$form->errorSummary($model, ['header'=>'<p>Ошибки при авторизации:</p>']);?>
<!--                <div class="msg">Sign in to start your session</div>-->
                <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">email</i>
                        </span>
                    <div class="form-line">
                        <?= $form->field($model, 'email')->textInput(['type' => 'email', 'placeholder' => 'E-mail', 'required' => true])->label(false) ?>
                    </div>
                </div>
                <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                    <div class="form-line">
                        <?= $form->field($model, 'password')->textInput(['type' => 'password', 'placeholder' => 'Пароль', 'required' => true])->label(false) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-8 p-t-5">
                        <input type="checkbox" name="rememberme" id="rememberme" class="filled-in chk-col-pink">
                        <label for="rememberme">Запомнить меня</label>
                    </div>
                    <div class="col-xs-4">
                        <button class="btn btn-block bg-pink waves-effect" type="submit">ВОЙТИ</button>
                    </div>
                </div>
                <div class="row m-t-15 m-b--20">
                    <div class="col-xs-6">
                        <a href="<?=\yii\helpers\Url::to(['/auth/users/signup'])?>">Зарегистрируйтесь</a>
                    </div>
<!--                    <div class="col-xs-6 align-right">-->
<!--                        <a href="--><?//=\yii\helpers\Url::to(['/auth/users/forgot'])?><!--">Забыли пароль?</a>-->
<!--                    </div>-->
                </div>
                <? ActiveForm::end(); ?>
        </div>
    </div>
</div>
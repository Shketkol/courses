<?php
use \yii\bootstrap\ActiveForm;
?>

<div class="row">
    <div class="col-sm-8 push-sm-1 col-md-4 push-md-4 col-lg-4 push-lg-4">
        <div class="text-xs-center m-2">
            <div class="icon-block rounded-circle">
                <i class="material-icons md-36 text-muted">lock</i>
            </div>
        </div>
        <div class="card bg-transparent">
            <div class="card-header bg-white text-xs-center">
                <h4 class="card-title">Войти в кабинет</h4>
                <p class="card-subtitle">Доступ в кабинет</p>
            </div>
            <div class="p-2">
                <? $form = ActiveForm::begin([
                    'id' => 'loginform',
                    'enableClientValidation'=> false,
                    'validateOnSubmit' => true,
                    'action' => \yii\helpers\Url::to(['/auth/users/login']),
                    'options' => [
                            'autocomplete' => 'off',
                    ]
                ]); ?>
                <?=$form->errorSummary($model, ['header'=>'<p>Ошибки при авторизации:</p>']);?>
                <?= $form->field($model, 'email')->textInput(['type' => 'email', 'placeholder' => 'E-mail', 'required' => true])->label(false) ?>
                <?= $form->field($model, 'password')->textInput(['type' => 'password', 'placeholder' => 'Пароль', 'required' => true])->label(false) ?>
                    <div class="form-group ">
                        <button type="submit" class="btn  btn-primary btn-block">
                            <span class="btn-block-text">Вход</span>
                        </button>
                    </div>
                    <div class="text-xs-center">
                        <a href="<?=\yii\helpers\Url::to(['/auth/users/forgot'])?>"><small>Забыли пароль?</small></a>
                    </div>
                    <? ActiveForm::end(); ?>
            </div>
            <div class="card-footer text-xs-center bg-white">
                Еще не студент? <a href="<?=\yii\helpers\Url::to(['/auth/users/signup'])?>">Зарегистрируйтесь</a>
            </div>
        </div>
    </div>
</div>


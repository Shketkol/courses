<?php
use \yii\bootstrap\ActiveForm;
use \yii\helpers\Url;
?>
<div class="row">
    <div class="col-sm-8 push-sm-1 col-md-4 push-md-4 col-lg-4 push-lg-4">
        <div class="text-xs-center m-2">
            <div class="icon-block rounded-circle">
                <i class="material-icons md-36 text-muted">lock</i>
            </div>
        </div>
        <div class="card bg-transparent">
            <div class="card-header bg-white text-xs-center">
                <h4 class="card-title">Востановление пароля</h4>
            </div>
            <div class="p-2">
                <? $form = ActiveForm::begin([
                    'id' => 'loginform',
                    'enableClientValidation'=> false,
                    'validateOnSubmit' => true,
                    'action' => Url::to(['forgot']),
                    'options' => [
                        'class' => 'form-horizontal form-material new-lg-form',
                        'autocomplete' => 'off',
                    ],
                ]); ?>
                <?=$form->errorSummary($model);?>
                <?= $form->field($model, 'email')->textInput(['type' => 'email', 'placeholder' => 'E-mail', 'required' => true])->label(false) ?>
                <div class="form-group ">
                    <button type="submit" class="btn  btn-primary btn-block">
                        <span class="btn-block-text">Востановить</span>
                    </button>
                </div>
                <div class="text-xs-center">
                    <a href="<?=\yii\helpers\Url::to(['/auth/users/login'])?>"><small>На станицу входа</small></a>
                </div>
                <? ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>

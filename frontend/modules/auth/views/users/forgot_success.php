<div class="row">
    <div class="col-sm-8 push-sm-1 col-md-4 push-md-3 col-lg-4 push-lg-4">
        <div class="card">
            <div class="card-header bg-white text-xs-center">
                <h4 class="card-title"Восстановление пароля</h4>
                <p class="card-subtitle">
                    Инструкции по восстановлению пароля отправлены на указанный адрес электронной почты.
                </p>
            </div>
            <div class="p-2">
                <div class="text-xs-center"><a href="<?=\yii\helpers\Url::to(['/auth/users/login'])?>">На страницу входа</a></div>
            </div>
        </div>
    </div>
</div>
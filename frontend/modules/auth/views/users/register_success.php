<?php
use \yii\bootstrap\ActiveForm;

?>
<div class="signup-page">
    <div class="signup-box">
        <div class="logo">
            <a href="javascript:void(0);" class="text-center"><img style="margin: 0 auto" src="/static/images/logo.png" alt="" width="250px" class="img-responsive"></a>
        </div>
        <div class="card">
            <div class="body">
                <div class="row">
                    <div class="header text-xs-center">
                        <h4 class="card-title">Спасибо за регистрацию</h4>
                        <p class="card-subtitle">
                            Аккаунт будет активирован в ближайшее время.
                            <br>Администратор должен подтвердить вашу регистрацию.
                        </p>
                    </div>
                    <div class="p-2">
                        <div class="text-center"><a href="<?= \yii\helpers\Url::to(['/auth/users/login']) ?>">На
                                страницу входа</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php

namespace frontend\modules\upload\controllers;

use yii\helpers\Json;
use yii\web\Controller;
use Yii;
use yii\web\UploadedFile;


/**
 * Default controller for the `upload` module
 */
class UploadController extends Controller
{

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;

        return parent:: beforeAction($action);
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionUpload()
    {
        if (Yii::$app->request->isAjax) {
            if (!empty($_POST['id'])) {
                $model_name = 'common\models\\' . $_POST['model'];
                $model = $model_name::findOne($_POST['id']);
                $image = UploadedFile::getInstances($model, $_POST['field']);

                $count = array();

                $images_isset = $model->getImagesByName($_POST['name']);
                if (!empty($images_isset)) {
                    foreach ($model->getImagesByName($_POST['name']) as $value) {
                        if (!empty($value->id)) {
                            $count[] = $value->id;
                        }
                    }
                }

                if (count($count) < (int)$_POST['max']) {
                    if (!empty($image)) {
                        if (is_array($image)) {
                            foreach ($image as $key => $value) {
                                $path = $_SERVER['DOCUMENT_ROOT'] . '/frontend/web/uploads/files/' . $model->id . $key . '.' . $value->name;
                                move_uploaded_file($value->tempName, $path);
                                $model->attachImage($path, false, $_POST['name']);
                                unlink($path);
                            }
                        } else {
                            $path = $_SERVER['DOCUMENT_ROOT'] . '/frontend/web/uploads/files/' . $model->id . '.' . $image->name;
                            move_uploaded_file($image->tempName, $path);
                            $model->attachImage($path, false, $_POST['name']);
                            unlink($path);
                        }
                        return Json::encode('');
                    }
                } else {
                    return Json::encode(array('error' => 'Max file'));
                }
            }
        }
        return Json::encode('');
    }

    public function actionDelete()
    {
        if (Yii::$app->request->isAjax) {
            $model_name = 'common\models\\' . $_POST['model'];
            $model = $model_name::findOne($_POST['id']);

            if (!empty($model)) {
                $image = $model->getImageById($_POST['key']);
                if (!empty($image)) {
                    $model->removeImage($image);
                    return Json::encode('');
                }
            }
        }
    }

    public function actionIndex()
    {
        //var_dump($_FILES);die;
        $name = $_FILES['upload']['name'];
        $path = $_SERVER['DOCUMENT_ROOT'] . "/frontend/web/files/";
        move_uploaded_file($_FILES['upload']['tmp_name'], $path . $name);
        $full_path = '/files/' . $name;
        $message = "Файл " . $_FILES['upload']['name'] . " загружен";
        $size = @getimagesize($path . $name);
        if ($size[0] < 50 OR $size[1] < 50) {
            unlink($path . $name);
            Yii::$app->end();
        }
        $callback = $_REQUEST['CKEditorFuncNum'];
        return '<script type="text/javascript">window.parent.CKEDITOR.tools.callFunction("' . $callback . '", "' . $full_path . '", "' . $message . '" );</script>';
    }
}

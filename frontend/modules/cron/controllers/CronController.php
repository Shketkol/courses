<?php

namespace frontend\modules\cron\controllers;

use common\models\UsersLessons;
use yii\web\Controller;
use Yii;



class CronController extends Controller
{

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;

        return parent:: beforeAction($action);
    }


    public function actionIndex()
    {
        $models = UsersLessons::find()
            ->where(['status' => 1])
            ->all();
        foreach ($models as $model) {
            $time_model = strtotime($model->created_at);
            $sec = $model->lessons->duration * 3600;
            $time = date('U', time()) - $time_model;
            $left = $sec - $time;
            if ($left <= (float)0) {
                $model->status = 2;
                $model->save(false);
            }
        }
    }
}

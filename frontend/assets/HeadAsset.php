<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class HeadAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'https://fonts.googleapis.com/icon?family=Material+Icons',
        'https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&lang=en',
//        'public/vendor/material-design-kit.css',
//        'public/vendor/sidebar-collapse.min.css',
//        'public/css/style.min.css',

        'static/plugins/bootstrap/css/bootstrap.css',
        'static/plugins/node-waves/waves.css',
        'static/plugins/animate-css/animate.css',
        'static/css/style.css',
    ];
    public $js = [
    ];
    public $depends = [
        'yii\bootstrap\BootstrapAsset',
    ];
    public $cssOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];
}

<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class MainAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [];
    public $js = [
//        'public/vendor/jquery.min.js',

//        'public/vendor/tether.min.js',
//        'public/vendor/bootstrap.min.js',
//        'public/vendor/dom-factory.js',
//        'public/vendor/material-design-kit.js',
//        'public/vendor/sidebar-collapse.js',
//        'public/js/main.min.js',

        'static/plugins/jquery/jquery.min.js',
        'static/plugins/bootstrap/js/bootstrap.js',
        'static/plugins/node-waves/waves.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
